package Entorno;

import java.util.HashMap;

public class Lista_Entornos {

    Entorno pila;
    int size;

    public void Lista_Entornos() {
        this.pila = null;// comienza con valor, por el entorno padre
        this.size = 0; // cant de entornos reconocidos
    }

    public boolean isEmpty() {
        return this.pila == null;
    }

    public int getSize() {
        return this.size;
    }

    public void crear_ent_padre() {
        agregar_nuevo_Ent_ADD();
    }


    public void agregar_nuevo_Ent_ADD() {// ADD
        this.size++;
        Entorno nuevoEntorno = new Entorno();
        nuevoEntorno.setTablaSimbolo(new HashMap<>());
/*
        if (isEmpty()) {
            this.pila = nuevoEntorno;
        } else {
            nuevoEntorno.setAnterior(this.pila);
            this.pila = nuevoEntorno;
        }
*/
        nuevoEntorno.setAnterior(this.pila);
        this.pila = nuevoEntorno;
    }

    public void regresar_un_Ent_POP(){// POP
        System.out.println("<pop  ,"+this.size+" >");
        this.pila = this.pila.getAnterior();
        this.size--;
    }


    public void print_Entornos() {
        Entorno aux = this.pila;
        while (aux != null) {
           // System.out.println("<Ent> " + aux.id);
            aux = aux.getAnterior();
        }

    }

    public Entorno getUltimo_Ent(){return this.pila;}


}
