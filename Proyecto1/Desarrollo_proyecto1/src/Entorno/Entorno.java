package Entorno;

import java.util.HashMap;

public class Entorno {
    HashMap<String, Simbolo> tablaSimbolo;
    Entorno padre;
    Entorno Anterior;

    //constructor
    public Entorno(Entorno padre) {
        this.padre = padre;
        this.Anterior = null;
        this.tablaSimbolo = new HashMap<String, Simbolo>();
    }

    public Entorno() {
        this.tablaSimbolo = new HashMap<String, Simbolo>();
        this.Anterior = null;
        this.padre = null;
    }

    //--------------------------------------------
    public void nuevoSimbolo(String nombre, Simbolo nuevoSim) {
        if (this.tablaSimbolo.containsKey(nombre.toUpperCase())) {
            //agregar a la lista de errores
            System.out.println("< la variable " + nombre + " ya existe ");
        } else {
            this.tablaSimbolo.put(nombre.toUpperCase(), nuevoSim);
        }
    }

    //---------------------------------------
    public HashMap<String, Simbolo> getTablaSimbolo() {
        return tablaSimbolo;
    }

    public void setTablaSimbolo(HashMap<String, Simbolo> tablaSimbolo) {
        this.tablaSimbolo = tablaSimbolo;
    }

    public Entorno getPadre() {
        return padre;
    }

    public void setPadre(Entorno padre) {
        this.padre = padre;
    }

    public Entorno getAnterior() {
        return Anterior;
    }

    public void setAnterior(Entorno anterior) {
        Anterior = anterior;
    }
}
