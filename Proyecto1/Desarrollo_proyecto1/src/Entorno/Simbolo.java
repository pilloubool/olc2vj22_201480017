package Entorno;

public class Simbolo {
    public String tipo;
    public Object valor;


    public Simbolo(String tipo, Object valor) {
        this.tipo = tipo;
        this.valor = valor;
    }
//---------------------------------------------
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }
}
