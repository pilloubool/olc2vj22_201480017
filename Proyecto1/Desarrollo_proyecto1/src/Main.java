import Gramatica.GramaticaLexer;
import Gramatica.GramaticaParser;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.antlr.v4.runtime.CharStreams.fromFileName;
import static org.antlr.v4.runtime.CharStreams.fromString;

public class Main {
    public static void main(String[] args) throws IOException {
        String input = "int var1 = 54+17*78;";
        String path = "./src/Micelaneos/input.txt";


        CharStream cs = fromString(input);
        CharStream cs_file = fromFileName(path);

        GramaticaLexer lexico = new GramaticaLexer(cs_file);
        CommonTokenStream tokens = new CommonTokenStream(lexico);
        GramaticaParser sintactico = new GramaticaParser(tokens);
        //GramaticaParser.StartContext startCtx = sintactico.start(); //produccion inicial
        ParseTree tree = sintactico.start();    //arbol desde la raiz


        Visitor visitor = new Visitor();
        // visitor.visit(startCtx);
        visitor.visit(tree);

//crear grafico del arbol
        List<String> rulesName = Arrays.asList(sintactico.getRuleNames());
        TreeViewer viewr = new TreeViewer(rulesName, tree);
       // viewr.open();

    }
}