grammar Gramatica;

//RESERVADAS
COMENT  : '!' .*? '\n' -> skip;
PROGRAM :   'program';
END     :   'end';


//TOKENS
INTEGER     : [0-9]+ ;
REAL    : INTEGER+ '.' INTEGER+;
ID      : [_a-zA-Z][a-zA-Z0-9_]* ;
STRING  : '"' (~["\r\n] | '""')* '"' ;
WS      : [ \t\r\n]+ -> skip ;


//RULES
start : programa  EOF;

//list_programas      : programa list_programas
//                    | programa
//                    ;

programa : PROGRAM  str_left= ID    linstrucciones     END PROGRAM str_right=ID  #v_programa;



linstrucciones  : instrucciones linstrucciones
                | instrucciones
                |
                ;

instrucciones   : block         #blck
                | declaration   #decl
                ;

block : '{' linstrucciones '}' ;


/// -------------------- DECLARACION --------------------------
declaration : type '::' list_id  ;

list_id :   ID ',' list_id
        |   ID
        ;

 /// -------------------- DECLARACION --------------------------


type : 'integer'
    | 'string'
    | 'complex'
    | 'character'
    | 'logical'
    ;


expr    : left=expr op = '^' right=expr          #opExpr
        | left=expr op=('*'|'/') right=expr      #opExpr
        | left=expr op=('+'|'-') right=expr      #opExpr
        | '(' expr ')'                           #parenExpr
        | entero=INTEGER                         #enteroExpr
        | str=STRING                             #strExpr
        | real=REAL                              #realExpr
        | '-' expr                               #unaryMinusExpr
        | '.not.' expr                           #notExpr

        ;