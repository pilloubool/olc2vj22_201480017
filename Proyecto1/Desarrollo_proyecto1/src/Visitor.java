import Gramatica.GramaticaBaseVisitor;
import Gramatica.GramaticaParser;

import java.util.LinkedList;

public class Visitor extends GramaticaBaseVisitor<Object> {

    LinkedList<Object> msj_Out = new LinkedList<Object>();

    @Override
    public Object visitEnteroExpr(GramaticaParser.EnteroExprContext ctx) {
        return Integer.valueOf(ctx.getText());
    }


    @Override
    public Object visitStrExpr(GramaticaParser.StrExprContext ctx) {
        return String.valueOf(ctx.str.getText());
    }

    @Override
    public Object visitOpExpr(GramaticaParser.OpExprContext ctx) {
        int izq = (int) visit(ctx.left);
        int der = (int) visit(ctx.right);
        String operacion = ctx.op.getText();

        switch (operacion.charAt(0)) {
            case '*':
                return izq * der;
            case '/':
                return izq / der;
            case '+':
                return izq + der;
            case '-':
                return izq - der;
            default:
                throw new IllegalArgumentException("operacion no valida");
        }
    }

    @Override
    public String visitDeclaration(GramaticaParser.DeclarationContext ctx) {
        String salida = "< var de tipo: \"" + visit(ctx.type()).toString() + "\" | nombre: \"" + ctx.list_id().getText();
               // + "\" | tiene valor de: \"" + visit(ctx.expr()).toString() + "\" >";
        System.out.println("Declaracion-->  " + salida);
        this.msj_Out.add(salida);

        return salida;
    }


    @Override
    public Object visitBlock(GramaticaParser.BlockContext ctx) {
        return visit(ctx.linstrucciones());
    }


    @Override
    public Object visitDecl(GramaticaParser.DeclContext ctx) {
        return visit(ctx.declaration());
    }


    @Override
    public Object visitStart(GramaticaParser.StartContext ctx) {
//        System.out.println("    <start> "+obj);
        Object obj = visit(ctx.programa());

        return this.msj_Out;
    }

    @Override
    public Object visitType(GramaticaParser.TypeContext ctx) {
        return ctx.getText();
    }


}
