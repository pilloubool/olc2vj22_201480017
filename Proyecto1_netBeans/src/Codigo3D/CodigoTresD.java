/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Codigo3D;

import java.util.*;

/**
 *
 * @author pilloubool
 */
public class CodigoTresD {

    public ArrayList<String> codigo3d;
    //contador de temporales
    private int temporal;//contador de temporales
    private int label;//contador de etiquetas
    String SubRutinas_Funciones = "";

    public CodigoTresD(ArrayList<String> codigo3d, int cont_Temporal, int cont_Etiquetas) {
        this.codigo3d = codigo3d;
        this.temporal = cont_Temporal;
        this.label = cont_Etiquetas;
    }

    public CodigoTresD() {
        this.codigo3d = new ArrayList<>();
        this.temporal = -1;
        this.label = -1;
    }

    public String generarTemporal() {
        this.temporal++;
        return String.valueOf("t" + this.temporal);
    }

    public String generarEtiqueta() {
        this.label++;
        return String.valueOf("L" + this.label);
    }

    public String lastTemporal() {
        return String.valueOf("t" + this.temporal);
    }

    public String getMain() {
        String retorno = "// temporal = (cont var de todos los Entornos anteriores) + (posicion de la variable en el entorno actual)\n";
        String codigo = getPrintVarInt()
                + getPrint_Int()
                + getPrintVarChar()
                + getPrint_char()
                + getPrintStr()
                + get_print_C3D_Arreglo_Char_una_dim()
                + get_print_C3D_Arreglo_float_una_dim()
                + get_print_C3D_Arreglo_float_dos_dim()
                + get_print_C3D_Arreglo_char_dos_dim()
                + "\n" + this.SubRutinas_Funciones;

        retorno
                += getHeader()
                + codigo
                + "//----------------------------------- MAIN ------------------------------------------------\n"
                + "int main(){\n";
        for (String ln : codigo3d) {
            retorno += ln + "\n";
        }
        retorno += "\n\nreturn 0;\n"
                + "}";
        return retorno;
    }

    public String getHeader() {
        //para obtner solo el listado de temporales : t1,t2,...,tn;
        String temporales = "";
        for (int i = 0; i <= this.temporal; i++) {
            temporales += "t" + String.valueOf(i) + (i < this.temporal ? "," : ";\n");
        }
        String retorno = "#include<stdio.h>\n"
                + "double STACK[30101999];\n"
                + "double HEAP[30101999];\n"
                + "double tprintInt;\n"
                + "double tprint_i;\n"
                + "double tprint_j;\n"
                + "double P; //puntero del STACK\n"
                + "double H; //puntero del HEAP\n"
                + "double " + temporales + "\n";

        return retorno;
    }

    public String getPrintVarInt() {
        String retorno = "void imprimir_var_int()\n{\n"
                + " double tprint;\n"
                + " tprint = STACK[(int)P];\n"
                + " printf(\"%f\\n\",tprint );\n"
                + " return;\n}\n\n";
        return retorno;
    }

    public String getPrintVarChar() {
        String retorno = "void imprimir_var_char()\n{\n"
                + " double tprint;\n"
                + " tprint = STACK[(int)P];\n"
                + " printf(\"%c\\n\",(char)tprint );\n"
                + " return;\n}\n\n";
        return retorno;
    }

    public String getPrint_Int() {
        String retorno = "void imprimir_int()\n{\n"
                + " printf(\"%f\\n\",tprintInt);\n"
                + " return;\n}\n\n";
        return retorno;
    }

    public String getPrint_char() {
        String retorno = "void imprimir_char()\n{\n"
                + " printf(\"%c\\n\",(char)tprintInt);\n"
                + " return;\n}\n\n";
        return retorno;
    }

    private String getPrintStr() {
//        L1:                           ciclo
//        if (relacional) goto L2       si
//          goto L3:                    no
//        L2:
//          <codigo>
//          goto L1:
//        L3:                           salida
        //imprimir_string
        String L1 = this.generarEtiqueta();//L1
        String L2 = this.generarEtiqueta();//L2
        String L3 = this.generarEtiqueta();//L3
        String tn = this.generarTemporal();

        String retorno = "void imprimir_string()\n{\n"
                + " " + L1 + ":\n"
                + "  " + tn + " = HEAP[(int)H] ;\n"
                + "     if (" + tn + " != -1) goto " + L2 + " ;\n"
                + "     goto " + L3 + " ;\n"
                + " " + L2 + ":\n"//CODIGO
                + "     printf(\"%c\",(char)" + tn + ");\n"//CODIGO
                + "     H = H + 1 ;\n" //CODIGO
                + "     goto " + L1 + " ;\n"
                + "  " + L3 + ":\n"
                + "     printf(\"%c\\n\",(char)32);\n"//CODIGO
                + " return;\n}\n\n";
        return retorno;
    }

    public String get_print_C3D_Arreglo_Char_una_dim() {
//        L1:                           ciclo
//        if (relacional) goto L2       si
//          goto L3:                    no
//        L2:
//          <codigo>
//          goto L1:
//        L3:                           salida
        //imprimir_string
        String L1 = this.generarEtiqueta();//L1
        String L2 = this.generarEtiqueta();//L2
        String L3 = this.generarEtiqueta();//L3
        String L4 = this.generarEtiqueta();//L3
        String L5 = this.generarEtiqueta();//L3

        String tn = this.generarTemporal();

        String retorno = "void imprimir_arr_char_una_dim()\n{\n"
                + "     printf(\"%c\",(char) 91);\n"//espacios para que se observe mejor el numero
                + " " + L1 + ":\n"
                + "  " + tn + " = HEAP[(int)H] ;\n"
                + "     if (" + tn + " != -1) goto " + L2 + " ;\n"
                + "     goto " + L3 + " ;\n"
                + " " + L2 + ":\n"//CODIGO

                + " if ( " + tn + " == 0 ) goto " + L4 + " ; //if para imprimir un espacio libre en el arreglo\n"
                + " goto " + L5 + " ;\n"
                + " " + L4 + ":\n"//CODIGO
                + "     " + tn + " = 95 ;//  95 = guion bajo\n"
                + " " + L5 + ":\n"//CODIGO

                + "     printf(\"%c\",(char) " + tn + " ) ;\n"//CODIGO
                + "     printf(\"%c\",(char) 32);//espacios para que se observe mejor el numero\n"//espacios para que se observe mejor el numero
                + "     printf(\"%c\",(char) 32);\n"//espacios para que se observe mejor el numero

                + "     H = H + 1 ;\n" //CODIGO
                + "     goto " + L1 + " ;\n"
                + "  " + L3 + ":\n"
                + "     printf(\"%c\\n\",(char) 93);\n"//espacios para que se observe mejor el numero
                + " return;\n}\n\n";
        return retorno;
    }

    public String get_print_C3D_Arreglo_float_una_dim() {
//        L1:                           ciclo
//        if (relacional) goto L2       si
//          goto L3:                    no
//        L2:
//          <codigo>
//          goto L1:
//        L3:                           salida
        //imprimir_string
        String L1 = this.generarEtiqueta();//L1
        String L2 = this.generarEtiqueta();//L2
        String L3 = this.generarEtiqueta();//L3
        String tn = this.generarTemporal();

        String retorno = "void imprimir_arr_float_una_dim()\n{\n"
                + "     printf(\"%c\",(char) 91);\n"//espacios para que se observe mejor el numero
                + " " + L1 + ":\n"
                + "  " + tn + " = HEAP[(int)H] ;\n"
                + "     if (" + tn + " != -1) goto " + L2 + " ;\n"
                + "     goto " + L3 + " ;\n"
                + " " + L2 + ":\n"//CODIGO
                + "     printf(\"%f\",(float) " + tn + ");\n"//CODIGO
                + "     printf(\"%c\",(char) 32);\n"//espacios para que se observe mejor el numero
                + "     printf(\"%c\",(char) 32);\n"//espacios para que se observe mejor el numero

                + "     H = H + 1 ;\n" //CODIGO
                + "     goto " + L1 + " ;\n"
                + "  " + L3 + ":\n"
                + "     printf(\"%c\\n\",(char) 93);\n//Salto de linea para generar una mejor visivilidad"
                + " return;\n}\n\n";
        return retorno;
    }

    public String get_print_C3D_Arreglo_float_dos_dim() {
        String L1 = this.generarEtiqueta();//L1
        String L2 = this.generarEtiqueta();//L2
        String L3 = this.generarEtiqueta();//L3
        String tn = this.generarTemporal();

        String L4 = this.generarEtiqueta();//L2//salto
        String L5 = this.generarEtiqueta();//L3

        String retorno = "void imprimir_arr_float_dos_dim()\n{\n"
                + "     printf(\"%c\",(char) 91);\n"//espacios para que se observe mejor el numero
                + "double cont = 0 ;"
                + " " + L1 + ":\n"
                + "  " + tn + " = HEAP[(int)H] ;\n"
                + "     if ( " + tn + " != -1) goto " + L2 + " ;\n"
                + "     goto " + L3 + " ;\n"
                + " " + L2 + ":\n"//CODIGO
                // 
                + "if ( " + "cont == tprint_j ) goto " + L4 + " ;\n"
                + "goto " + L5 + " ;\n"
                + L4 + ": \n"
                + "     printf(\"%c\\n\",(char) 32);\n"//espacios para que se observe mejor el numero
                + L5 + ": \n"
                //
                + "     printf(\"%f\",(float) " + tn + ");\n"//CODIGO
                + "     printf(\"%c\",(char) 32);\n"//espacios para que se observe mejor el numero
                + "     printf(\"%c\",(char) 32);\n"//espacios para que se observe mejor el numero

                + "     H = H + 1 ;\n" //CODIGO
                + "     cont = cont + 1 ;\n"
                + "     goto " + L1 + " ;\n"
                + "  " + L3 + ":\n"
                + "     printf(\"%c\\n\\n\",(char) 93);\n//Salto de linea para generar una mejor visivilidad"
                + " return;\n}\n\n";
        return retorno;
    }

    public String get_print_C3D_Arreglo_char_dos_dim() {
        String L1 = this.generarEtiqueta();//L1
        String L2 = this.generarEtiqueta();//L2
        String L3 = this.generarEtiqueta();//L3
        String tn = this.generarTemporal();

        String L4 = this.generarEtiqueta();//L2//salto
        String L5 = this.generarEtiqueta();//L3

        String retorno = "void imprimir_arr_char_dos_dim()\n{\n"
                + "     printf(\"%c\",(char) 91);\n"//espacios para que se observe mejor el numero
                + "double cont = 0 ;"
                + " " + L1 + ":\n"
                + "  " + tn + " = HEAP[(int)H] ;\n"
                + "     if ( " + tn + " != -1) goto " + L2 + " ;\n"
                + "     goto " + L3 + " ;\n"
                + " " + L2 + ":\n"//CODIGO
                // 
                + "if ( " + "cont == tprint_j ) goto " + L4 + " ;\n"
                + "goto " + L5 + " ;\n"
                + L4 + ": \n"
                + "     printf(\"%c\\n\",(char) 32);\n"//espacios para que se observe mejor el numero
                + L5 + ": \n"
                //
                + "     printf(\"%c\",(char) " + tn + ");\n"//CODIGO
                + "     printf(\"%c\",(char) 32);\n"//espacios para que se observe mejor el numero
                + "     printf(\"%c\",(char) 32);\n"//espacios para que se observe mejor el numero

                + "     H = H + 1 ;\n" //CODIGO
                + "     cont = cont + 1 ;\n"
                + "     goto " + L1 + " ;\n"
                + "  " + L3 + ":\n"
                + "     printf(\"%c\\n\\n\",(char) 93);\n//Salto de linea para generar una mejor visivilidad"
                + " return;\n}\n\n";
        return retorno;
    }

    public void addSubFunc(String str) {
        this.SubRutinas_Funciones += str;
    }

    //k
}
