/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
//package proyecto1_netbeans.Gramatica;
import Entorno.Entorno;
import Micelaneos.NumeroLinea;

import Gramatica.GramaticaLexer;
import Gramatica.GramaticaParser;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import java.util.ArrayList;
import Micelaneos.ErroresAnalizador;
import Micelaneos.Metodo_Genericos;
import Reportes.Nodo_Ast;
import org.antlr.v4.runtime.BaseErrorListener;

import static org.antlr.v4.runtime.CharStreams.fromString;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import Reportes.Tabla;
import Reportes.Reporte_Entornos;
import java.util.Stack;

/**
 *
 * @author pilloubool
 */
public class Gui extends javax.swing.JFrame {

    NumeroLinea Area_texto_input;
    NumeroLinea Area_texto_output;
    NumeroLinea Area_texto_c3d;
    TreeViewer viewr;
    boolean is_compilado;
    ArrayList<ErroresAnalizador> Errores;
    Stack<Entorno> pila_Ent;
    String astGraphviz;

    public Gui() {
        initComponents();
        Errores = new ArrayList<>();
        pila_Ent = new Stack<>();
        is_compilado = false;
        this.Area_texto_input = new NumeroLinea(jTextArea_input);
        this.Area_texto_output = new NumeroLinea(jTextArea_outPut);
        this.Area_texto_c3d = new NumeroLinea(jTextArea_c3d);

        jTextArea_input.setText("");

        jScrollPane1.setRowHeaderView(Area_texto_input);
        jScrollPane2.setRowHeaderView(Area_texto_output);
        jScrollPane3.setRowHeaderView(Area_texto_c3d);
        astGraphviz = "";
        cargaMasiva();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton_compilar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea_input = new javax.swing.JTextArea();
        jButton_limpiar_compilar = new javax.swing.JButton();
        jButton_guardar = new javax.swing.JButton();
        jButton_cargaMasiva = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea_outPut = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea_c3d = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        jButton_limpiar_salida = new javax.swing.JButton();
        jButton_limpiar_c3d = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        jButton_reporte = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Proyecto1_Compi2_201480017_vacas_Junio_2022");

        jButton_compilar.setText("Compilar");
        jButton_compilar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_compilarActionPerformed(evt);
            }
        });

        jLabel1.setText("Entrada");

        jTextArea_input.setColumns(20);
        jTextArea_input.setRows(5);
        jScrollPane1.setViewportView(jTextArea_input);

        jButton_limpiar_compilar.setText("Limpiar");
        jButton_limpiar_compilar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_limpiar_compilarActionPerformed(evt);
            }
        });

        jButton_guardar.setText("Guardar");
        jButton_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_guardarActionPerformed(evt);
            }
        });

        jButton_cargaMasiva.setText("Carga masiva");
        jButton_cargaMasiva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_cargaMasivaActionPerformed(evt);
            }
        });

        jLabel2.setText("Salida");

        jTextArea_outPut.setColumns(20);
        jTextArea_outPut.setRows(5);
        jScrollPane2.setViewportView(jTextArea_outPut);

        jTextArea_c3d.setColumns(20);
        jTextArea_c3d.setRows(5);
        jScrollPane3.setViewportView(jTextArea_c3d);

        jLabel3.setText("C3D");

        jButton_limpiar_salida.setText("limpiar");
        jButton_limpiar_salida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_limpiar_salidaActionPerformed(evt);
            }
        });

        jButton_limpiar_c3d.setText("limpiar");
        jButton_limpiar_c3d.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_limpiar_c3dActionPerformed(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Arbol_CST", "Errores", "Rep_Ent", "Rep_Ast" }));

        jButton_reporte.setText("Reporte");
        jButton_reporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_reporteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton_reporte))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 505, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jButton_compilar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_limpiar_compilar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_guardar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_cargaMasiva)
                .addGap(328, 328, 328)
                .addComponent(jButton_limpiar_salida)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton_limpiar_c3d)
                .addGap(106, 106, 106))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_reporte))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton_limpiar_c3d, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton_compilar)
                        .addComponent(jButton_limpiar_compilar)
                        .addComponent(jButton_guardar)
                        .addComponent(jButton_cargaMasiva)
                        .addComponent(jButton_limpiar_salida)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_limpiar_salidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_limpiar_salidaActionPerformed
        // TODO add your handling code here:
        jTextArea_outPut.setText("");
    }//GEN-LAST:event_jButton_limpiar_salidaActionPerformed

    private void jButton_limpiar_c3dActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_limpiar_c3dActionPerformed
        // TODO add your handling code here:
        jTextArea_c3d.setText("");
    }//GEN-LAST:event_jButton_limpiar_c3dActionPerformed

    private void jButton_limpiar_compilarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_limpiar_compilarActionPerformed
        // TODO add your handling code here:
        jTextArea_input.setText("");
        is_compilado = false;
    }//GEN-LAST:event_jButton_limpiar_compilarActionPerformed
// -------------------------------------------------------------------- entrada de texto -----------------------------------------------------------------------------------------------------------------------------------------
    private void jButton_compilarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_compilarActionPerformed
        // TODO add your handling code here:
        jTextArea_outPut.setText("");

        String input = jTextArea_input.getText();
        if (!input.equals("")) {

            is_compilado = true;
            jTextArea_outPut.setText(Principal(input));
            JOptionPane.showMessageDialog(null, " <Ejecucion Ok>");
        } else {
            JOptionPane.showMessageDialog(null, " <La entrada No debe estar vacio>");

        }
    }//GEN-LAST:event_jButton_compilarActionPerformed

    private void jButton_cargaMasivaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_cargaMasivaActionPerformed
        cargaMasiva();
    }//GEN-LAST:event_jButton_cargaMasivaActionPerformed

    private void jButton_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_guardarActionPerformed
        String path = "./src/Micelaneos/input.txt";
        String data = jTextArea_input.getText();
        if (!data.equals("")) {
            guardarArchivo(path, data);
            JOptionPane.showMessageDialog(null, "Se guardo Correctamente");
        } else {
            JOptionPane.showMessageDialog(null, "No, se guardo, por que la entrada esta vacia");
        }

    }//GEN-LAST:event_jButton_guardarActionPerformed

    private void jButton_reporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_reporteActionPerformed
        // TODO add your handling code here:
        String seleccion = jComboBox1.getSelectedItem().toString();
        System.out.println(seleccion);
        if (is_compilado) {
            if (seleccion.toLowerCase().equals("arbol_cst")) {
                viewr.open();
            } else if (seleccion.toLowerCase().equals("errores")) {
                Tabla RepErrores = new Tabla(Errores, "Reporte de Errores, (lex,sin,semantico)");
            } else if (seleccion.toLowerCase().equals("rep_ent")) {
                Reporte_Entornos rp = new Reporte_Entornos(pila_Ent);
                rp.crearReporte_Ent();
            } else if (seleccion.toLowerCase().equals("rep_ast")) {
                Metodo_Genericos m = new Metodo_Genericos();
                m.crearArch_dot("AST", astGraphviz);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se ha compilado");
        }
    }//GEN-LAST:event_jButton_reporteActionPerformed
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Gui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Gui().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_cargaMasiva;
    private javax.swing.JButton jButton_compilar;
    private javax.swing.JButton jButton_guardar;
    private javax.swing.JButton jButton_limpiar_c3d;
    private javax.swing.JButton jButton_limpiar_compilar;
    private javax.swing.JButton jButton_limpiar_salida;
    private javax.swing.JButton jButton_reporte;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jTextArea_c3d;
    private javax.swing.JTextArea jTextArea_input;
    private javax.swing.JTextArea jTextArea_outPut;
    // End of variables declaration//GEN-END:variables

    //-------------------------------------------------------------------------------------------------------------------
    public void cargaMasiva() {
        String input = "";
        String path = "./src/Micelaneos/input.txt";
        File file = new File(path);
        BufferedReader buffer = null;
        try {
            buffer = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
        }
        String str;
        try {
            while ((str = buffer.readLine()) != null) {
                input += str + "\n";
            }
        } catch (IOException ex) {
            Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
        }
        jTextArea_input.setText(input);
    }

    public void guardarArchivo(String path, String data) {

        FileWriter fw;
        try {
            fw = new FileWriter(path);
        } catch (IOException io) {
            return;
        }

        //Escribimos
        try {
            fw.write(data);
        } catch (IOException io) {
            System.out.println("< Error al escribir Archivo >");
        }

        //cerramos el fichero
        try {
            fw.close();
        } catch (IOException io) {
            System.out.println("Error al cerrar el archivo");
        }
    }

    public String Principal(String input) {
        //errores
        Errores = new ArrayList<>();
        //String input = "int var1 = 54+17*78;";
        input = input.toLowerCase();
        CharStream cs = fromString(input);

        GramaticaLexer lexico = new GramaticaLexer(cs);
        lexico.removeErrorListeners();
        lexico.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
                //throw new ParseCancellationException("line " + line + ":" + charPositionInLine + " " + msg);
                Errores.add(new ErroresAnalizador(line, charPositionInLine, "Lex", msg, ErroresAnalizador.ErrorTipo.Lexico));
            }
        });

        CommonTokenStream tokens = new CommonTokenStream(lexico);
        GramaticaParser sintactico = new GramaticaParser(tokens);

        sintactico.removeErrorListeners();
        sintactico.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
                Errores.add(new ErroresAnalizador(line, charPositionInLine, "Sin", msg, ErroresAnalizador.ErrorTipo.Sintactico));
            }
        });

        //GramaticaParser.StartContext startCtx = sintactico.start(); //produccion inicial
        ParseTree tree = sintactico.start();    //arbol desde la raiz
        //sintactico.notifyErrorListeners(sintactico.getCurrentToken(),"holi",null);

        MyVisitor visitor = new MyVisitor(new Entorno(null), false);
        // visitor.visit(startCtx);
        Object retorno = visitor.visit(tree);

        //crear grafico del arbol
        List<String> rulesName = Arrays.asList(sintactico.getRuleNames());
        viewr = new TreeViewer(rulesName, tree);

        //capturar errores semanticos, de la clase visitor
        Errores.addAll(visitor.Errores);
        //reporte de Entornos
        pila_Ent = visitor.pila_Ent;
        System.out.println("--------------- AST----------------------------------");
//        MyVisitor_AST visitorAST = new MyVisitor_AST(new Entorno(null));
//        visitorAST.visit(tree);
//        if (!visitorAST.ast.isEmpty()) {
//            Nodo_Ast ast = visitorAST.ast.pop();
//            astGraphviz = ast.getAstGraphviz();
//        }
        System.out.println("-------------------------------------------------");

        //SEGUNDA PASADA -> GENERACION DE CODIGO EN 3 DIRECCIONES   
        //retorno para jtextArea_output
        Crear_C3D(tree, visitor.padre, visitor.pila_Ent);
        return LinkedList_to_String((LinkedList<String>) retorno);
    }

    public String LinkedList_to_String(LinkedList<String> list) {
        String retorno = "";
        for (int i = 0; i < list.size(); i++) {
            retorno += list.get(i) + "\n";
        }
        return retorno;
    }

    private void Crear_C3D(ParseTree tree, Entorno padre, Stack<Entorno> pila_ent) {
        MyVisitor_c3d visitor_c3d = new MyVisitor_c3d(padre, pila_ent, true);
        visitor_c3d.visit(tree);
        String str_c3d = visitor_c3d.c3d.getMain();
        //System.out.println(str_c3d);
        jTextArea_c3d.setText("");
        jTextArea_c3d.setText(str_c3d);
        //crear Archivo en cpp
        String path = "./src/Reportes/c3d.cpp";
        String data = str_c3d;
        if (!data.equals("")) {
            guardarArchivo(path, data);
        } else {
            JOptionPane.showMessageDialog(null, "No, se guardo, por que la entrada esta vacia");
        }
    }//c3d

}
