/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Micelaneos;

/**
 *
 * @author pilloubool
 */
public class ErroresAnalizador {

    int fila;
    int columna;
    String instruccion;
    String descripcion;
    ErrorTipo tipo;

    public ErroresAnalizador(int fila, int columna, String instruccion, String descripcion, ErrorTipo tipo) {
        this.fila = fila;
        this.columna = columna;
        this.instruccion = instruccion;
        this.descripcion = descripcion;
        this.tipo = tipo;
    }
    
    public enum ErrorTipo {
        Lexico,
        Sintactico,
        Semantico
    }
    
//----------------------------------

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ErrorTipo getTipo() {
        return tipo;
    }

    public void setTipo(ErrorTipo tipo) {
        this.tipo = tipo;
    }

    public String getInstruccion() {
        return instruccion;
    }

    public void setInstruccion(String instruccion) {
        this.instruccion = instruccion;
    }

}
