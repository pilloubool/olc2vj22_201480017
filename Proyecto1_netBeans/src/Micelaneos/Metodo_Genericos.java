/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Micelaneos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author pilloubool
 */
public class Metodo_Genericos {

    public String get_str_archivo() throws FileNotFoundException, IOException {
        // TODO add your handling code here:
        String texto = "";
        String path = "./src/Micelaneos/input.txt";
        File file = new File(path);
        BufferedReader buffer = null;
        buffer = new BufferedReader(new FileReader(file));

        String str;
        while ((str = buffer.readLine()) != null) {
            texto += str + "\n";
        }
        return texto;
    }

    public void guardar_Archivo(String path, String data) {

        FileWriter fw;
        try {
            fw = new FileWriter(path);
        } catch (IOException io) {
            return;
        }

        //Escribimos
        try {
            fw.write(data);
        } catch (IOException io) {
            System.out.println("< Error al escribir Archivo >");
        }

        //cerramos el fichero
        try {
            fw.close();
        } catch (IOException io) {
            System.out.println("Error al cerrar el archivo");
        }
    }

    public void crearArch_dot(String nombreDot, String data) {
        String path = "./src/Reportes/" + nombreDot;

        try {
            File file = new File(path + ".dot");
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(data);
            bw.close();
            Compilar_dot(path);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Compilar_dot(String path) {
        String extension = "pdf";
        try {
            //dot -Tpng nombreArchivo.dot -o nomImagen.png
            String[] cmd = {"dot", "-T" + extension, path + ".dot", "-o", path + "." + extension};
            Runtime.getRuntime().exec(cmd);

            String[] cmd2 = {"okular", path + "." + extension};
            Runtime.getRuntime().exec(cmd2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
