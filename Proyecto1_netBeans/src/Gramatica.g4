grammar Gramatica;

options { caseInsensitive = true; }
//RESERVADAS
COMENT  : '!' .*? '\n' -> skip;


//TOKENS
INTEGER     : [0-9]+ ;
REAL    : INTEGER+ '.' INTEGER+;
ID      : [_a-zA-Z][a-zA-Z0-9_]* ;
STRING  : '\'' (~["\r\n] | '""')* '\'' ;
CHAR    : '"' (~["\r\n] | '""')? '"' ;
WS      : [ \t\r\n]+ -> skip ;


//RULES
start : (function | subroutine)* programa  EOF;

//list_programas      : programa list_programas
//                    | programa
//                    ;
programa : 'program'  prog= ID  implicit  instrucciones*  'end' 'program' prog_end=ID;

implicit : 'implicit' 'none' ;

linstrucciones  : (instrucciones)+
                ;

instrucciones   : block         
                | declaration   
                | print         
                | asignacion    
                | declaracion_arreglos
                | allocate
                | deallocate
                | printEnt
                | if_
                | do_
                | do_while
                | exit
                | cycle
                | call
                | call_function
                ;

block   : '{' linstrucciones '}' 
        ;

/// -------------------- Arreglos --------------------------
declaracion_arreglos    :       type ',' 'dimension' '(' l_expr+ ')' '::' ID            #declaracion_arreglos1
                        |       type '::' ID '(' l_expr+ ')'                            #declaracion_arreglos1
                        |       type ','   'allocatable' '::' ID cant_arrDinamic_dim    #declaracion_arr_dinamicos
                        ;

cant_arrDinamic_dim : '(' una=':' (',' dos=':')? ')'   ;


l_expr  : ',' expr
        | expr
        ;


allocate        :       'allocate' '(' ID '(' una=expr (',' dos=expr)? ')'  ')'
                ;

deallocate      :       'deallocate' '(' ID ')'
                ;

/// -------------------- DECLARACION --------------------------
declaration     : type '::' declaration2 (',' declaration2)* 
                ;

declaration2    :  ID '=' expr  
                |  ID           
                ;
/// -------------------- ASIGNACION SIMPLE Y ARREGLOS --------------------------
asignacion      : ID '=' expr                            #asignacion_primitivos
                | ID '=' '(''/' l_expr* '/'')'           #asignacion_arreglos_unaDim
                | ID '[' posicion=expr ']' '=' dato=expr #asignacion_arreglos_unaDim2
                | ID '[' exp_izq=expr ',' exp_der=expr ']' '=' dato=expr   #asignacion_arreglos_DosDim
                ;
//-------------------------------       ENTORNOS --------------------------------------------------------
printEnt :  'print_ent()';
//----------------------------------    IF  -----------------------------------------------------
if_     : 'if' '(' expr ')' 'then' (instrucciones)*  (elseIf_)* (else_)? 'end' 'if'
        ;

elseIf_ : 'else' 'if' '(' expr ')' 'then'  (instrucciones)* 
        ;

else_   :  'else' (instrucciones)*
        ;

//-------------------------------------  CICLOS --------------------------------------------------

//--------------------------------------   DO     -------------------------------------------------
do_     :    (etiquete=ID ':')?   'do'  asignacion ',' fin=expr ',' incremento=expr   (instrucciones)*  'end' 'do' etiqueta_end=ID?
        ;
//----------------------------------------  DO WHILE -----------------------------------------------
do_while:   (etiqueta=ID ':')?     'do' 'while' '(' expr ')'  (instrucciones)* 'end' 'do'etiqueta_end=ID?
        ;
//-----------------------------------    CONTROL DE CICLOS       ----------------------------------------------------
exit    :       'exit'  ID?;

cycle   :       'cycle' ID?;
//--------------------------------------  SUB-RUTINAS      -------------------------------------------------

list_param      :  ',' ID
                | ID;

intent :',' 'intent' '(' 'in' ')' '::';                

declaracion_parametro_func  : type  intent  ID ( '(' list_param? ')' )?    ;

subroutine      : 'subroutine' nom=ID '(' list_param* ')' implicit declaracion_parametro_func*  instrucciones*  'end' 'subroutine' nom_end=ID
                ;

call : 'call' ID '(' l_expr* ')';

//------------------------------------     FUNCIONES     ---------------------------------------------------

function :'function' nom=ID '(' list_param* ')' 'result' '(' retorno=ID? ')' implicit declaracion_parametro_func* instrucciones* 'end' 'function' nom_end=ID   ;

call_function : ID '(' l_expr* ')' ;

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------
 /// -------------------- Print --------------------------
//how make a list:  https://stackoverflow.com/questions/27405893/using-visitors-in-antlr4-in-a-simple-integer-list-grammar
// Errores:             https://stackoverflow.com/questions/18132078/handling-errors-in-antlr4
print : 'print' '*' ','  estado_expr (',' estado_expr)* ;

estado_expr : expr ;




 /// -------------------- tYPE --------------------------

type : 'integer'
    |   'real'
    |   'complex'
    |   'character'
    |   'logical'
    ;


expr    : left=expr op = '**' right=expr         #opExpr
        | left=expr op=('*'|'/') right=expr      #opExpr
        | left=expr op=('+'|'-') right=expr      #opExpr
        | '(' expr ')'                           #parenExpr
        | entero=INTEGER                         #enteroExpr
        | str=STRING                             #strExpr
        | real=REAL                              #realExpr
        | id=ID                                  #idExpr
        | CHAR                                   #charExpr      
        | '-' expr                               #unaryMinusExpr
        | '.''not''.' expr                       #notExpr
        | '.'b='true''.'                         #boolExpr
        | '.'b='false''.'                        #boolExpr
        | left=expr     op=('=='|'.eq.')        right=expr  #relacionales_Expr
        | left=expr     op=('/='|'.ne.')        right=expr  #relacionales_Expr
        | left=expr     op=('>'|'.gt.')         right=expr  #relacionales_Expr
        | left=expr     op=('<'|'.lt.')         right=expr  #relacionales_Expr
        | left=expr     op=('>='|'.ge.')        right=expr  #relacionales_Expr
        | left=expr     op=('<='|'.le.')        right=expr  #relacionales_Expr
        | left=expr     op='.and.'              right=expr  #relacionales_Expr
        | left=expr     op='.or.'               right=expr  #relacionales_Expr
        | ID '(' l_expr* ')'                     #call_function_expr 
        | 'size' '(' ID ')'                     #getSize_Arreglo
        ;