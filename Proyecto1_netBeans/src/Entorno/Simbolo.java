package Entorno;

public class Simbolo {

    String tipo;
    Object valor;
    public int posicion;

    public Simbolo(String tipo, Object valor, int posicion) {
        this.tipo = tipo;
        this.valor = valor;
        this.posicion = posicion;
    }

    public Simbolo(String tipo, Object valor) {
        this.tipo = tipo;
        this.valor = valor;
        this.posicion = -1;
    }
//---------------------------------------------

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }
}
