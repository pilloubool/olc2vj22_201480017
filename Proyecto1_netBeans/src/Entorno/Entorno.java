package Entorno;

import java.util.HashMap;
import java.util.Stack;

public class Entorno {

    HashMap<String, Simbolo> tablaSimbolo;
    Entorno padre;
    Entorno siguiente;
    public int ultimaPosicion;
    Stack<Entorno> lista_entornos_Aux;

    //constructor
    public Entorno(Entorno padre) {
        this.lista_entornos_Aux = new Stack<>();
        this.padre = padre;
        this.tablaSimbolo = new HashMap<>();
        this.siguiente = null;
        this.ultimaPosicion = 0;
    }

    //--------------------------------------------------------------------------------------------------------------------
    public void nuevoSimbolo(String nombre, Simbolo nuevoSim) {
        if (this.tablaSimbolo.containsKey(nombre)) {
            //agregar a la lista de errores
            System.out.println("< la variable " + nombre + " ya existe, en entorno actual >");
        } else {
            this.tablaSimbolo.put(nombre, nuevoSim);
        }
    }

    public Simbolo Buscar(String nombre) {
        for (Entorno ent = this; ent != null; ent = ent.getPadre()) {
            if (ent.tablaSimbolo.containsKey(nombre)) {
                return ent.tablaSimbolo.get(nombre);
            }
        }
        return null;
    }

    public Boolean existe_simbolo(String nombre) {
        for (Entorno ent = this; ent != null; ent = ent.getPadre()) {
            if (ent.tablaSimbolo.containsKey(nombre)) {
                return true;
            }
        }
        return false;
    }

    public void Actualizar_Simbolo(String nombre, Simbolo nuevoSim) {
        for (Entorno ent = this; ent != null; ent = ent.getPadre()) {
            if (ent.tablaSimbolo.containsKey(nombre)) {
                ent.tablaSimbolo.put(nombre, nuevoSim);
            }
        }
    }

    public int getPrevSizes() {
        int size = 0;
        for (Entorno ent = this.padre; ent != null; ent = ent.padre) {
            size += ent.tablaSimbolo.size();
        }
        return size;
    }

// ----------------------------------------------------------------------
    public HashMap<String, Simbolo> getTablaSimbolo() {
        return tablaSimbolo;
    }

    public void setTablaSimbolo(HashMap<String, Simbolo> tablaSimbolo) {
        this.tablaSimbolo = tablaSimbolo;
    }

    public Entorno getPadre() {
        return padre;
    }

    public void setPadre(Entorno padre) {
        this.padre = padre;
    }

    public Entorno getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Entorno siguiente) {
        this.siguiente = siguiente;
    }

    public int getUltimaPosicion() {
        return ultimaPosicion;
    }

    public void setUltimaPosicion(int ultimaPosicion) {
        this.ultimaPosicion = ultimaPosicion;
    }

    public Stack<Entorno> getLista_entornos_Aux() {
        return lista_entornos_Aux;
    }

    public void setLista_entornos_Aux(Stack<Entorno> lista_entornos_Aux) {
        this.lista_entornos_Aux = lista_entornos_Aux;
    }

}//fin
