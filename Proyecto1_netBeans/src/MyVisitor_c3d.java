
import Entorno.Entorno;
import Entorno.Simbolo;
import Gramatica.GramaticaBaseVisitor;
import Gramatica.GramaticaParser;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;
import Instrucciones.Declaracion;
import Instrucciones.Relacionales;
import Instrucciones.Arreglo;
import Instrucciones.Cycle;
import Instrucciones.Exit;
import Instrucciones.Funciones;
import Micelaneos.ErroresAnalizador;
import Instrucciones.Subroutine;
import java.util.List;
import Reportes.*;
import Codigo3D.CodigoTresD;

public class MyVisitor_c3d extends GramaticaBaseVisitor<Object> {

    public Stack<Entorno> pila_Ent = new Stack<>();
    Stack<String> Etiquetas_if = new Stack<>();
    public static LinkedList<Object> msj_Out = new LinkedList<>();
    ArrayList<ErroresAnalizador> Errores = new ArrayList<>();
    public int limite_panic_doWhile;
    boolean is3d;
    CodigoTresD c3d = new CodigoTresD();
    Entorno padre;

    public MyVisitor_c3d(Entorno ent, Stack<Entorno> pilaEnt, boolean is3d) {
        this.pila_Ent = pilaEnt;
        this.is3d = is3d;
        this.msj_Out.clear();
        this.pila_Ent.push(ent);//creando el entorno padre
        this.limite_panic_doWhile = 1000;
        this.padre = ent;
    }

    //ctx.getStart().getLine(),  
    //ctx.getStart().getCharPositionInLine()
    //-------------------------------- MSJ  -------------------------------
    public void addMsj(int type, String Inst, String msj, int fila, int columna) {

        Inst = Inst.toUpperCase();
        switch (type) {
            case 1:
                //error
                // msj = " <Error ," + Inst + "> [fila: " + fila + ",col: " + columna + "] {" + msj + "}";
                Errores.add(new ErroresAnalizador(fila, columna, Inst, msj, ErroresAnalizador.ErrorTipo.Semantico));
                break;
            case 2:
                //msj
                msj = " (MSJ," + Inst + ") [fila: " + fila + ",col: " + columna + "] {" + msj + "}";
                break;
            case 3:
                //print
                msj = "(print) <f: " + fila + " , c: " + columna + ">   " + msj;
                break;
            default:
                break;
        }
        if (type != 1) {
            this.msj_Out.add(msj);
        }
    }
    //-------------------------------- INICIAL -------------------------------

    @Override
    public Object visitStart(GramaticaParser.StartContext ctx) {
        //        System.out.println("    <start> "+obj);
        if (ctx.subroutine() != null) {
            for (GramaticaParser.SubroutineContext sc : ctx.subroutine()) {
                visitSubroutine(sc);
            }
        }
        if (ctx.function() != null) {
            for (GramaticaParser.FunctionContext functionContext : ctx.function()) {
                visitFunction(functionContext);
            }
        }

        visitPrograma(ctx.programa());
        return this.msj_Out;
    }

    @Override
    public Object visitPrograma(GramaticaParser.ProgramaContext ctx) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();

        if ((ctx.prog == null) || (ctx.prog_end == null)) {
            addMsj(1, "Programa", "el programa debe contener un nombre valido", fila, columna);
            return new Error();
        } else if (!ctx.prog.getText().equals(ctx.prog_end.getText())) {
            addMsj(1, "Programa", "El nombre del programa no coincide", fila, columna);
            return new Error();
        }

        for (GramaticaParser.InstruccionesContext instruccionesContext : ctx.instrucciones()) {
            visit(instruccionesContext);
        }
        return true;
    }

    //-------------------------------- PRIMITIVOS -------------------------------
    @Override
    public Object visitEnteroExpr(GramaticaParser.EnteroExprContext ctx) {
        int retorno = Integer.valueOf(ctx.getText());
        return retorno;
    }

    @Override
    public Object visitStrExpr(GramaticaParser.StrExprContext ctx) {
        return String.valueOf(ctx.getText());
    }

    @Override
    public Object visitRealExpr(GramaticaParser.RealExprContext ctx) {
        return Double.parseDouble(ctx.getText());
    }

    @Override
    public Object visitBoolExpr(GramaticaParser.BoolExprContext ctx) {
        if (ctx.b.getText().equals("true")) {
            return true;
        }
        return false;
    }

    @Override
    public Object visitCharExpr(GramaticaParser.CharExprContext ctx) {
        String str = (String) ctx.CHAR().getText();
        str = str.replace("\"", "");
        return str.charAt(0);
    }

    @Override
    public Object visitParenExpr(GramaticaParser.ParenExprContext ctx) {
        return visit(ctx.expr());
    }

    //-------------------------------- Unarios -------------------------------
    @Override
    public Object visitUnaryMinusExpr(GramaticaParser.UnaryMinusExprContext ctx) {
        Object obj = visit(ctx.expr());
        if (obj instanceof Integer) {
            obj = -1 * (Integer) obj;
            return obj;
        } else if (obj instanceof Double) {
            obj = -1 * (Double) obj;
            return obj;
        }
        addMsj(1, "Unary_minus", "error de tipos, no es de tipo Int/Double", ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
        return new Error();
    }

    @Override
    public Object visitNotExpr(GramaticaParser.NotExprContext ctx) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();

        Object obj = visit(ctx.expr());
        if (obj instanceof Boolean) {
            return !(Boolean) obj;
        }
        addMsj(1, "Not", "se intenta negar una exprecion no booleana", fila, columna);
        return new Error();
    }
    //--------------------------- Relacionales ------------------------------------

    @Override
    public Object visitRelacionales_Expr(GramaticaParser.Relacionales_ExprContext ctx) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();

        Object izq = visit(ctx.left);
        Object der = visit(ctx.right);
        String operador = ctx.op.getText();
        //1. comprobamos que tanto izq como der no sean Error y cuenten con un valor bool valido
        if ((izq instanceof Error) || (der instanceof Error)) {
            addMsj(1, "Relacional", "Un operador es igual a Error(), (no Boolean)", fila, columna);
            return new Error();
        }

        //2. Creamos nuestro objeto Relacionales para validar sus hijos 
        Relacionales r = new Relacionales(izq, der, operador);
        return r.validar(msj_Out, fila, columna);//retornar la validacion 
    }

    //--------------------------- Operaciones matematicas ------------------------------------
    @Override
    public Object visitOpExpr(GramaticaParser.OpExprContext ctx) {
        String operacion = ctx.op.getText();
        Object izq = visit(ctx.left);
        Object der = visit(ctx.right);

        if ((izq instanceof Integer) && (der instanceof Integer)) {
            switch (operacion) {
                case "*":
                    return (Integer) izq * (Integer) der;
                case "/":
                    if ((Integer) der == 0) {
                        addMsj(1, "Op_Expr", "Operacion: [ " + izq.toString() + " " + operacion + " " + der.toString() + " ] no valida, \"X/0\"", ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
                        return new Error();
                    }
                    return (Integer) izq / (Integer) der;
                case "+":

                    return (Integer) izq + (Integer) der;
                case "-":
                    return (Integer) izq - (Integer) der;
                case "**":
                    int base = 1;
                    for (int i = 0; i < (Integer) der; i++) {
                        base = (Integer) izq * base;
                    }
                    return base;

            }//int y int
        }

        if (((izq instanceof Integer) || (izq instanceof Double)) && (((der instanceof Integer) || (der instanceof Double)))) {
            Double auxIzq = Double.valueOf(izq.toString());
            Double auxDer = Double.valueOf(der.toString());
            switch (operacion) {
                case "*":
                    return auxIzq * auxDer;
                case "/":
                    if (auxDer == 0) {
                        addMsj(1, "Op_Expr", "Operacion: [ " + izq.toString() + " " + operacion + " " + der.toString() + " ] no valida, \"X/0\"", ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
                        return new Error();
                    }
                    return auxIzq / auxDer;
                case "+":
                    return auxIzq + auxDer;
                case "-":
                    return auxIzq - auxDer;
                case "**":
                    Double base = 1.0;
                    for (int i = 0; i < auxDer.intValue(); i++) {
                        base = auxIzq * base;
                    }
                    return base;
            }
        }//double y double
        addMsj(1, "Op_Expr", "Operacion: [ " + izq.toString() + " " + operacion + " " + der.toString() + " ] no valida", ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
        return new Error();
    }

    //----------------------------  DECLARACION-----------------------------------
    @Override
    public Object visitDeclaration(GramaticaParser.DeclarationContext ctx) {
//        if (!is3d) (modo de 3 direcciones)
        Entorno ent = pila_Ent.peek();
        for (GramaticaParser.Declaration2Context declaration2Context : ctx.declaration2()) {
            Declaracion dec = (Declaracion) visitDeclaration2(declaration2Context);
            String primerId = dec.getId();
            if (ent.getTablaSimbolo().containsKey(primerId)) {
                Simbolo sim = ent.getTablaSimbolo().get(primerId);
                //                generar codigo c3d de las expresiones -. int :: var = 58 +7/3+9
                //                t1 = 58 * 7
                //                t2 = t1 / 3 
                //                t3 = t2 + 9 //t4 = 12 + 2
                //                P = t4
                //                STACK[P] = t3 ;  -> 5 , 0 , 1
                //DECLARACION DEL DATO EN C3D
                c3d.codigo3d.add("  //Declaracion int");
                c3d.codigo3d.add(c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + sim.getPosicion() + " ;");//t4
                c3d.codigo3d.add("P = " + c3d.lastTemporal() + ";");
                if ((sim.getValor() instanceof Integer) || (sim.getTipo().equals("integer"))) {
                    c3d.codigo3d.add("STACK[(int)P] = " + sim.getValor().toString() + ";\n");
                } else if (sim.getValor() instanceof Character) {
                    int ch = (Character) sim.getValor();
                    c3d.codigo3d.add("STACK[(int)P] = " + ch + ";\n");
                } else if (sim.getValor() instanceof Boolean) {
                    c3d.codigo3d.add("STACK[(int)P] = " + ((Boolean) sim.getValor() ? "1" : "0") + " ;\n");
                }

            }
        }//for
        return true;
    }//declaracion

    @Override
    public Object visitDeclaration2(GramaticaParser.Declaration2Context ctx) {
        if (ctx.expr() != null) {
            return new Declaracion(ctx.ID().getText(), visit(ctx.expr()));
        }
        return new Declaracion(ctx.ID().getText(), null);
    }

    //---------------------------ASIGNACION ------------------------------------
    @Override
    public Object visitAsignacion_primitivos(GramaticaParser.Asignacion_primitivosContext ctx) {
        String id = ctx.ID().getText();
        Entorno ent = pila_Ent.peek();
        //1. comprobar que el ID exista
        if (ent.existe_simbolo(id)) {//Ns existe
            Simbolo sim = ent.Buscar(id);
            if (!(sim.getValor() instanceof Funciones)) {
                //DECLARACION DEL DATO EN C3D
                c3d.codigo3d.add("  //Asignacion");
                c3d.codigo3d.add(c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + sim.getPosicion() + " ;");
                c3d.codigo3d.add("P = " + c3d.lastTemporal() + " ;");

                if (sim.getValor() instanceof Boolean) {
                    c3d.codigo3d.add("STACK[(int)P] = " + ((Boolean) sim.getValor() ? "1" : "0") + " ;\n");
                } else {
                    c3d.codigo3d.add("STACK[(int)P] =  " + sim.getValor().toString() + " ;\n");
                }
            }
        }
        return true;
    }

    //------------------------  Block ---------------------------------------
    @Override
    public Object visitBlock(GramaticaParser.BlockContext ctx) {
        Entorno block = null;
        if (!is3d) {
            block = new Entorno(pila_Ent.peek());
            pila_Ent.peek().setSiguiente(block);
        } else {
            block = pila_Ent.peek().getSiguiente();
        }
        pila_Ent.push(block);
        visit(ctx.linstrucciones());
        pila_Ent.pop();
        return true;
    }

    //-------------------------- type -------------------------------------
    @Override
    public Object visitType(GramaticaParser.TypeContext ctx) {
        return ctx.getText();
    }
    //-----------------------  ID ----------------------------------------

    @Override
    public Object visitIdExpr(GramaticaParser.IdExprContext ctx) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();

        Entorno ent = pila_Ent.peek();
        String id = ctx.ID().getText();
        Simbolo sim = ent.Buscar(id);
        if (sim != null) {
            return sim.getValor();
        }
        addMsj(1, "id_epxr", "La variable \'" + id + "\", no existe ", fila, columna);
        return new Error();
    }

    //------------------------------ print ---------------------------------
    @Override
    public Object visitPrint(GramaticaParser.PrintContext ctx) {
        String msj = "";
        Object obj = null;
        Entorno ent = pila_Ent.peek();
        for (GramaticaParser.Estado_exprContext estado_exprContext : ctx.estado_expr()) {
            //            Simbolo sim = ent.Buscar(msj)
            String id = estado_exprContext.expr().getText();
            Simbolo sim = ent.Buscar(id);
            obj = visitEstado_expr(estado_exprContext);

            if (sim != null) {//es un ID(int o char)

                if (obj instanceof Arreglo) {
                    Arreglo arr = (Arreglo) obj;
                    c3d.codigo3d.add("//Print Arreglo " + arr.getCant_dimenciones() + " dim Float: " + id + ";");
                    c3d.codigo3d.add("  " + c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + sim.posicion + ";");
                    c3d.codigo3d.add("  P = " + c3d.lastTemporal() + ";");
                    c3d.codigo3d.add("  H = STACK[(int)P] ;");
                    String tn = c3d.lastTemporal();

                    //si es de una dim
                    if (arr.getCant_dimenciones() == 1) {
                        String metodo = "   imprimir_arr_float_una_dim() ;\n";
                        if (arr.getTipo().equals("character")) {
                            metodo = "  imprimir_arr_char_una_dim() ;\n";
                        }
                        c3d.codigo3d.add(metodo);
                    } else {
                        // si es de dos dim
                        int i = arr.getSize_unaDim();
                        int j = arr.getSize_dosDim();
                        c3d.codigo3d.add("  tprint_i = " + i + " ;");
                        c3d.codigo3d.add("  tprint_j = " + j + " ;");

                        String metodo = "   imprimir_arr_float_dos_dim() ;\n";
                        if (arr.getTipo().equals("character")) {
                            metodo = "  imprimir_arr_char_dos_dim() ;\n";
                        }
                        c3d.codigo3d.add(metodo);
                    }
                } else if ((obj instanceof Integer) || (sim.getTipo().equals("integer")) || (sim.getTipo().equals("logical"))) {
                    c3d.codigo3d.add("  //Print ID int");
                    c3d.codigo3d.add(c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + sim.posicion + ";");
                    c3d.codigo3d.add("P = " + c3d.lastTemporal() + ";");
                    c3d.codigo3d.add("imprimir_var_int();\n");
                } else if ((obj instanceof Character) || (sim.getTipo().equals("character"))) {
                    c3d.codigo3d.add("  //Print ID char");
                    c3d.codigo3d.add(c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + sim.posicion + ";");
                    c3d.codigo3d.add("P = " + c3d.lastTemporal() + ";");
                    c3d.codigo3d.add("imprimir_var_char();\n");
                }

            } else {// es una dato 
                if ((obj instanceof Integer)) {
                    c3d.codigo3d.add("  //Print dato");
                    c3d.codigo3d.add("  tprintInt = " + obj.toString() + ";");
                    c3d.codigo3d.add("  imprimir_int();\n");
                } else if (obj instanceof Character) {
                    c3d.codigo3d.add("  //Print dato");
                    c3d.codigo3d.add("  tprintInt = " + ((int) obj) + ";");
                    c3d.codigo3d.add("  imprimir_char();\n");
                } else if (obj instanceof String) {
                    crearSTR_c3d(obj.toString().replace("\'", ""));
                }
            }

        }//for
        return true;
    }

    @Override
    public Object visitEstado_expr(GramaticaParser.Estado_exprContext ctx) {
        return visit(ctx.expr());
    }

    //------------------------ARREGLOS simples---------------------------------------
    @Override
    public Object visitDeclaracion_arreglos1(GramaticaParser.Declaracion_arreglos1Context ctx) {
        String id = ctx.ID().getText();
        Entorno ent = pila_Ent.peek();
        Simbolo sim = ent.Buscar(id);
        if (sim != null) {
            Arreglo arr = (Arreglo) sim.getValor();

            //en la posicion P del stack,guardamos en el Heap el incio del vector
            c3d.codigo3d.add("  //Declaracion Arreglo: " + id + ".dim()=" + arr.getCant_dimenciones());
            c3d.codigo3d.add(c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + sim.getPosicion() + " ;");//t4
            c3d.codigo3d.add("P = " + c3d.lastTemporal() + ";");
            c3d.codigo3d.add("STACK[(int)P] = H ;");
            if (arr.getCant_dimenciones() == 1) {
                for (int i = 0; i < arr.getSize_unaDim(); i++) {
                    Object obj = arr.get_dato_una_dim(i);
                    String dato = ((obj == null) ? "0" : obj.toString());
                    if (obj instanceof Character) {
                        dato = (int) dato.charAt(0) + "";
                    }
                    c3d.codigo3d.add("  HEAP[(int)H] = " + dato + " ;");
                    c3d.codigo3d.add("  H = H + 1 ;");
                }
            } else {//si es de dos dim
                //row major    ->  dato = m[i* (catnCols) + j]
                int imax = arr.getSize_unaDim();
                int jmax = arr.getSize_dosDim();
                for (int i = 0; i < imax; i++) {
                    for (int j = 0; j < jmax; j++) {
                        Object obj = arr.get_dato_dos_dim(i, j);
                        int dato = (obj == null) ? 0 : Integer.parseInt(obj.toString());
                        c3d.codigo3d.add("  HEAP[(int)H] = " + dato + " ;");
                        c3d.codigo3d.add("  H = H + 1 ;");
                    }//j
                }//i

            }//else
            c3d.codigo3d.add("  HEAP[(int)H] = -1 ;   //fin del arreglo");//fin del arreglo
            c3d.codigo3d.add("  H = H + 1 ;\n");
        }//if

        return true;
    }

    @Override
    public Object visitAsignacion_arreglos_unaDim(GramaticaParser.Asignacion_arreglos_unaDimContext ctx) {
        String id = ctx.ID().getText();
        Entorno ent = pila_Ent.peek();
        Simbolo sim = ent.Buscar(id);

        if (sim != null) {
            Arreglo arr = (Arreglo) sim.getValor();

            //en la posicion P del stack,guardamos en el Heap el incio del vector
            c3d.codigo3d.add("  //Asignacion Arreglo: " + id + ".dim()=" + arr.getCant_dimenciones());
            c3d.codigo3d.add(c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + sim.getPosicion() + " ;");//t4
            c3d.codigo3d.add("P = " + c3d.lastTemporal() + ";");
            c3d.codigo3d.add("H = STACK[(int)P] ;");//posicion inicial del arreglo en STACK

            //recorremos el arreglo en el STACK
            for (int i = 0; i < arr.getSize_unaDim(); i++) {
                Object obj = arr.get_dato_una_dim(i);
                String dato = ((obj == null) ? "0" : obj.toString());
                if (obj instanceof Character) {
                    dato = (int) dato.charAt(0) + "";
                }
                c3d.codigo3d.add("  HEAP[(int)H] = " + dato + " ;");
                c3d.codigo3d.add("  H = H + 1 ;");
            }
            c3d.codigo3d.add("  HEAP[(int)H] = -1 ;   //fin del arreglo");//fin del arreglo
            c3d.codigo3d.add("  H = H + 1 ;\n");

        }//if

        return true;
    }

    @Override
    public Object visitAsignacion_arreglos_unaDim2(GramaticaParser.Asignacion_arreglos_unaDim2Context ctx) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();

        String id = ctx.ID().getText();
        Entorno ent = pila_Ent.peek();

        //1.si, No existe el simbolo, retorna Error()
        if (!ent.existe_simbolo(id)) {
            addMsj(1, "Arreglos_asignacion2", "El arreglo \"" + id + "\", No ha sido declarado ", fila, columna);
            return new Error();
        }

        //2. capturando el arreglo
        Simbolo sim = ent.Buscar(id);
        if (sim == null) {
            addMsj(1, "Arreglos_asignacion2", "La variable \"" + id + "\",No Existe", fila, columna);
            return new Error();
        }

        Object objArr = sim.getValor();

        //3. validando que sea de tipo Arreglo
        if (!(objArr instanceof Arreglo)) {
            addMsj(1, "Arreglos_asignacion2", "la variable \"" + id + "\", No es de tipo Arreglo", fila, columna);
            return new Error();
        }

        //3.1.    comprobamos, que el arreglo sea unicamente de una dimension
        if (objArr instanceof Arreglo) {
            if (((Arreglo) objArr).getCant_dimenciones() != 1) {
                addMsj(1, "Arreglos_asignacion2", "El arreglo \"" + id + "\" tiene mas de una dimension", fila, columna);
                return new Error();
            }
        }

        //4.comprobamos que el arreglo cuenta con la posicion a asignar.
        Object objPosicion = visit(ctx.posicion);
        //4.1 se valida que la posicion solo sea int
        if (!(objPosicion instanceof Integer)) {
            addMsj(1, "Arreglos_asignacion2", "El arreglo  \"" + id + "\", usa una posicion que No es Int \"" + objPosicion.toString() + "\"", fila, columna);
            return new Error();
        }

        //4.2     Comprobamos que la posicion exista
        Arreglo arr = (Arreglo) objArr;

        int sizeArr = arr.getL_una_dimension().size();
        if ((Integer) objPosicion > sizeArr) {
            addMsj(1, "Arreglos_asignacion2", "El arreglo \"" + id + "\", no cuenta con la posicion \"" + objPosicion.toString() + "\"  ,  " + id + ".size()=" + sizeArr, fila, columna);
            return new Error();
        }

        //5 comprobamos que el dato a guardar, No sea del tipo del arreglo (arreglo.tipo != dato.tipo)
        Object objDato = visit(ctx.dato);

        if (!(comprobacion_de_tipos(sim.getTipo(), objDato))) {
            addMsj(1, "Arreglos_asignacion2", "El tipo de dato a guardar es diferente al del \"arreglo: " + id + ", tipo: " + sim.getTipo() + "\"", fila, columna);
            return new Error();
        }

        //6. si los tipos coinciden, Agregamos el dato al arreglo
        //se suma -1 a la pisicion por que fortran asi lo demanda
        arr.getL_una_dimension().set((Integer) objPosicion - 1, objDato);

        //7. Actualizar el arreglo en tabla de simbolos
        sim.setValor(arr);

        //8. Agregar sim al entorno
        ent.Actualizar_Simbolo(id, sim);
        return true;
    }

    @Override
    public Object visitAsignacion_arreglos_DosDim(GramaticaParser.Asignacion_arreglos_DosDimContext ctx) {
        //  ID '[' i=expr ']''[' j=expr ']' '=' dato=expr
        String id = ctx.ID().getText();
        Entorno ent = pila_Ent.peek();
        Simbolo sim = ent.Buscar(id);

        if (sim != null) {
            Arreglo arr = (Arreglo) sim.getValor();
            int i = (int) visit(ctx.exp_izq);
            int j = (int) visit(ctx.exp_der);
            int nCol = arr.getSize_dosDim();

            Object dato = visit(ctx.dato);

            int rowMajor = (i * nCol) + j;//posicion relativa al inicio del arreglo

            c3d.codigo3d.add("  //Asignacion Arreglo: " + id + ".dim()=" + arr.getCant_dimenciones());
            c3d.codigo3d.add(c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + sim.getPosicion() + " ;");//t4
            c3d.codigo3d.add("P = " + c3d.lastTemporal() + ";");
            c3d.codigo3d.add("H = STACK[(int)P] ;");//posicion inicial del arreglo en STACK
            c3d.codigo3d.add("H = H + " + rowMajor + " ;//posicion relativa, dada por calculo de rowMajor");
            c3d.codigo3d.add("STACK[(int)H] = " + dato + " ;\n");
        }//if
        return true;
    }//

    @Override
    public Object visitL_expr(GramaticaParser.L_exprContext ctx
    ) {
        return visit(ctx.expr());
    }

    //------------------------ARREGLOS dinamicos---------------------------------------
    @Override
    public Object visitCant_arrDinamic_dim(GramaticaParser.Cant_arrDinamic_dimContext ctx
    ) {
        int cont = 0;
        if (ctx.una != null) {
            cont++;
        }
        if (ctx.dos != null) {
            cont++;
        }
        return cont;
    }

    @Override
    public Object visitDeclaracion_arr_dinamicos(GramaticaParser.Declaracion_arr_dinamicosContext ctx
    ) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();

        int cant = (Integer) visitCant_arrDinamic_dim(ctx.cant_arrDinamic_dim());
        String id = ctx.ID().getText();
        String tipo = ctx.type().getText();

        //1 validando que concuerde la escritura.
        if (cant == 0) {
            addMsj(1, "Dec_Arr_Dinamicos", "Hace falta el parametro, que indica la dimension", fila, columna);
            return new Error();
        }

        //2. creamos el arreglo
        Arreglo arr = new Arreglo(tipo, true, cant);

        Entorno ent = pila_Ent.peek();
        //3 verificamos que la var id no exista
        if (ent.Buscar(id) != null) {
            addMsj(1, "Dec_Arr_Dinamicos", "la varaiable \"" + id + "\"  ya fue declarada", fila, columna);
            return new Error();
        }

        //4. agregamos al entorno
        Simbolo sim = new Simbolo(tipo, arr);
        ent.nuevoSimbolo(id, sim);

        return true;
    }

    @Override
    public Object visitAllocate(GramaticaParser.AllocateContext ctx
    ) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();

        String id = ctx.ID().getText();
        Entorno ent = pila_Ent.peek();
        Simbolo sim = ent.Buscar(id);

        //1. existe el arreglo
        if (sim == null) {
            addMsj(1, "Allocate", "No existe el Arreglo \"" + id + "\"", fila, columna);
            return new Error();
        }
        //2. comprobar que el simbolo, sea de tipo arreglo
        Object objArr = sim.getValor();
        if (!(objArr instanceof Arreglo)) {
            addMsj(1, "Allocate", "Error de tipos, la variable \"" + id + ".tipo() = " + sim.getTipo() + ",  No es de tipo Arreglo", fila, columna);
            return new Error();
        }

        Arreglo arr = (Arreglo) sim.getValor();
        //3. comprobar que el arreglo no tenga una dimension asignada
        if (arr.tieneDimension()) {
            addMsj(1, "Allocate", "El arreglo ya cuenta con dimenciones (ya se activo un Allocate)", fila, columna);
            return new Error();
        }

        //4. validar  que las expr sean tipo entero
        Object objUno = visit(ctx.una);
        Object objDos = null;
        if (ctx.dos != null) {
            objDos = visit(ctx.dos);
        }

        if (!(objUno instanceof Integer)) {
            addMsj(1, "Allocate", "la uno=expr retorna un valor diferente de entero, id:  \"" + id + ".tipo()=" + getTipo_obj(objUno) + "\"", fila, columna);
            return new Error();
        }
        if (!((objDos instanceof Integer) || (ctx.dos == null))) //5 asignamos la dimension
        {
            addMsj(1, "Allocate", "la dos=expr retorna un valor diferente de entero, id:  \"" + id + ".tipo()=" + getTipo_obj(objDos) + "\"", fila, columna);
            return new Error();
        }

        //5. validar que sea de la dimension correcta 
        int cont = 0;
        if (ctx.una != null) {
            cont++;
        }
        if (ctx.dos != null) {
            cont++;
        }
        if (arr.getCant_dimenciones() != cont) {
            addMsj(1, "Allocate", "Error de dimensiones arrDim=(" + arr.getCant_dimenciones() + "), se intenta asignar dim=(" + cont + ")", fila, columna);
            return new Error();
        }

        //6 actualizamos el simbolo
        arr.Establecer_limites_de_Arreglo_dinamico(objUno, objDos);
        sim.setValor(arr);
        ent.Actualizar_Simbolo(id, sim);

        return true;
    }

    @Override
    public Object visitDeallocate(GramaticaParser.DeallocateContext ctx
    ) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();

        if (ctx.ID() == null) {
            addMsj(1, "Deallocate", "El id no existe, o es null", fila, columna);
            return new Error();
        }

        //1.    comprobamos que el id exista
        String id = ctx.ID().getText();

        Entorno ent = pila_Ent.peek();
        Simbolo sim = ent.Buscar(id);

        if (sim == null) {
            addMsj(1, "Deallocate", "La variable:\"" + id + "\", no existe", fila, columna);
            return new Error();
        }
        //2.   comprobamos que la variable sea de tipo arreglo 
        Object objArr = sim.getValor();
        if (!(objArr instanceof Arreglo)) {
            addMsj(1, "Deallocate", "La variable id: \"" + id + ".tipo()=" + sim.getTipo() + " \", No es tipo Arreglo", fila, columna);
            return new Error();
        }

        //3.    comprobar error, sobre deallocate 
        Arreglo arr = (Arreglo) objArr;
        if (!arr.deallocate()) {
            addMsj(1, "Deallocate", "No se puede, ejecutar Deallocate, por que el Arreglo no tiene un Allocate", fila, columna);
            return new Error();
        }

        //4.    Guardamos cambios
        sim.setValor(arr);
        ent.Actualizar_Simbolo(id, sim);

        return true;
    }
    //------------------------      IF      ---------------------------------------

    @Override
    public Object visitElseIf_(GramaticaParser.ElseIf_Context ctx) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();

        //1 comprobar que la expr=bool
        Object objElseIf = visit(ctx.expr());
        if (!(objElseIf instanceof Boolean)) {
            addMsj(1, "Else If", "La exprecion no es de tipo Boolean,   expr.tipo()=\"" + getTipo_obj(objElseIf) + "\"", fila, columna);
            return new Error();
        }//if

        String L_if = c3d.generarEtiqueta();//L2
        String l2 = c3d.generarEtiqueta();//L3
        String L_else = c3d.generarEtiqueta();//L3
        String tn = c3d.generarTemporal();

        //2. validamos que la expr = true/false, de cada else_if
        if ((Boolean) objElseIf) {
            //-------------------------------C3D--------------------------------------------------------
            String c3dIf = "0";
            if ((Boolean) objElseIf) {
                c3dIf = "1";
            }
            c3d.codigo3d.add("  if ( " + c3dIf + " == 1 ) goto " + L_if + ";");
            c3d.codigo3d.add(" goto " + L_else + " ;");
            //----------------------------------------------------------------------------------------
            c3d.codigo3d.add("" + L_if + ":");
            //3. creamos el entorno del Else_if
            pila_Ent.push(new Entorno(pila_Ent.peek()));
            for (GramaticaParser.InstruccionesContext instruccionesContext : ctx.instrucciones()) {
                Object objResult = visitInstrucciones(instruccionesContext);
                if (objResult instanceof Exit) {
                    pila_Ent.pop();
                    return (Exit) objResult;
                } else if (objResult instanceof Cycle) {
                    pila_Ent.pop();
                    return (Cycle) objResult;
                }
            }//for
            //extraemos su entorno
            pila_Ent.pop();
            c3d.codigo3d.add("" + L_else + ":\n");
            return true;
        }//if
        return false;
    }

    @Override
    public Object visitElse_(GramaticaParser.Else_Context ctx
    ) {
        pila_Ent.push(new Entorno(pila_Ent.peek()));
        for (GramaticaParser.InstruccionesContext instruccionesContext : ctx.instrucciones()) {
            Object objResult = visitInstrucciones(instruccionesContext);
            if (objResult instanceof Exit) {
                pila_Ent.pop();
                return objResult;
            } else if (objResult instanceof Cycle) {
                pila_Ent.pop();
                return objResult;
            }
        }
        //extraemos su entorno
        pila_Ent.pop();
        return true;
    }

    @Override
    public Object visitIf_(GramaticaParser.If_Context ctx) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();

        Object objExpr = visit(ctx.expr());
        //0. validando IF

        if (!(objExpr instanceof Boolean)) {
            addMsj(1, "If", "La exprecion no es de tipo Boolean,   expr.tipo()=\"" + getTipo_obj(objExpr) + "\"", fila, columna);
            return new Error();
        }
        String L_if = c3d.generarEtiqueta();//L2
        String L_elseIf = c3d.generarEtiqueta();//L3
        String L_else = c3d.generarEtiqueta();//L3
        String L_out = c3d.generarEtiqueta();//L3
        //1. comprobando el valor booleano de la expr
        if ((Boolean) objExpr) {
            //--------------------------------------C3D-------------------------------------------------------------
            String c3dIf = "0";
            if ((Boolean) objExpr) {
                c3dIf = "1";
            }
            c3d.codigo3d.add("// SENTENCIA IF");
            c3d.codigo3d.add("  if ( " + c3dIf + " == 1 ) goto " + L_if + ";");
            if (ctx.elseIf_() != null) {
                //c3d.codigo3d.add(" goto " + L_elseIf + " ;//else if");
            } else if (ctx.else_() != null) {
                //c3d.codigo3d.add(" goto " + L_else + " ;//else");
            }
            c3d.codigo3d.add(" goto " + L_out + " ;//solo if");
            //----------------------------------------------------------------------------------------------------

//            c3d.codigo3d.add(c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + sim.getPosicion() + " ;");//t4
            // si es verdad, comprobamos las inst del if
            //2.    Creamos un nuevo entorno solo para IF
            c3d.codigo3d.add(L_if + ":");
            pila_Ent.push(new Entorno(pila_Ent.peek()));
            for (GramaticaParser.InstruccionesContext instruccionesContext : ctx.instrucciones()) {
                Object objResult = visitInstrucciones(instruccionesContext);
                if (objResult instanceof Exit) {
                    pila_Ent.pop();
                    return objResult;
                } else if (objResult instanceof Cycle) {
                    pila_Ent.pop();
                    return objResult;
                }
            }
            //3. extraigo el entorno que cree.
            pila_Ent.pop();
            c3d.codigo3d.add("" + L_out + "://if_out");
            return true;
        }//if

        //4.    si no entro al if,  comprobando, si existe Else_if
        if (ctx.elseIf_() != null) {
            // recorriendo el listado de Else_if
            c3d.codigo3d.add("// SENTENCIA eLSE IF");
            c3d.codigo3d.add("  " + L_elseIf + ":");
            Etiquetas_if.push(L_elseIf);

            for (GramaticaParser.ElseIf_Context elseIf_Context : ctx.elseIf_()) {
                Object objResult = visitElseIf_(elseIf_Context);
                if ((Boolean) objResult) {//si entro al else, nos salimos del if
                    return true;
                } //si existe algun error en las expr
                else if (objResult instanceof Error) {
                    return new Error();
                } else if (objResult instanceof Exit) {
                    return objResult;
                } else if (objResult instanceof Cycle) {
                    return objResult;
                }

            }//for
        }//if

        //4. comprobamos el else, por que no entro a ningun if o else_if
        c3d.codigo3d.add("// SENTENCIA eLSE ");
        c3d.codigo3d.add("  " + L_else + ":");
        if (ctx.else_() != null) {
            //recorremos el listado de Else
            Object objResult = visitElse_(ctx.else_());
            if (objResult instanceof Exit) {
                return objResult;
            } else if (objResult instanceof Cycle) {
                return objResult;
            }
        }//if
        c3d.codigo3d.add("  " + L_out + ":");

        return true;
    }

    //--------------------------   DO   -------------------------------------
    @Override
    public Object visitDo_(GramaticaParser.Do_Context ctx) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();
        //  NOTA: solo se puede asignar variables de tipo int (no se puede declarar variables)
        //  NOTA2:  DO es el analogo a FOR
        String etiqueta = "";
        //ETIQUETAS
        //comprobando si el ciclo tiene una etiqueta
        if ((ctx.etiquete != null) || (ctx.etiqueta_end != null)) {
            if ((ctx.etiquete != null) && (ctx.etiqueta_end != null)) {
                //preguntamos si las etiquetas son iguales
                if (ctx.etiquete.getText().equals(ctx.etiqueta_end.getText())) {
                    etiqueta = ctx.etiquete.getText();
                } else {
                    addMsj(1, "Do", "el ciclo Do, cuenta con etiquetas no iguales (" + ctx.etiquete.getText() + " != " + ctx.etiqueta_end.getText() + ")", fila, columna);
                    return new Error();
                }
            } else if (ctx.etiquete == null) {
                addMsj(1, "Do", "No existe la etiqueta No.1 (Inicio)", fila, columna);
                return new Error();
            } else if (ctx.etiqueta_end == null) {
                addMsj(1, "Do", "No existe la etiqueta No.2 (End) ", fila, columna);
                return new Error();
            }

        }//if
        //-------------
        String id = "";
        if (ctx.asignacion() != null) {
            id = ctx.asignacion().getStart().getText();
        }

        //0.    crear el entorno del ciclo
        pila_Ent.push(new Entorno(pila_Ent.peek()));

        //1.    Ejecutamos la asignacion,
        Object objAsig = visit(ctx.asignacion());
        if (objAsig instanceof Error) {
            return new Error();
        }

        //2.    capturamos el limite/cant de ciclos, debe ser tipo int
        Object objFin = visit(ctx.fin);
        if (!(objFin instanceof Integer)) {
            addMsj(1, "Do", "El tipo del limite superior, del ciclo Do, es diferente de Integer", fila, columna);
            return new Error();
        }
        //3.    Capturamos el incremento
        Object objInc = visit(ctx.incremento);
        if (!(objInc instanceof Integer)) {
            addMsj(1, "Do", "El tipo del incremento, del ciclo Do, es diferente de Integer", fila, columna);
            return new Error();
        }

        //4.    buscamos el valor de la variable guardada
        Entorno ent = pila_Ent.peek();
        Simbolo sim = ent.Buscar(id);
        sim.setValor(0);
        int limite_inferior = (Integer) sim.getValor();

        //5.    Parametros de correctos
        int limite_superior = (Integer) objFin;
        int incremento = (Integer) objInc;

        Object objResult = new Object();
        //ciclo analogo al Do.
        int i = limite_inferior;
        int panic = 0;
        String i_c3d = c3d.generarTemporal();
        String top = c3d.generarTemporal();
        String inc = c3d.generarTemporal();

        c3d.codigo3d.add("  //Ciclo do: ");
        c3d.codigo3d.add(i_c3d + " = " + 0 + " ;");
        c3d.codigo3d.add(top + " = " + limite_superior + " ;");
        c3d.codigo3d.add(inc + " = " + incremento + " ;");

        Boolean acceso = true;
        String L1 = c3d.generarEtiqueta();//L1
        String L2 = c3d.generarEtiqueta();//L2

        c3d.codigo3d.add("  if ( " + i_c3d + " <= " + top + " ) goto " + L1 + " ;");    // if (i <= limite_superior)
        c3d.codigo3d.add("  goto " + L2 + " ;");
        for (i = limite_inferior; i <= limite_superior; i += incremento) {

            panic++;
            if (panic > limite_panic_doWhile) {
                addMsj(1, "Do", "Salida de panico,  solo se admiten n=" + limite_panic_doWhile + " iteraciones. (se puede cambiar en el constructor de MyVisitor)", fila, columna);
                break;
            }

            if (acceso) {
                c3d.codigo3d.add(L1 + ":");
                acceso = !acceso;
            }

            //recorremos la lista de instrucciones
            for (GramaticaParser.InstruccionesContext instruccionesContext : ctx.instrucciones()) {
                objResult = visitInstrucciones(instruccionesContext);
                //comprueba en las instrucciones, si aparece un Exit( si lo hace, sale del ciclo
                if (objResult instanceof Exit) {
                    String eti_exit = ((Exit) objResult).getEtiqueta();
                    if (!(eti_exit.equals(""))) {
                        if (!etiqueta.equals(eti_exit)) {
                            pila_Ent.pop();
                            return objResult;
                        }
                    }
                    break;
                } else if (objResult instanceof Cycle) {
                    String eti_cycle = ((Cycle) objResult).getEtiqueta();
                    if (!(eti_cycle.equals(""))) {
                        if (!etiqueta.equals(eti_cycle)) {
                            pila_Ent.pop();
                            return objResult;
                        }
                    }
                    break;
                }
            }//for
            //validando Exit()
            if (objResult instanceof Exit) {
                break;
            }
            //6.    actualizamos los parametros del ciclo Do
            // si la variable id es modificada en una instruccion, actualizamos el valor a i,
            //si no solo i mas el incremento
            Simbolo sAux = ent.Buscar(id);
            int i_aux = (Integer) sAux.getValor();
            if (i_aux != i) {
                i = i_aux;
                c3d.codigo3d.add(c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + sim.getPosicion() + " ;");//t4
                c3d.codigo3d.add("P = " + c3d.lastTemporal() + " ;");
                c3d.codigo3d.add("STACK[(int)P] = " + i + " ;");
            } else {
                sim.setValor(i + incremento);
                ent.Actualizar_Simbolo(id, sim);
                c3d.codigo3d.add(i_c3d + " = " + i_c3d + " + 1 ;    //incremento");
            }

            //Simbolo simAux = new Simbolo(sim.getTipo(), i + incremento);
        }//for

        //7.    extraemos el ciclo del Do.
        pila_Ent.pop();
        c3d.codigo3d.add(L2 + ":\n");
        return true;
    }

    //----------------------------    DO WHILE  -----------------------------------
    @Override
    public Object visitDo_while(GramaticaParser.Do_whileContext ctx
    ) {
        int panic = 0;
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();
        //  NOTA:   el ciclo esta activo, si y solo si, expr==true

        String etiqueta = "";
        //ETIQUETAS
        //comprobando si el ciclo tiene una etiqueta
        if ((ctx.etiqueta != null) || (ctx.etiqueta_end != null)) {
            if ((ctx.etiqueta != null) && (ctx.etiqueta_end != null)) {
                //preguntamos si las etiquetas son iguales
                if (ctx.etiqueta.getText().equals(ctx.etiqueta_end.getText())) {
                    etiqueta = ctx.etiqueta.getText();
                } else {
                    addMsj(1, "Do_while", "el ciclo Do, cuenta con etiquetas no iguales (" + ctx.etiqueta.getText() + " != " + ctx.etiqueta_end.getText() + ")", fila, columna);
                    return new Error();
                }
            } else if (ctx.etiqueta == null) {
                addMsj(1, "Do_while", "No existe la etiqueta No.1 (Inicio)", fila, columna);
                return new Error();
            } else if (ctx.etiqueta_end == null) {
                addMsj(1, "Do_while", "No existe la etiqueta No.2 (End) ", fila, columna);
                return new Error();
            }

        }//if

        //1.    capturar la expr y validar que sea de tipo Boolean
        Object objExp = visit(ctx.expr());
        if (!(objExp instanceof Boolean)) {
            addMsj(1, "Do_while ", "la expresion no es de tipo Boolean,   expr.tipo()=" + getTipo_obj(objExp), fila, columna);
            return new Error();
        }

        do {
            //Creamos el entorno para ejecutar las instruccines
            pila_Ent.push(new Entorno(pila_Ent.peek()));
            //recorrer el listado de instrucciones
            Object objResult = new Object();
            for (GramaticaParser.InstruccionesContext instruccionesContext : ctx.instrucciones()) {
                objResult = visitInstrucciones(instruccionesContext);

                if (objResult instanceof Exit) {
                    String eti_exit = ((Exit) objResult).getEtiqueta();
                    if (!(eti_exit.equals(""))) {
                        if (!etiqueta.equals(eti_exit)) {
                            pila_Ent.pop();
                            return objResult;
                        }
                    }
                    break;
                } else if (objResult instanceof Cycle) {
                    String eti_cycle = ((Cycle) objResult).getEtiqueta();
                    if (!(eti_cycle.equals(""))) {
                        if (!etiqueta.equals(eti_cycle)) {
                            pila_Ent.pop();
                            return objResult;
                        }
                    }
                    break;
                }

            }//for
            //terminando el ciclo con "Exit"
            if (objResult instanceof Exit) {
                break;
            }
            pila_Ent.pop();

            //actualizar el valor de while
            objExp = visit(ctx.expr());
            panic++;
            if (panic > limite_panic_doWhile) {
                addMsj(1, "Do_while", "Salida de panico,  solo se admiten n=" + limite_panic_doWhile + " iteraciones.  (se puede cambiar en el constructor de MyVisitor)", fila, columna);
                break;
            }
        } while ((Boolean) objExp);

        return true;
    }

    //------------------------------------  EXIT    ---------------------------
    @Override
    public Object visitExit(GramaticaParser.ExitContext ctx
    ) {
        String etiqueta = "";
        if (ctx.ID() != null) {
            etiqueta = ctx.ID().getText();
        }
        return new Exit(etiqueta);
    }

    //---------------------------  CYCLE     ------------------------------------
    @Override
    public Object visitCycle(GramaticaParser.CycleContext ctx
    ) {
        String etiqueta = "";
        if (ctx.ID() != null) {
            etiqueta = ctx.ID().getText();
        }
        return new Cycle(etiqueta);
    }

    //---------------------------------------------------------------
    //---------------------------------------------------------------
    //-----------------------------    SUB-ROUTINE     ----------------------------------
    @Override
    public Object visitSubroutine(GramaticaParser.SubroutineContext ctx) {

        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();
        String inst = "Subroutine";

        //1.    capturar y comprobar el nombre de la Subroutina sea valido
        String nom = ctx.nom.getText();
        String nom_end = ctx.nom_end.getText();
        if (!nom.equals(nom_end)) {
            addMsj(1, inst, "El nombre de la Subroutine no coincide", fila, columna);
            return new Error();
        }

        //2.    comprobar que la variable no este guardada
        Simbolo sim = pila_Ent.peek().Buscar(nom);
//        if (sim != null) {
//            addMsj(1, inst, "No se puede crear. Ya existe una var con este nombre:" + nom + ".tipo()=" + sim.getTipo(), fila, columna);
//            return new Error();
//        }

        //3     creamos el entorno de la sub-rutina
        // pila_Ent.push(new Entorno(pila_Ent.peek()));
        Entorno ent = new Entorno(null);

        List<String> list_nom_param = new ArrayList<>();
        //4.    validamos y asignamos valores a un entorno "ent" propio de la sub rutina, 
        //(solo ejecutamos los parametros) 
        //si no tiene parametros saltamos estos if anidados
        if ((ctx.list_param() != null) && (ctx.declaracion_parametro_func() != null)) {
            //4.1 validamos que la cantidad de parametros sea igual a lista_declaraciones_parametros
            if (ctx.list_param().size() != ctx.declaracion_parametro_func().size()) {
                addMsj(1, inst, "La cantidad de parametros no concuerda, con la cantidad de asignaciones", fila, columna);
                return new Error();
            }
            //ejecutamos las listas de parametros y declaraciones

            for (int i = 0; i < ctx.list_param().size(); i++) {
                //4.2 comprobamos el nombre del parametro sea el mismo
                String param = ctx.list_param(i).ID().getText();
                String param_dec = ctx.declaracion_parametro_func(i).ID().getText();
                if (!(param.equals(param_dec))) {
                    addMsj(1, inst, "el nombre de los parametos no coinciden \"" + param + "!=" + param_dec + "\"", fila, columna);
                    return new Error();
                }
                // 4.3  si estan correcto, lo guardamos en el entorno

                String tipo = ctx.declaracion_parametro_func(i).type().getText();
                Simbolo nuevoSim = new Simbolo(tipo, agregar_valor_predeterminado(tipo));
                if (ctx.declaracion_parametro_func(i).list_param() != null) {
                    nuevoSim.setValor(new Arreglo(tipo));
                }

                ent.nuevoSimbolo(param, nuevoSim);
                list_nom_param.add(param);

            }//for

        } else if ((ctx.list_param() == null) && (ctx.declaracion_parametro_func() != null)) {
            addMsj(1, inst, "La subRut no cuenta con parametros declarados para asignarle tipo", fila, columna);
            return new Error();
        } else if ((ctx.list_param() != null) && (ctx.declaracion_parametro_func() == null)) {
            addMsj(1, inst, "La subRut no cuenta con asignaciones de tipos, para los parametros declarados", fila, columna);
            return new Error();
        }

        //3.    Creamos el objeto Subrountine
        Subroutine simSub = new Subroutine(ent, ctx.list_param(), ctx.declaracion_parametro_func(), ctx.instrucciones(), list_nom_param);
        Simbolo nuevoSim = new Simbolo("subrountine", simSub);
        pila_Ent.peek().nuevoSimbolo(nom, nuevoSim);
        addMsj(2, inst, "se agrego una nueva SubRutina: " + nom, fila, columna);
        return true;
    }

    @Override
    public Object visitCall(GramaticaParser.CallContext ctx
    ) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();
        String inst = "Call";

        //1.    capturar el nombre de la subrutina a llamar
        String id = ctx.ID().getText();

        //2.    Buscar si existe la funcion guardada y comprobar si es de tipo subrutina
        Entorno ent = pila_Ent.peek();
        Simbolo sim = ent.Buscar(id);
        if (sim == null) {
            addMsj(1, inst, "La sub-rutina : \"" + id + "\", No existe  ", fila, columna);
            return new Error();
        }
        if (!(sim.getValor() instanceof Subroutine)) {
            addMsj(1, inst, "La variable : \"" + id + ".tipo()=" + sim.getTipo() + " \", No es de tipo SubRutina", fila, columna);
            return new Error();
        }

        Subroutine sub = (Subroutine) sim.getValor();

//        Trabando con Call
//        4.  comprobar que la cantidad de parametros de la subrutina sean iguales a el de call
        if ((sub.getDeclaracion_param() != null) && (ctx.l_expr() != null)) {

            if (sub.getDeclaracion_param().size() != ctx.l_expr().size()) {
                addMsj(1, inst, "No concuerdan la cantidad de parametros: [ sub.size()=" + sub.getDeclaracion_param().size() + " !=  call.size()=" + ctx.l_expr().size() + " ]", fila, columna);
                return new Error();
            }//if
            // comprando que los parametros vengan del mismo tipo

            pila_Ent.push(new Entorno(pila_Ent.peek()));
            ent = pila_Ent.peek();

            c3d.codigo3d.add("//Call sub routina");
            //extramos y asignamos todos los parametros
            for (int i = 0; i < sub.getDeclaracion_param().size(); i++) {
                id = sub.getList_nom_param().get(i);//nombre del parametro de la subrutina
                Object objExp = visit(ctx.l_expr(i));//resultado de la expr del parametro de call
                Simbolo saux = sub.getEntSub().Buscar(id);
                saux.setValor(objExp);//actualizamos el valor de la variable 
                ent.nuevoSimbolo(id, saux);//agregamos el parametro al entorno de call

                saux.setPosicion(ent.ultimaPosicion);
                ent.ultimaPosicion++;

                c3d.codigo3d.add(c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + saux.getPosicion() + " ;");//t4
                c3d.codigo3d.add("P = " + c3d.lastTemporal() + " ;");
                c3d.codigo3d.add("STACK[(int)P] = " + saux.getValor() + " ;//holi\n");
            }//for
        }//if
        //si todo esta bien 
        for (int i = 0; i < sub.getInstrucciones().size(); i++) {
            visit(sub.getInstrucciones().get(i));
        }
        c3d.codigo3d.add("//Call sub routina end");

        //4.     
        pila_Ent.pop();
        return true;
    }

    //-------------------------------   FUNCIONES --------------------------------
    @Override
    public Object visitFunction(GramaticaParser.FunctionContext ctx) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();
        String inst = "Function";

        //1.    capturar y comprobar el nombre de la funcion sea valido
        String nom = ctx.nom.getText();
        String nom_end = ctx.nom_end.getText();
        if (!nom.equals(nom_end)) {
            addMsj(1, inst, "El nombre de la Funcion no coincide", fila, columna);
            return new Error();
        }

        //2.    comprobar que la variable no este guardada
        Simbolo sim = pila_Ent.peek().Buscar(nom);
        if (sim != null) {
            addMsj(1, inst, "No se puede crear. Ya existe una var con este nombre:" + nom + ".tipo()=" + sim.getTipo(), fila, columna);
            return new Error();
        }

        //3     creamos el entorno de la funcion
        // pila_Ent.push(new Entorno(pila_Ent.peek()));
        Entorno ent = new Entorno(null);

        List<String> list_nom_param = new ArrayList<>();
        //4.    validamos y asignamos valores a un entorno "ent" propio de la sub rutina, 
        //(solo ejecutamos los parametros) 
        //si no tiene parametros saltamos estos if anidados
        if ((ctx.list_param() != null) && (ctx.declaracion_parametro_func() != null)) {
            //4.1 validamos que la cantidad de parametros sea igual a lista_declaraciones_parametros
            if (ctx.list_param().size() != ctx.declaracion_parametro_func().size()) {
                addMsj(1, inst, "La cantidad de parametros no concuerda, con la cantidad de asignaciones", fila, columna);
                return new Error();
            }
            //ejecutamos las listas de parametros y declaraciones

            for (int i = 0; i < ctx.list_param().size(); i++) {
                //4.2 comprobamos el nombre del parametro sea el mismo
                String param = ctx.list_param(i).ID().getText();
                String param_dec = ctx.declaracion_parametro_func(i).ID().getText();
                if (!(param.equals(param_dec))) {
                    addMsj(1, inst, "el nombre de los parametos no coinciden \"" + param + "!=" + param_dec + "\"", fila, columna);
                    return new Error();
                }
                // 4.3  si estan correcto, lo guardamos en el entorno

                String tipo = ctx.declaracion_parametro_func(i).type().getText();
                Simbolo nuevoSim = new Simbolo(tipo, agregar_valor_predeterminado(tipo));
                if (ctx.declaracion_parametro_func(i).list_param() != null) {
                    nuevoSim.setValor(new Arreglo(tipo));
                }
                ent.nuevoSimbolo(param, nuevoSim);
                list_nom_param.add(param);

            }//for

        } else if ((ctx.list_param() == null) && (ctx.declaracion_parametro_func() != null)) {
            addMsj(1, inst, "La subRut no cuenta con parametros declarados para asignarle tipo", fila, columna);
            return new Error();
        } else if ((ctx.list_param() != null) && (ctx.declaracion_parametro_func() == null)) {
            addMsj(1, inst, "La subRut no cuenta con asignaciones de tipos, para los parametros declarados", fila, columna);
            return new Error();
        }
        //Agregando el nombre del retorno
        String nomRetorno = "";
        if (ctx.retorno != null) {
            nomRetorno = ctx.retorno.getText();
        }

        //3.    Creamos el objeto Subrountine
        Funciones simSub = new Funciones(ent, ctx.list_param(), ctx.declaracion_parametro_func(), ctx.instrucciones(), list_nom_param, nomRetorno);
        Simbolo nuevoSim = new Simbolo("function", simSub);
        pila_Ent.peek().nuevoSimbolo(nom, nuevoSim);
        addMsj(2, inst, "se agrego una nueva Funcion: " + nom, fila, columna);

        return true;
    }

    @Override
    public Object visitCall_function(GramaticaParser.Call_functionContext ctx
    ) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();
        String inst = "Call_function";

        //1.    capturar el nombre de la subrutina a llamar
        String id = ctx.ID().getText();

        //2.    Buscar si existe la funcion guardada y comprobar si es de tipo subrutina
        Entorno ent = pila_Ent.peek();
        Simbolo sim = ent.Buscar(id);
        if (sim == null) {
            addMsj(1, inst, "La sub-rutina : \"" + id + "\", No existe  ", fila, columna);
            return new Error();
        }
//        if (!(sim.getValor() instanceof Funciones)) {
//            addMsj(1, inst, "La variable : \"" + id + ".tipo()=" + sim.getTipo() + " \", No es de tipo Funcion", fila, columna);
//            return new Error();
//        }

        Funciones func = (Funciones) sim.getValor();

//        Trabando con Call
//        4.  comprobar que la cantidad de parametros de la subrutina sean iguales a el de call
        if ((func.getDeclaracion_param() != null) && (ctx.l_expr() != null)) {

            if (func.getDeclaracion_param().size() != ctx.l_expr().size()) {
                addMsj(1, inst, "No concuerdan la cantidad de parametros: [ sub.size()=" + func.getDeclaracion_param().size() + " !=  call.size()=" + ctx.l_expr().size() + " ]", fila, columna);
                return new Error();
            }//if
            // comprando que los parametros vengan del mismo tipo

            pila_Ent.push(new Entorno(pila_Ent.peek()));
            ent = pila_Ent.peek();

            c3d.codigo3d.add("//Call Funcion: " + id);

            //extramos y asignamos todos los parametros
            for (int i = 0; i < func.getDeclaracion_param().size(); i++) {
                id = func.getList_nom_param().get(i);//nombre del parametro de la subrutina
                Object objExp = visit(ctx.l_expr(i));//resultado de la expr del parametro de call
                Simbolo saux = func.getEntSub().Buscar(id);
                saux.setValor(objExp);//actualizamos el valor de la variable 
                ent.nuevoSimbolo(id, saux);//agregamos el parametro al entorno de call

                saux.setPosicion(ent.ultimaPosicion);
                ent.ultimaPosicion++;

                c3d.codigo3d.add(c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + saux.getPosicion() + " ;");//t4
                c3d.codigo3d.add("P = " + c3d.lastTemporal() + " ;");
                c3d.codigo3d.add("STACK[(int)P] = " + saux.getValor() + " ;//holi funcion\n");
            }//for
        }//if

        //ejecutamos las instrucciones
        for (int i = 0; i < func.getInstrucciones().size(); i++) {
            visit(func.getInstrucciones().get(i));
        }
        //4.     
        pila_Ent.pop();
        c3d.codigo3d.add("//Call funcion end: " + id);

        return true;
    }

    @Override
    public Object visitCall_function_expr(GramaticaParser.Call_function_exprContext ctx
    ) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();
        String inst = "Call_function_expr";

        //1.    capturar el nombre de la subrutina a llamar
        String id = ctx.ID().getText();

        //2.    Buscar si existe la funcion guardada y comprobar si es de tipo subrutina
        Entorno ent = pila_Ent.peek();
        Simbolo sim = ent.Buscar(id);
        if (sim == null) {
            addMsj(1, inst, "La sub-rutina : \"" + id + "\", No existe  ", fila, columna);
            return new Error();
        }
        if (!(sim.getValor() instanceof Funciones)) {
            addMsj(1, inst, "La variable : \"" + id + ".tipo()=" + sim.getTipo() + " \", No es de tipo Funcion", fila, columna);
            return new Error();
        }

        Funciones func = (Funciones) sim.getValor();

//        Trabando con Call
//        4.  comprobar que la cantidad de parametros de la subrutina sean iguales a el de call
        if ((func.getDeclaracion_param() != null) && (ctx.l_expr() != null)) {

            if (func.getDeclaracion_param().size() != ctx.l_expr().size()) {
                addMsj(1, inst, "No concuerdan la cantidad de parametros: [ sub.size()=" + func.getDeclaracion_param().size() + " !=  call.size()=" + ctx.l_expr().size() + " ]", fila, columna);
                return new Error();
            }//if
            // comprando que los parametros vengan del mismo tipo

            pila_Ent.push(new Entorno(pila_Ent.peek()));
            ent = pila_Ent.peek();

            c3d.codigo3d.add("//Call Funcion_exp: " + id);

            //extramos y asignamos todos los parametros
            for (int i = 0; i < func.getDeclaracion_param().size(); i++) {
                id = func.getList_nom_param().get(i);//nombre del parametro de la subrutina
                Object objExp = visit(ctx.l_expr(i));//resultado de la expr del parametro de call
                Simbolo saux = func.getEntSub().Buscar(id);
                saux.setValor(objExp);//actualizamos el valor de la variable 
                ent.nuevoSimbolo(id, saux);//agregamos el parametro al entorno de call

                saux.setPosicion(ent.ultimaPosicion);
                ent.ultimaPosicion++;

                c3d.codigo3d.add(c3d.generarTemporal() + " = " + ent.getPrevSizes() + " + " + saux.getPosicion() + " ;");//t4
                c3d.codigo3d.add("P = " + c3d.lastTemporal() + " ;");
                c3d.codigo3d.add("STACK[(int)P] = " + saux.getValor() + " ;//holi funcion\n");
            }//for
        }//if

        String idRetorno = func.getRetorno();
        //guardamos la variable de retorno para asignar un valor despues
        Simbolo simRetorno = new Simbolo("function", new Funciones());
        ent.nuevoSimbolo(idRetorno, sim);

        //ejecutamos las instrucciones
        for (int i = 0; i < func.getInstrucciones().size(); i++) {
            visit(func.getInstrucciones().get(i));
        }
        simRetorno = ent.Buscar(idRetorno);

        //4.     
        pila_Ent.pop();
        c3d.codigo3d.add("//Call funcion_exp end: " + id);
        return true;

    }

    //---------------------------------------------------------------
    //---------------------------------------------------------------
    //---------------------------------------------------------------
    //---------------------------------------------------------------
    @Override
    public Object visitPrintEnt(GramaticaParser.PrintEntContext ctx
    ) {
        return new Reporte_Entornos(pila_Ent);
    }
    //---------------------------------------------------------------

    public Boolean comprobacion_de_tipos(String tipoVar, Object valor) {
        switch (tipoVar) {
            case "integer":
                if (valor instanceof Integer) {
                    return true;
                }
                break;
            case "real":
                if (valor instanceof Double) {
                    return true;
                }
                break;
            case "character":
                if (valor instanceof Character) {
                    return true;
                }
                break;
            case "logical":
                if (valor instanceof Boolean) {
                    return true;
                }
                break;
        }
        return false;
    }//metodo
    //---------------------------------------------------------------

    public String getTipo_obj(Object valor) {
        if (valor instanceof Integer) {
            return "Integer";
        }
        if (valor instanceof Double) {
            return "Real";
        }
        if (valor instanceof Character) {
            return "Character";
        }
        if (valor instanceof Boolean) {
            return "Logical";
        }
        if (valor instanceof Arreglo) {
            return "Arreglo";
        }
        return "";
    }

    //---------------------------------------------------------------
    public Object agregar_valor_predeterminado(String tipo) {
        switch (tipo) {
            case "integer":
                return 0;

            case "real":
                return 0.00000000;

            case "complex":
                return "(9.192517926E-43,0.00000000)";

            case "character":
                return ' ';

            case "logical":
                return false;

        }
        return new Error();
    }//metodo

    public void crearSTR_c3d(String str) {
        c3d.codigo3d.add("\n  //Print String:(\"" + str + "\")");
        c3d.codigo3d.add("  " + c3d.generarTemporal() + " = H ;"); //tn = H;

        for (int i = 0; i < str.length(); i++) {
            int ch = str.charAt(i);
            c3d.codigo3d.add("    HEAP[(int)H] = " + ch + " ;");
            c3d.codigo3d.add("      H = H + 1 ;");
        }
        //agregando limite de lectura
        c3d.codigo3d.add("    HEAP[(int)H] = -1 ;");
        c3d.codigo3d.add("    H = " + c3d.lastTemporal() + " ;");
        c3d.codigo3d.add("  imprimir_string();\n");
    }

    @Override
    public Object visitGetSize_Arreglo(GramaticaParser.GetSize_ArregloContext ctx) {
        int fila = ctx.getStart().getLine();
        int columna = ctx.getStart().getCharPositionInLine();
        String inst = "Sise Arreglo";

        String id = ctx.ID().getText();
        Entorno ent = pila_Ent.peek();
        Simbolo sim = ent.Buscar(id);
        if (sim != null) {

            if (sim.getValor() instanceof Arreglo) {
                Arreglo arr = (Arreglo) sim.getValor();
                return arr.getSize();

            } else {
                addMsj(1, inst, "La variable: \"" + id + ".tipo()=" + sim.getTipo() + "\",  No es un Arreglo", fila, columna);
                return new Error();
            }

        } else {
            addMsj(1, inst, "No existe el Arreglo:  \"" + id + " \"", fila, columna);
            return new Error();
        }
    }

}//class
