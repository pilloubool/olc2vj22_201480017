// Generated from Gramatica.g4 by ANTLR 4.10.1
package Gramatica;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link GramaticaParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface GramaticaVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(GramaticaParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#programa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrograma(GramaticaParser.ProgramaContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#implicit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImplicit(GramaticaParser.ImplicitContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#linstrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLinstrucciones(GramaticaParser.LinstruccionesContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#instrucciones}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInstrucciones(GramaticaParser.InstruccionesContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(GramaticaParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declaracion_arreglos1}
	 * labeled alternative in {@link GramaticaParser#declaracion_arreglos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracion_arreglos1(GramaticaParser.Declaracion_arreglos1Context ctx);
	/**
	 * Visit a parse tree produced by the {@code declaracion_arr_dinamicos}
	 * labeled alternative in {@link GramaticaParser#declaracion_arreglos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracion_arr_dinamicos(GramaticaParser.Declaracion_arr_dinamicosContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#cant_arrDinamic_dim}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCant_arrDinamic_dim(GramaticaParser.Cant_arrDinamic_dimContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#l_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitL_expr(GramaticaParser.L_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#allocate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllocate(GramaticaParser.AllocateContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#deallocate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeallocate(GramaticaParser.DeallocateContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(GramaticaParser.DeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#declaration2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration2(GramaticaParser.Declaration2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code asignacion_primitivos}
	 * labeled alternative in {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignacion_primitivos(GramaticaParser.Asignacion_primitivosContext ctx);
	/**
	 * Visit a parse tree produced by the {@code asignacion_arreglos_unaDim}
	 * labeled alternative in {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignacion_arreglos_unaDim(GramaticaParser.Asignacion_arreglos_unaDimContext ctx);
	/**
	 * Visit a parse tree produced by the {@code asignacion_arreglos_unaDim2}
	 * labeled alternative in {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignacion_arreglos_unaDim2(GramaticaParser.Asignacion_arreglos_unaDim2Context ctx);
	/**
	 * Visit a parse tree produced by the {@code asignacion_arreglos_DosDim}
	 * labeled alternative in {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsignacion_arreglos_DosDim(GramaticaParser.Asignacion_arreglos_DosDimContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#printEnt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrintEnt(GramaticaParser.PrintEntContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#if_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_(GramaticaParser.If_Context ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#elseIf_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElseIf_(GramaticaParser.ElseIf_Context ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#else_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElse_(GramaticaParser.Else_Context ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#do_}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDo_(GramaticaParser.Do_Context ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#do_while}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDo_while(GramaticaParser.Do_whileContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#exit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExit(GramaticaParser.ExitContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#cycle}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCycle(GramaticaParser.CycleContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#list_param}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitList_param(GramaticaParser.List_paramContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#intent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntent(GramaticaParser.IntentContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#declaracion_parametro_func}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaracion_parametro_func(GramaticaParser.Declaracion_parametro_funcContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#subroutine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubroutine(GramaticaParser.SubroutineContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall(GramaticaParser.CallContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(GramaticaParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#call_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall_function(GramaticaParser.Call_functionContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(GramaticaParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#estado_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEstado_expr(GramaticaParser.Estado_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link GramaticaParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(GramaticaParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code strExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrExpr(GramaticaParser.StrExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code charExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharExpr(GramaticaParser.CharExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpExpr(GramaticaParser.OpExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code realExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRealExpr(GramaticaParser.RealExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code enteroExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnteroExpr(GramaticaParser.EnteroExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relacionales_Expr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelacionales_Expr(GramaticaParser.Relacionales_ExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code call_function_expr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall_function_expr(GramaticaParser.Call_function_exprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parenExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParenExpr(GramaticaParser.ParenExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code getSize_Arreglo}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGetSize_Arreglo(GramaticaParser.GetSize_ArregloContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotExpr(GramaticaParser.NotExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryMinusExpr(GramaticaParser.UnaryMinusExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolExpr(GramaticaParser.BoolExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdExpr(GramaticaParser.IdExprContext ctx);
}