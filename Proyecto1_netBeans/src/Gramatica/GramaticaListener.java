// Generated from Gramatica.g4 by ANTLR 4.10.1
package Gramatica;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link GramaticaParser}.
 */
public interface GramaticaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(GramaticaParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(GramaticaParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#programa}.
	 * @param ctx the parse tree
	 */
	void enterPrograma(GramaticaParser.ProgramaContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#programa}.
	 * @param ctx the parse tree
	 */
	void exitPrograma(GramaticaParser.ProgramaContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#implicit}.
	 * @param ctx the parse tree
	 */
	void enterImplicit(GramaticaParser.ImplicitContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#implicit}.
	 * @param ctx the parse tree
	 */
	void exitImplicit(GramaticaParser.ImplicitContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#linstrucciones}.
	 * @param ctx the parse tree
	 */
	void enterLinstrucciones(GramaticaParser.LinstruccionesContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#linstrucciones}.
	 * @param ctx the parse tree
	 */
	void exitLinstrucciones(GramaticaParser.LinstruccionesContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void enterInstrucciones(GramaticaParser.InstruccionesContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#instrucciones}.
	 * @param ctx the parse tree
	 */
	void exitInstrucciones(GramaticaParser.InstruccionesContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(GramaticaParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(GramaticaParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declaracion_arreglos1}
	 * labeled alternative in {@link GramaticaParser#declaracion_arreglos}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracion_arreglos1(GramaticaParser.Declaracion_arreglos1Context ctx);
	/**
	 * Exit a parse tree produced by the {@code declaracion_arreglos1}
	 * labeled alternative in {@link GramaticaParser#declaracion_arreglos}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracion_arreglos1(GramaticaParser.Declaracion_arreglos1Context ctx);
	/**
	 * Enter a parse tree produced by the {@code declaracion_arr_dinamicos}
	 * labeled alternative in {@link GramaticaParser#declaracion_arreglos}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracion_arr_dinamicos(GramaticaParser.Declaracion_arr_dinamicosContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declaracion_arr_dinamicos}
	 * labeled alternative in {@link GramaticaParser#declaracion_arreglos}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracion_arr_dinamicos(GramaticaParser.Declaracion_arr_dinamicosContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#cant_arrDinamic_dim}.
	 * @param ctx the parse tree
	 */
	void enterCant_arrDinamic_dim(GramaticaParser.Cant_arrDinamic_dimContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#cant_arrDinamic_dim}.
	 * @param ctx the parse tree
	 */
	void exitCant_arrDinamic_dim(GramaticaParser.Cant_arrDinamic_dimContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#l_expr}.
	 * @param ctx the parse tree
	 */
	void enterL_expr(GramaticaParser.L_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#l_expr}.
	 * @param ctx the parse tree
	 */
	void exitL_expr(GramaticaParser.L_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#allocate}.
	 * @param ctx the parse tree
	 */
	void enterAllocate(GramaticaParser.AllocateContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#allocate}.
	 * @param ctx the parse tree
	 */
	void exitAllocate(GramaticaParser.AllocateContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#deallocate}.
	 * @param ctx the parse tree
	 */
	void enterDeallocate(GramaticaParser.DeallocateContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#deallocate}.
	 * @param ctx the parse tree
	 */
	void exitDeallocate(GramaticaParser.DeallocateContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(GramaticaParser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(GramaticaParser.DeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#declaration2}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration2(GramaticaParser.Declaration2Context ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#declaration2}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration2(GramaticaParser.Declaration2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code asignacion_primitivos}
	 * labeled alternative in {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterAsignacion_primitivos(GramaticaParser.Asignacion_primitivosContext ctx);
	/**
	 * Exit a parse tree produced by the {@code asignacion_primitivos}
	 * labeled alternative in {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitAsignacion_primitivos(GramaticaParser.Asignacion_primitivosContext ctx);
	/**
	 * Enter a parse tree produced by the {@code asignacion_arreglos_unaDim}
	 * labeled alternative in {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterAsignacion_arreglos_unaDim(GramaticaParser.Asignacion_arreglos_unaDimContext ctx);
	/**
	 * Exit a parse tree produced by the {@code asignacion_arreglos_unaDim}
	 * labeled alternative in {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitAsignacion_arreglos_unaDim(GramaticaParser.Asignacion_arreglos_unaDimContext ctx);
	/**
	 * Enter a parse tree produced by the {@code asignacion_arreglos_unaDim2}
	 * labeled alternative in {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterAsignacion_arreglos_unaDim2(GramaticaParser.Asignacion_arreglos_unaDim2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code asignacion_arreglos_unaDim2}
	 * labeled alternative in {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitAsignacion_arreglos_unaDim2(GramaticaParser.Asignacion_arreglos_unaDim2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code asignacion_arreglos_DosDim}
	 * labeled alternative in {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void enterAsignacion_arreglos_DosDim(GramaticaParser.Asignacion_arreglos_DosDimContext ctx);
	/**
	 * Exit a parse tree produced by the {@code asignacion_arreglos_DosDim}
	 * labeled alternative in {@link GramaticaParser#asignacion}.
	 * @param ctx the parse tree
	 */
	void exitAsignacion_arreglos_DosDim(GramaticaParser.Asignacion_arreglos_DosDimContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#printEnt}.
	 * @param ctx the parse tree
	 */
	void enterPrintEnt(GramaticaParser.PrintEntContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#printEnt}.
	 * @param ctx the parse tree
	 */
	void exitPrintEnt(GramaticaParser.PrintEntContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#if_}.
	 * @param ctx the parse tree
	 */
	void enterIf_(GramaticaParser.If_Context ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#if_}.
	 * @param ctx the parse tree
	 */
	void exitIf_(GramaticaParser.If_Context ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#elseIf_}.
	 * @param ctx the parse tree
	 */
	void enterElseIf_(GramaticaParser.ElseIf_Context ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#elseIf_}.
	 * @param ctx the parse tree
	 */
	void exitElseIf_(GramaticaParser.ElseIf_Context ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#else_}.
	 * @param ctx the parse tree
	 */
	void enterElse_(GramaticaParser.Else_Context ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#else_}.
	 * @param ctx the parse tree
	 */
	void exitElse_(GramaticaParser.Else_Context ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#do_}.
	 * @param ctx the parse tree
	 */
	void enterDo_(GramaticaParser.Do_Context ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#do_}.
	 * @param ctx the parse tree
	 */
	void exitDo_(GramaticaParser.Do_Context ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#do_while}.
	 * @param ctx the parse tree
	 */
	void enterDo_while(GramaticaParser.Do_whileContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#do_while}.
	 * @param ctx the parse tree
	 */
	void exitDo_while(GramaticaParser.Do_whileContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#exit}.
	 * @param ctx the parse tree
	 */
	void enterExit(GramaticaParser.ExitContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#exit}.
	 * @param ctx the parse tree
	 */
	void exitExit(GramaticaParser.ExitContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#cycle}.
	 * @param ctx the parse tree
	 */
	void enterCycle(GramaticaParser.CycleContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#cycle}.
	 * @param ctx the parse tree
	 */
	void exitCycle(GramaticaParser.CycleContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#list_param}.
	 * @param ctx the parse tree
	 */
	void enterList_param(GramaticaParser.List_paramContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#list_param}.
	 * @param ctx the parse tree
	 */
	void exitList_param(GramaticaParser.List_paramContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#intent}.
	 * @param ctx the parse tree
	 */
	void enterIntent(GramaticaParser.IntentContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#intent}.
	 * @param ctx the parse tree
	 */
	void exitIntent(GramaticaParser.IntentContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#declaracion_parametro_func}.
	 * @param ctx the parse tree
	 */
	void enterDeclaracion_parametro_func(GramaticaParser.Declaracion_parametro_funcContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#declaracion_parametro_func}.
	 * @param ctx the parse tree
	 */
	void exitDeclaracion_parametro_func(GramaticaParser.Declaracion_parametro_funcContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#subroutine}.
	 * @param ctx the parse tree
	 */
	void enterSubroutine(GramaticaParser.SubroutineContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#subroutine}.
	 * @param ctx the parse tree
	 */
	void exitSubroutine(GramaticaParser.SubroutineContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#call}.
	 * @param ctx the parse tree
	 */
	void enterCall(GramaticaParser.CallContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#call}.
	 * @param ctx the parse tree
	 */
	void exitCall(GramaticaParser.CallContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(GramaticaParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(GramaticaParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#call_function}.
	 * @param ctx the parse tree
	 */
	void enterCall_function(GramaticaParser.Call_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#call_function}.
	 * @param ctx the parse tree
	 */
	void exitCall_function(GramaticaParser.Call_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(GramaticaParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(GramaticaParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#estado_expr}.
	 * @param ctx the parse tree
	 */
	void enterEstado_expr(GramaticaParser.Estado_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#estado_expr}.
	 * @param ctx the parse tree
	 */
	void exitEstado_expr(GramaticaParser.Estado_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link GramaticaParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(GramaticaParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link GramaticaParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(GramaticaParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code strExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterStrExpr(GramaticaParser.StrExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code strExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitStrExpr(GramaticaParser.StrExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code charExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterCharExpr(GramaticaParser.CharExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code charExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitCharExpr(GramaticaParser.CharExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterOpExpr(GramaticaParser.OpExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitOpExpr(GramaticaParser.OpExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code realExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterRealExpr(GramaticaParser.RealExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code realExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitRealExpr(GramaticaParser.RealExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code enteroExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterEnteroExpr(GramaticaParser.EnteroExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code enteroExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitEnteroExpr(GramaticaParser.EnteroExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code relacionales_Expr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterRelacionales_Expr(GramaticaParser.Relacionales_ExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code relacionales_Expr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitRelacionales_Expr(GramaticaParser.Relacionales_ExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code call_function_expr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterCall_function_expr(GramaticaParser.Call_function_exprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code call_function_expr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitCall_function_expr(GramaticaParser.Call_function_exprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parenExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterParenExpr(GramaticaParser.ParenExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parenExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitParenExpr(GramaticaParser.ParenExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getSize_Arreglo}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterGetSize_Arreglo(GramaticaParser.GetSize_ArregloContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getSize_Arreglo}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitGetSize_Arreglo(GramaticaParser.GetSize_ArregloContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNotExpr(GramaticaParser.NotExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNotExpr(GramaticaParser.NotExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterUnaryMinusExpr(GramaticaParser.UnaryMinusExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryMinusExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitUnaryMinusExpr(GramaticaParser.UnaryMinusExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolExpr(GramaticaParser.BoolExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolExpr(GramaticaParser.BoolExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterIdExpr(GramaticaParser.IdExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link GramaticaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitIdExpr(GramaticaParser.IdExprContext ctx);
}