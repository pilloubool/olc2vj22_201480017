// Generated from Gramatica.g4 by ANTLR 4.10.1
package Gramatica;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GramaticaParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.10.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, T__51=52, 
		T__52=53, T__53=54, T__54=55, T__55=56, T__56=57, T__57=58, T__58=59, 
		T__59=60, T__60=61, T__61=62, COMENT=63, INTEGER=64, REAL=65, ID=66, STRING=67, 
		CHAR=68, WS=69;
	public static final int
		RULE_start = 0, RULE_programa = 1, RULE_implicit = 2, RULE_linstrucciones = 3, 
		RULE_instrucciones = 4, RULE_block = 5, RULE_declaracion_arreglos = 6, 
		RULE_cant_arrDinamic_dim = 7, RULE_l_expr = 8, RULE_allocate = 9, RULE_deallocate = 10, 
		RULE_declaration = 11, RULE_declaration2 = 12, RULE_asignacion = 13, RULE_printEnt = 14, 
		RULE_if_ = 15, RULE_elseIf_ = 16, RULE_else_ = 17, RULE_do_ = 18, RULE_do_while = 19, 
		RULE_exit = 20, RULE_cycle = 21, RULE_list_param = 22, RULE_intent = 23, 
		RULE_declaracion_parametro_func = 24, RULE_subroutine = 25, RULE_call = 26, 
		RULE_function = 27, RULE_call_function = 28, RULE_print = 29, RULE_estado_expr = 30, 
		RULE_type = 31, RULE_expr = 32;
	private static String[] makeRuleNames() {
		return new String[] {
			"start", "programa", "implicit", "linstrucciones", "instrucciones", "block", 
			"declaracion_arreglos", "cant_arrDinamic_dim", "l_expr", "allocate", 
			"deallocate", "declaration", "declaration2", "asignacion", "printEnt", 
			"if_", "elseIf_", "else_", "do_", "do_while", "exit", "cycle", "list_param", 
			"intent", "declaracion_parametro_func", "subroutine", "call", "function", 
			"call_function", "print", "estado_expr", "type", "expr"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'program'", "'end'", "'implicit'", "'none'", "'{'", "'}'", "','", 
			"'dimension'", "'('", "')'", "'::'", "'allocatable'", "':'", "'allocate'", 
			"'deallocate'", "'='", "'/'", "'['", "']'", "'print_ent()'", "'if'", 
			"'then'", "'else'", "'do'", "'while'", "'exit'", "'cycle'", "'intent'", 
			"'in'", "'subroutine'", "'call'", "'function'", "'result'", "'print'", 
			"'*'", "'integer'", "'real'", "'complex'", "'character'", "'logical'", 
			"'**'", "'+'", "'-'", "'.'", "'not'", "'true'", "'false'", "'=='", "'.eq.'", 
			"'/='", "'.ne.'", "'>'", "'.gt.'", "'<'", "'.lt.'", "'>='", "'.ge.'", 
			"'<='", "'.le.'", "'.and.'", "'.or.'", "'size'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, "COMENT", "INTEGER", "REAL", "ID", "STRING", "CHAR", 
			"WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Gramatica.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public GramaticaParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class StartContext extends ParserRuleContext {
		public ProgramaContext programa() {
			return getRuleContext(ProgramaContext.class,0);
		}
		public TerminalNode EOF() { return getToken(GramaticaParser.EOF, 0); }
		public List<FunctionContext> function() {
			return getRuleContexts(FunctionContext.class);
		}
		public FunctionContext function(int i) {
			return getRuleContext(FunctionContext.class,i);
		}
		public List<SubroutineContext> subroutine() {
			return getRuleContexts(SubroutineContext.class);
		}
		public SubroutineContext subroutine(int i) {
			return getRuleContext(SubroutineContext.class,i);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterStart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitStart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitStart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(70);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__29 || _la==T__31) {
				{
				setState(68);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__31:
					{
					setState(66);
					function();
					}
					break;
				case T__29:
					{
					setState(67);
					subroutine();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(72);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(73);
			programa();
			setState(74);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramaContext extends ParserRuleContext {
		public Token prog;
		public Token prog_end;
		public ImplicitContext implicit() {
			return getRuleContext(ImplicitContext.class,0);
		}
		public List<TerminalNode> ID() { return getTokens(GramaticaParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(GramaticaParser.ID, i);
		}
		public List<InstruccionesContext> instrucciones() {
			return getRuleContexts(InstruccionesContext.class);
		}
		public InstruccionesContext instrucciones(int i) {
			return getRuleContext(InstruccionesContext.class,i);
		}
		public ProgramaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_programa; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterPrograma(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitPrograma(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitPrograma(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramaContext programa() throws RecognitionException {
		ProgramaContext _localctx = new ProgramaContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_programa);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			match(T__0);
			setState(77);
			((ProgramaContext)_localctx).prog = match(ID);
			setState(78);
			implicit();
			setState(82);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (T__4 - 5)) | (1L << (T__13 - 5)) | (1L << (T__14 - 5)) | (1L << (T__19 - 5)) | (1L << (T__20 - 5)) | (1L << (T__23 - 5)) | (1L << (T__25 - 5)) | (1L << (T__26 - 5)) | (1L << (T__30 - 5)) | (1L << (T__33 - 5)) | (1L << (T__35 - 5)) | (1L << (T__36 - 5)) | (1L << (T__37 - 5)) | (1L << (T__38 - 5)) | (1L << (T__39 - 5)) | (1L << (ID - 5)))) != 0)) {
				{
				{
				setState(79);
				instrucciones();
				}
				}
				setState(84);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(85);
			match(T__1);
			setState(86);
			match(T__0);
			setState(87);
			((ProgramaContext)_localctx).prog_end = match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImplicitContext extends ParserRuleContext {
		public ImplicitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_implicit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterImplicit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitImplicit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitImplicit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ImplicitContext implicit() throws RecognitionException {
		ImplicitContext _localctx = new ImplicitContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_implicit);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89);
			match(T__2);
			setState(90);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LinstruccionesContext extends ParserRuleContext {
		public List<InstruccionesContext> instrucciones() {
			return getRuleContexts(InstruccionesContext.class);
		}
		public InstruccionesContext instrucciones(int i) {
			return getRuleContext(InstruccionesContext.class,i);
		}
		public LinstruccionesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_linstrucciones; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterLinstrucciones(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitLinstrucciones(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitLinstrucciones(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LinstruccionesContext linstrucciones() throws RecognitionException {
		LinstruccionesContext _localctx = new LinstruccionesContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_linstrucciones);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(93); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(92);
				instrucciones();
				}
				}
				setState(95); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( ((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (T__4 - 5)) | (1L << (T__13 - 5)) | (1L << (T__14 - 5)) | (1L << (T__19 - 5)) | (1L << (T__20 - 5)) | (1L << (T__23 - 5)) | (1L << (T__25 - 5)) | (1L << (T__26 - 5)) | (1L << (T__30 - 5)) | (1L << (T__33 - 5)) | (1L << (T__35 - 5)) | (1L << (T__36 - 5)) | (1L << (T__37 - 5)) | (1L << (T__38 - 5)) | (1L << (T__39 - 5)) | (1L << (ID - 5)))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstruccionesContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public DeclarationContext declaration() {
			return getRuleContext(DeclarationContext.class,0);
		}
		public PrintContext print() {
			return getRuleContext(PrintContext.class,0);
		}
		public AsignacionContext asignacion() {
			return getRuleContext(AsignacionContext.class,0);
		}
		public Declaracion_arreglosContext declaracion_arreglos() {
			return getRuleContext(Declaracion_arreglosContext.class,0);
		}
		public AllocateContext allocate() {
			return getRuleContext(AllocateContext.class,0);
		}
		public DeallocateContext deallocate() {
			return getRuleContext(DeallocateContext.class,0);
		}
		public PrintEntContext printEnt() {
			return getRuleContext(PrintEntContext.class,0);
		}
		public If_Context if_() {
			return getRuleContext(If_Context.class,0);
		}
		public Do_Context do_() {
			return getRuleContext(Do_Context.class,0);
		}
		public Do_whileContext do_while() {
			return getRuleContext(Do_whileContext.class,0);
		}
		public ExitContext exit() {
			return getRuleContext(ExitContext.class,0);
		}
		public CycleContext cycle() {
			return getRuleContext(CycleContext.class,0);
		}
		public CallContext call() {
			return getRuleContext(CallContext.class,0);
		}
		public Call_functionContext call_function() {
			return getRuleContext(Call_functionContext.class,0);
		}
		public InstruccionesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instrucciones; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterInstrucciones(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitInstrucciones(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitInstrucciones(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InstruccionesContext instrucciones() throws RecognitionException {
		InstruccionesContext _localctx = new InstruccionesContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_instrucciones);
		try {
			setState(112);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(97);
				block();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(98);
				declaration();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(99);
				print();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(100);
				asignacion();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(101);
				declaracion_arreglos();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(102);
				allocate();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(103);
				deallocate();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(104);
				printEnt();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(105);
				if_();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(106);
				do_();
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(107);
				do_while();
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(108);
				exit();
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(109);
				cycle();
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(110);
				call();
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(111);
				call_function();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public LinstruccionesContext linstrucciones() {
			return getRuleContext(LinstruccionesContext.class,0);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(114);
			match(T__4);
			setState(115);
			linstrucciones();
			setState(116);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Declaracion_arreglosContext extends ParserRuleContext {
		public Declaracion_arreglosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaracion_arreglos; }
	 
		public Declaracion_arreglosContext() { }
		public void copyFrom(Declaracion_arreglosContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Declaracion_arr_dinamicosContext extends Declaracion_arreglosContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public Cant_arrDinamic_dimContext cant_arrDinamic_dim() {
			return getRuleContext(Cant_arrDinamic_dimContext.class,0);
		}
		public Declaracion_arr_dinamicosContext(Declaracion_arreglosContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterDeclaracion_arr_dinamicos(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitDeclaracion_arr_dinamicos(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitDeclaracion_arr_dinamicos(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Declaracion_arreglos1Context extends Declaracion_arreglosContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public List<L_exprContext> l_expr() {
			return getRuleContexts(L_exprContext.class);
		}
		public L_exprContext l_expr(int i) {
			return getRuleContext(L_exprContext.class,i);
		}
		public Declaracion_arreglos1Context(Declaracion_arreglosContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterDeclaracion_arreglos1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitDeclaracion_arreglos1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitDeclaracion_arreglos1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Declaracion_arreglosContext declaracion_arreglos() throws RecognitionException {
		Declaracion_arreglosContext _localctx = new Declaracion_arreglosContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_declaracion_arreglos);
		int _la;
		try {
			setState(149);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				_localctx = new Declaracion_arreglos1Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(118);
				type();
				setState(119);
				match(T__6);
				setState(120);
				match(T__7);
				setState(121);
				match(T__8);
				setState(123); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(122);
					l_expr();
					}
					}
					setState(125); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( ((((_la - 7)) & ~0x3f) == 0 && ((1L << (_la - 7)) & ((1L << (T__6 - 7)) | (1L << (T__8 - 7)) | (1L << (T__42 - 7)) | (1L << (T__43 - 7)) | (1L << (T__61 - 7)) | (1L << (INTEGER - 7)) | (1L << (REAL - 7)) | (1L << (ID - 7)) | (1L << (STRING - 7)) | (1L << (CHAR - 7)))) != 0) );
				setState(127);
				match(T__9);
				setState(128);
				match(T__10);
				setState(129);
				match(ID);
				}
				break;
			case 2:
				_localctx = new Declaracion_arreglos1Context(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(131);
				type();
				setState(132);
				match(T__10);
				setState(133);
				match(ID);
				setState(134);
				match(T__8);
				setState(136); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(135);
					l_expr();
					}
					}
					setState(138); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( ((((_la - 7)) & ~0x3f) == 0 && ((1L << (_la - 7)) & ((1L << (T__6 - 7)) | (1L << (T__8 - 7)) | (1L << (T__42 - 7)) | (1L << (T__43 - 7)) | (1L << (T__61 - 7)) | (1L << (INTEGER - 7)) | (1L << (REAL - 7)) | (1L << (ID - 7)) | (1L << (STRING - 7)) | (1L << (CHAR - 7)))) != 0) );
				setState(140);
				match(T__9);
				}
				break;
			case 3:
				_localctx = new Declaracion_arr_dinamicosContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(142);
				type();
				setState(143);
				match(T__6);
				setState(144);
				match(T__11);
				setState(145);
				match(T__10);
				setState(146);
				match(ID);
				setState(147);
				cant_arrDinamic_dim();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cant_arrDinamic_dimContext extends ParserRuleContext {
		public Token una;
		public Token dos;
		public Cant_arrDinamic_dimContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cant_arrDinamic_dim; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterCant_arrDinamic_dim(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitCant_arrDinamic_dim(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitCant_arrDinamic_dim(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Cant_arrDinamic_dimContext cant_arrDinamic_dim() throws RecognitionException {
		Cant_arrDinamic_dimContext _localctx = new Cant_arrDinamic_dimContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_cant_arrDinamic_dim);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(151);
			match(T__8);
			setState(152);
			((Cant_arrDinamic_dimContext)_localctx).una = match(T__12);
			setState(155);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__6) {
				{
				setState(153);
				match(T__6);
				setState(154);
				((Cant_arrDinamic_dimContext)_localctx).dos = match(T__12);
				}
			}

			setState(157);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class L_exprContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public L_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_l_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterL_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitL_expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitL_expr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final L_exprContext l_expr() throws RecognitionException {
		L_exprContext _localctx = new L_exprContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_l_expr);
		try {
			setState(162);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__6:
				enterOuterAlt(_localctx, 1);
				{
				setState(159);
				match(T__6);
				setState(160);
				expr(0);
				}
				break;
			case T__8:
			case T__42:
			case T__43:
			case T__61:
			case INTEGER:
			case REAL:
			case ID:
			case STRING:
			case CHAR:
				enterOuterAlt(_localctx, 2);
				{
				setState(161);
				expr(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AllocateContext extends ParserRuleContext {
		public ExprContext una;
		public ExprContext dos;
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public AllocateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_allocate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterAllocate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitAllocate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitAllocate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AllocateContext allocate() throws RecognitionException {
		AllocateContext _localctx = new AllocateContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_allocate);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(164);
			match(T__13);
			setState(165);
			match(T__8);
			setState(166);
			match(ID);
			setState(167);
			match(T__8);
			setState(168);
			((AllocateContext)_localctx).una = expr(0);
			setState(171);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__6) {
				{
				setState(169);
				match(T__6);
				setState(170);
				((AllocateContext)_localctx).dos = expr(0);
				}
			}

			setState(173);
			match(T__9);
			setState(174);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeallocateContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public DeallocateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_deallocate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterDeallocate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitDeallocate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitDeallocate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeallocateContext deallocate() throws RecognitionException {
		DeallocateContext _localctx = new DeallocateContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_deallocate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(176);
			match(T__14);
			setState(177);
			match(T__8);
			setState(178);
			match(ID);
			setState(179);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public List<Declaration2Context> declaration2() {
			return getRuleContexts(Declaration2Context.class);
		}
		public Declaration2Context declaration2(int i) {
			return getRuleContext(Declaration2Context.class,i);
		}
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(181);
			type();
			setState(182);
			match(T__10);
			setState(183);
			declaration2();
			setState(188);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__6) {
				{
				{
				setState(184);
				match(T__6);
				setState(185);
				declaration2();
				}
				}
				setState(190);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Declaration2Context extends ParserRuleContext {
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Declaration2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterDeclaration2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitDeclaration2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitDeclaration2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Declaration2Context declaration2() throws RecognitionException {
		Declaration2Context _localctx = new Declaration2Context(_ctx, getState());
		enterRule(_localctx, 24, RULE_declaration2);
		try {
			setState(195);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(191);
				match(ID);
				setState(192);
				match(T__15);
				setState(193);
				expr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(194);
				match(ID);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AsignacionContext extends ParserRuleContext {
		public AsignacionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_asignacion; }
	 
		public AsignacionContext() { }
		public void copyFrom(AsignacionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Asignacion_arreglos_DosDimContext extends AsignacionContext {
		public ExprContext exp_izq;
		public ExprContext exp_der;
		public ExprContext dato;
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Asignacion_arreglos_DosDimContext(AsignacionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterAsignacion_arreglos_DosDim(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitAsignacion_arreglos_DosDim(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitAsignacion_arreglos_DosDim(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Asignacion_arreglos_unaDim2Context extends AsignacionContext {
		public ExprContext posicion;
		public ExprContext dato;
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Asignacion_arreglos_unaDim2Context(AsignacionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterAsignacion_arreglos_unaDim2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitAsignacion_arreglos_unaDim2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitAsignacion_arreglos_unaDim2(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Asignacion_arreglos_unaDimContext extends AsignacionContext {
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public List<L_exprContext> l_expr() {
			return getRuleContexts(L_exprContext.class);
		}
		public L_exprContext l_expr(int i) {
			return getRuleContext(L_exprContext.class,i);
		}
		public Asignacion_arreglos_unaDimContext(AsignacionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterAsignacion_arreglos_unaDim(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitAsignacion_arreglos_unaDim(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitAsignacion_arreglos_unaDim(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Asignacion_primitivosContext extends AsignacionContext {
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Asignacion_primitivosContext(AsignacionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterAsignacion_primitivos(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitAsignacion_primitivos(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitAsignacion_primitivos(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AsignacionContext asignacion() throws RecognitionException {
		AsignacionContext _localctx = new AsignacionContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_asignacion);
		int _la;
		try {
			setState(228);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				_localctx = new Asignacion_primitivosContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(197);
				match(ID);
				setState(198);
				match(T__15);
				setState(199);
				expr(0);
				}
				break;
			case 2:
				_localctx = new Asignacion_arreglos_unaDimContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(200);
				match(ID);
				setState(201);
				match(T__15);
				setState(202);
				match(T__8);
				setState(203);
				match(T__16);
				setState(207);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 7)) & ~0x3f) == 0 && ((1L << (_la - 7)) & ((1L << (T__6 - 7)) | (1L << (T__8 - 7)) | (1L << (T__42 - 7)) | (1L << (T__43 - 7)) | (1L << (T__61 - 7)) | (1L << (INTEGER - 7)) | (1L << (REAL - 7)) | (1L << (ID - 7)) | (1L << (STRING - 7)) | (1L << (CHAR - 7)))) != 0)) {
					{
					{
					setState(204);
					l_expr();
					}
					}
					setState(209);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(210);
				match(T__16);
				setState(211);
				match(T__9);
				}
				break;
			case 3:
				_localctx = new Asignacion_arreglos_unaDim2Context(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(212);
				match(ID);
				setState(213);
				match(T__17);
				setState(214);
				((Asignacion_arreglos_unaDim2Context)_localctx).posicion = expr(0);
				setState(215);
				match(T__18);
				setState(216);
				match(T__15);
				setState(217);
				((Asignacion_arreglos_unaDim2Context)_localctx).dato = expr(0);
				}
				break;
			case 4:
				_localctx = new Asignacion_arreglos_DosDimContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(219);
				match(ID);
				setState(220);
				match(T__17);
				setState(221);
				((Asignacion_arreglos_DosDimContext)_localctx).exp_izq = expr(0);
				setState(222);
				match(T__6);
				setState(223);
				((Asignacion_arreglos_DosDimContext)_localctx).exp_der = expr(0);
				setState(224);
				match(T__18);
				setState(225);
				match(T__15);
				setState(226);
				((Asignacion_arreglos_DosDimContext)_localctx).dato = expr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintEntContext extends ParserRuleContext {
		public PrintEntContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_printEnt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterPrintEnt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitPrintEnt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitPrintEnt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrintEntContext printEnt() throws RecognitionException {
		PrintEntContext _localctx = new PrintEntContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_printEnt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(230);
			match(T__19);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_Context extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<InstruccionesContext> instrucciones() {
			return getRuleContexts(InstruccionesContext.class);
		}
		public InstruccionesContext instrucciones(int i) {
			return getRuleContext(InstruccionesContext.class,i);
		}
		public List<ElseIf_Context> elseIf_() {
			return getRuleContexts(ElseIf_Context.class);
		}
		public ElseIf_Context elseIf_(int i) {
			return getRuleContext(ElseIf_Context.class,i);
		}
		public Else_Context else_() {
			return getRuleContext(Else_Context.class,0);
		}
		public If_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterIf_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitIf_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitIf_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_Context if_() throws RecognitionException {
		If_Context _localctx = new If_Context(_ctx, getState());
		enterRule(_localctx, 30, RULE_if_);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(232);
			match(T__20);
			setState(233);
			match(T__8);
			setState(234);
			expr(0);
			setState(235);
			match(T__9);
			setState(236);
			match(T__21);
			setState(240);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (T__4 - 5)) | (1L << (T__13 - 5)) | (1L << (T__14 - 5)) | (1L << (T__19 - 5)) | (1L << (T__20 - 5)) | (1L << (T__23 - 5)) | (1L << (T__25 - 5)) | (1L << (T__26 - 5)) | (1L << (T__30 - 5)) | (1L << (T__33 - 5)) | (1L << (T__35 - 5)) | (1L << (T__36 - 5)) | (1L << (T__37 - 5)) | (1L << (T__38 - 5)) | (1L << (T__39 - 5)) | (1L << (ID - 5)))) != 0)) {
				{
				{
				setState(237);
				instrucciones();
				}
				}
				setState(242);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(246);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(243);
					elseIf_();
					}
					} 
				}
				setState(248);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			setState(250);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__22) {
				{
				setState(249);
				else_();
				}
			}

			setState(252);
			match(T__1);
			setState(253);
			match(T__20);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElseIf_Context extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<InstruccionesContext> instrucciones() {
			return getRuleContexts(InstruccionesContext.class);
		}
		public InstruccionesContext instrucciones(int i) {
			return getRuleContext(InstruccionesContext.class,i);
		}
		public ElseIf_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elseIf_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterElseIf_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitElseIf_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitElseIf_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ElseIf_Context elseIf_() throws RecognitionException {
		ElseIf_Context _localctx = new ElseIf_Context(_ctx, getState());
		enterRule(_localctx, 32, RULE_elseIf_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(255);
			match(T__22);
			setState(256);
			match(T__20);
			setState(257);
			match(T__8);
			setState(258);
			expr(0);
			setState(259);
			match(T__9);
			setState(260);
			match(T__21);
			setState(264);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (T__4 - 5)) | (1L << (T__13 - 5)) | (1L << (T__14 - 5)) | (1L << (T__19 - 5)) | (1L << (T__20 - 5)) | (1L << (T__23 - 5)) | (1L << (T__25 - 5)) | (1L << (T__26 - 5)) | (1L << (T__30 - 5)) | (1L << (T__33 - 5)) | (1L << (T__35 - 5)) | (1L << (T__36 - 5)) | (1L << (T__37 - 5)) | (1L << (T__38 - 5)) | (1L << (T__39 - 5)) | (1L << (ID - 5)))) != 0)) {
				{
				{
				setState(261);
				instrucciones();
				}
				}
				setState(266);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_Context extends ParserRuleContext {
		public List<InstruccionesContext> instrucciones() {
			return getRuleContexts(InstruccionesContext.class);
		}
		public InstruccionesContext instrucciones(int i) {
			return getRuleContext(InstruccionesContext.class,i);
		}
		public Else_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterElse_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitElse_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitElse_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Else_Context else_() throws RecognitionException {
		Else_Context _localctx = new Else_Context(_ctx, getState());
		enterRule(_localctx, 34, RULE_else_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(267);
			match(T__22);
			setState(271);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (T__4 - 5)) | (1L << (T__13 - 5)) | (1L << (T__14 - 5)) | (1L << (T__19 - 5)) | (1L << (T__20 - 5)) | (1L << (T__23 - 5)) | (1L << (T__25 - 5)) | (1L << (T__26 - 5)) | (1L << (T__30 - 5)) | (1L << (T__33 - 5)) | (1L << (T__35 - 5)) | (1L << (T__36 - 5)) | (1L << (T__37 - 5)) | (1L << (T__38 - 5)) | (1L << (T__39 - 5)) | (1L << (ID - 5)))) != 0)) {
				{
				{
				setState(268);
				instrucciones();
				}
				}
				setState(273);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Do_Context extends ParserRuleContext {
		public Token etiquete;
		public ExprContext fin;
		public ExprContext incremento;
		public Token etiqueta_end;
		public AsignacionContext asignacion() {
			return getRuleContext(AsignacionContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<InstruccionesContext> instrucciones() {
			return getRuleContexts(InstruccionesContext.class);
		}
		public InstruccionesContext instrucciones(int i) {
			return getRuleContext(InstruccionesContext.class,i);
		}
		public List<TerminalNode> ID() { return getTokens(GramaticaParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(GramaticaParser.ID, i);
		}
		public Do_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_do_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterDo_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitDo_(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitDo_(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Do_Context do_() throws RecognitionException {
		Do_Context _localctx = new Do_Context(_ctx, getState());
		enterRule(_localctx, 36, RULE_do_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(276);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(274);
				((Do_Context)_localctx).etiquete = match(ID);
				setState(275);
				match(T__12);
				}
			}

			setState(278);
			match(T__23);
			setState(279);
			asignacion();
			setState(280);
			match(T__6);
			setState(281);
			((Do_Context)_localctx).fin = expr(0);
			setState(282);
			match(T__6);
			setState(283);
			((Do_Context)_localctx).incremento = expr(0);
			setState(287);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (T__4 - 5)) | (1L << (T__13 - 5)) | (1L << (T__14 - 5)) | (1L << (T__19 - 5)) | (1L << (T__20 - 5)) | (1L << (T__23 - 5)) | (1L << (T__25 - 5)) | (1L << (T__26 - 5)) | (1L << (T__30 - 5)) | (1L << (T__33 - 5)) | (1L << (T__35 - 5)) | (1L << (T__36 - 5)) | (1L << (T__37 - 5)) | (1L << (T__38 - 5)) | (1L << (T__39 - 5)) | (1L << (ID - 5)))) != 0)) {
				{
				{
				setState(284);
				instrucciones();
				}
				}
				setState(289);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(290);
			match(T__1);
			setState(291);
			match(T__23);
			setState(293);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				{
				setState(292);
				((Do_Context)_localctx).etiqueta_end = match(ID);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Do_whileContext extends ParserRuleContext {
		public Token etiqueta;
		public Token etiqueta_end;
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public List<InstruccionesContext> instrucciones() {
			return getRuleContexts(InstruccionesContext.class);
		}
		public InstruccionesContext instrucciones(int i) {
			return getRuleContext(InstruccionesContext.class,i);
		}
		public List<TerminalNode> ID() { return getTokens(GramaticaParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(GramaticaParser.ID, i);
		}
		public Do_whileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_do_while; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterDo_while(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitDo_while(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitDo_while(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Do_whileContext do_while() throws RecognitionException {
		Do_whileContext _localctx = new Do_whileContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_do_while);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(297);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(295);
				((Do_whileContext)_localctx).etiqueta = match(ID);
				setState(296);
				match(T__12);
				}
			}

			setState(299);
			match(T__23);
			setState(300);
			match(T__24);
			setState(301);
			match(T__8);
			setState(302);
			expr(0);
			setState(303);
			match(T__9);
			setState(307);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (T__4 - 5)) | (1L << (T__13 - 5)) | (1L << (T__14 - 5)) | (1L << (T__19 - 5)) | (1L << (T__20 - 5)) | (1L << (T__23 - 5)) | (1L << (T__25 - 5)) | (1L << (T__26 - 5)) | (1L << (T__30 - 5)) | (1L << (T__33 - 5)) | (1L << (T__35 - 5)) | (1L << (T__36 - 5)) | (1L << (T__37 - 5)) | (1L << (T__38 - 5)) | (1L << (T__39 - 5)) | (1L << (ID - 5)))) != 0)) {
				{
				{
				setState(304);
				instrucciones();
				}
				}
				setState(309);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(310);
			match(T__1);
			setState(311);
			match(T__23);
			setState(313);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				{
				setState(312);
				((Do_whileContext)_localctx).etiqueta_end = match(ID);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExitContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public ExitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterExit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitExit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitExit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExitContext exit() throws RecognitionException {
		ExitContext _localctx = new ExitContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_exit);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(315);
			match(T__25);
			setState(317);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				{
				setState(316);
				match(ID);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CycleContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public CycleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cycle; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterCycle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitCycle(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitCycle(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CycleContext cycle() throws RecognitionException {
		CycleContext _localctx = new CycleContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_cycle);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(319);
			match(T__26);
			setState(321);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				{
				setState(320);
				match(ID);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class List_paramContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public List_paramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_list_param; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterList_param(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitList_param(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitList_param(this);
			else return visitor.visitChildren(this);
		}
	}

	public final List_paramContext list_param() throws RecognitionException {
		List_paramContext _localctx = new List_paramContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_list_param);
		try {
			setState(326);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__6:
				enterOuterAlt(_localctx, 1);
				{
				setState(323);
				match(T__6);
				setState(324);
				match(ID);
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(325);
				match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntentContext extends ParserRuleContext {
		public IntentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intent; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterIntent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitIntent(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitIntent(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntentContext intent() throws RecognitionException {
		IntentContext _localctx = new IntentContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_intent);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(328);
			match(T__6);
			setState(329);
			match(T__27);
			setState(330);
			match(T__8);
			setState(331);
			match(T__28);
			setState(332);
			match(T__9);
			setState(333);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Declaracion_parametro_funcContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public IntentContext intent() {
			return getRuleContext(IntentContext.class,0);
		}
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public List_paramContext list_param() {
			return getRuleContext(List_paramContext.class,0);
		}
		public Declaracion_parametro_funcContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaracion_parametro_func; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterDeclaracion_parametro_func(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitDeclaracion_parametro_func(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitDeclaracion_parametro_func(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Declaracion_parametro_funcContext declaracion_parametro_func() throws RecognitionException {
		Declaracion_parametro_funcContext _localctx = new Declaracion_parametro_funcContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_declaracion_parametro_func);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(335);
			type();
			setState(336);
			intent();
			setState(337);
			match(ID);
			setState(343);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__8) {
				{
				setState(338);
				match(T__8);
				setState(340);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__6 || _la==ID) {
					{
					setState(339);
					list_param();
					}
				}

				setState(342);
				match(T__9);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubroutineContext extends ParserRuleContext {
		public Token nom;
		public Token nom_end;
		public ImplicitContext implicit() {
			return getRuleContext(ImplicitContext.class,0);
		}
		public List<TerminalNode> ID() { return getTokens(GramaticaParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(GramaticaParser.ID, i);
		}
		public List<List_paramContext> list_param() {
			return getRuleContexts(List_paramContext.class);
		}
		public List_paramContext list_param(int i) {
			return getRuleContext(List_paramContext.class,i);
		}
		public List<Declaracion_parametro_funcContext> declaracion_parametro_func() {
			return getRuleContexts(Declaracion_parametro_funcContext.class);
		}
		public Declaracion_parametro_funcContext declaracion_parametro_func(int i) {
			return getRuleContext(Declaracion_parametro_funcContext.class,i);
		}
		public List<InstruccionesContext> instrucciones() {
			return getRuleContexts(InstruccionesContext.class);
		}
		public InstruccionesContext instrucciones(int i) {
			return getRuleContext(InstruccionesContext.class,i);
		}
		public SubroutineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subroutine; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterSubroutine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitSubroutine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitSubroutine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubroutineContext subroutine() throws RecognitionException {
		SubroutineContext _localctx = new SubroutineContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_subroutine);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(345);
			match(T__29);
			setState(346);
			((SubroutineContext)_localctx).nom = match(ID);
			setState(347);
			match(T__8);
			setState(351);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__6 || _la==ID) {
				{
				{
				setState(348);
				list_param();
				}
				}
				setState(353);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(354);
			match(T__9);
			setState(355);
			implicit();
			setState(359);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(356);
					declaracion_parametro_func();
					}
					} 
				}
				setState(361);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			}
			setState(365);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (T__4 - 5)) | (1L << (T__13 - 5)) | (1L << (T__14 - 5)) | (1L << (T__19 - 5)) | (1L << (T__20 - 5)) | (1L << (T__23 - 5)) | (1L << (T__25 - 5)) | (1L << (T__26 - 5)) | (1L << (T__30 - 5)) | (1L << (T__33 - 5)) | (1L << (T__35 - 5)) | (1L << (T__36 - 5)) | (1L << (T__37 - 5)) | (1L << (T__38 - 5)) | (1L << (T__39 - 5)) | (1L << (ID - 5)))) != 0)) {
				{
				{
				setState(362);
				instrucciones();
				}
				}
				setState(367);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(368);
			match(T__1);
			setState(369);
			match(T__29);
			setState(370);
			((SubroutineContext)_localctx).nom_end = match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public List<L_exprContext> l_expr() {
			return getRuleContexts(L_exprContext.class);
		}
		public L_exprContext l_expr(int i) {
			return getRuleContext(L_exprContext.class,i);
		}
		public CallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_call; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallContext call() throws RecognitionException {
		CallContext _localctx = new CallContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_call);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(372);
			match(T__30);
			setState(373);
			match(ID);
			setState(374);
			match(T__8);
			setState(378);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 7)) & ~0x3f) == 0 && ((1L << (_la - 7)) & ((1L << (T__6 - 7)) | (1L << (T__8 - 7)) | (1L << (T__42 - 7)) | (1L << (T__43 - 7)) | (1L << (T__61 - 7)) | (1L << (INTEGER - 7)) | (1L << (REAL - 7)) | (1L << (ID - 7)) | (1L << (STRING - 7)) | (1L << (CHAR - 7)))) != 0)) {
				{
				{
				setState(375);
				l_expr();
				}
				}
				setState(380);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(381);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public Token nom;
		public Token retorno;
		public Token nom_end;
		public ImplicitContext implicit() {
			return getRuleContext(ImplicitContext.class,0);
		}
		public List<TerminalNode> ID() { return getTokens(GramaticaParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(GramaticaParser.ID, i);
		}
		public List<List_paramContext> list_param() {
			return getRuleContexts(List_paramContext.class);
		}
		public List_paramContext list_param(int i) {
			return getRuleContext(List_paramContext.class,i);
		}
		public List<Declaracion_parametro_funcContext> declaracion_parametro_func() {
			return getRuleContexts(Declaracion_parametro_funcContext.class);
		}
		public Declaracion_parametro_funcContext declaracion_parametro_func(int i) {
			return getRuleContext(Declaracion_parametro_funcContext.class,i);
		}
		public List<InstruccionesContext> instrucciones() {
			return getRuleContexts(InstruccionesContext.class);
		}
		public InstruccionesContext instrucciones(int i) {
			return getRuleContext(InstruccionesContext.class,i);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_function);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(383);
			match(T__31);
			setState(384);
			((FunctionContext)_localctx).nom = match(ID);
			setState(385);
			match(T__8);
			setState(389);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__6 || _la==ID) {
				{
				{
				setState(386);
				list_param();
				}
				}
				setState(391);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(392);
			match(T__9);
			setState(393);
			match(T__32);
			setState(394);
			match(T__8);
			setState(396);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(395);
				((FunctionContext)_localctx).retorno = match(ID);
				}
			}

			setState(398);
			match(T__9);
			setState(399);
			implicit();
			setState(403);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,37,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(400);
					declaracion_parametro_func();
					}
					} 
				}
				setState(405);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,37,_ctx);
			}
			setState(409);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (T__4 - 5)) | (1L << (T__13 - 5)) | (1L << (T__14 - 5)) | (1L << (T__19 - 5)) | (1L << (T__20 - 5)) | (1L << (T__23 - 5)) | (1L << (T__25 - 5)) | (1L << (T__26 - 5)) | (1L << (T__30 - 5)) | (1L << (T__33 - 5)) | (1L << (T__35 - 5)) | (1L << (T__36 - 5)) | (1L << (T__37 - 5)) | (1L << (T__38 - 5)) | (1L << (T__39 - 5)) | (1L << (ID - 5)))) != 0)) {
				{
				{
				setState(406);
				instrucciones();
				}
				}
				setState(411);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(412);
			match(T__1);
			setState(413);
			match(T__31);
			setState(414);
			((FunctionContext)_localctx).nom_end = match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Call_functionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public List<L_exprContext> l_expr() {
			return getRuleContexts(L_exprContext.class);
		}
		public L_exprContext l_expr(int i) {
			return getRuleContext(L_exprContext.class,i);
		}
		public Call_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_call_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterCall_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitCall_function(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitCall_function(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Call_functionContext call_function() throws RecognitionException {
		Call_functionContext _localctx = new Call_functionContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_call_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(416);
			match(ID);
			setState(417);
			match(T__8);
			setState(421);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 7)) & ~0x3f) == 0 && ((1L << (_la - 7)) & ((1L << (T__6 - 7)) | (1L << (T__8 - 7)) | (1L << (T__42 - 7)) | (1L << (T__43 - 7)) | (1L << (T__61 - 7)) | (1L << (INTEGER - 7)) | (1L << (REAL - 7)) | (1L << (ID - 7)) | (1L << (STRING - 7)) | (1L << (CHAR - 7)))) != 0)) {
				{
				{
				setState(418);
				l_expr();
				}
				}
				setState(423);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(424);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintContext extends ParserRuleContext {
		public List<Estado_exprContext> estado_expr() {
			return getRuleContexts(Estado_exprContext.class);
		}
		public Estado_exprContext estado_expr(int i) {
			return getRuleContext(Estado_exprContext.class,i);
		}
		public PrintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterPrint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitPrint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitPrint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrintContext print() throws RecognitionException {
		PrintContext _localctx = new PrintContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_print);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(426);
			match(T__33);
			setState(427);
			match(T__34);
			setState(428);
			match(T__6);
			setState(429);
			estado_expr();
			setState(434);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__6) {
				{
				{
				setState(430);
				match(T__6);
				setState(431);
				estado_expr();
				}
				}
				setState(436);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Estado_exprContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Estado_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_estado_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterEstado_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitEstado_expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitEstado_expr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Estado_exprContext estado_expr() throws RecognitionException {
		Estado_exprContext _localctx = new Estado_exprContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_estado_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(437);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(439);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StrExprContext extends ExprContext {
		public Token str;
		public TerminalNode STRING() { return getToken(GramaticaParser.STRING, 0); }
		public StrExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterStrExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitStrExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitStrExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CharExprContext extends ExprContext {
		public TerminalNode CHAR() { return getToken(GramaticaParser.CHAR, 0); }
		public CharExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterCharExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitCharExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitCharExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OpExprContext extends ExprContext {
		public ExprContext left;
		public Token op;
		public ExprContext right;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public OpExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterOpExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitOpExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitOpExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RealExprContext extends ExprContext {
		public Token real;
		public TerminalNode REAL() { return getToken(GramaticaParser.REAL, 0); }
		public RealExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterRealExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitRealExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitRealExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EnteroExprContext extends ExprContext {
		public Token entero;
		public TerminalNode INTEGER() { return getToken(GramaticaParser.INTEGER, 0); }
		public EnteroExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterEnteroExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitEnteroExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitEnteroExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Relacionales_ExprContext extends ExprContext {
		public ExprContext left;
		public Token op;
		public ExprContext right;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Relacionales_ExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterRelacionales_Expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitRelacionales_Expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitRelacionales_Expr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Call_function_exprContext extends ExprContext {
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public List<L_exprContext> l_expr() {
			return getRuleContexts(L_exprContext.class);
		}
		public L_exprContext l_expr(int i) {
			return getRuleContext(L_exprContext.class,i);
		}
		public Call_function_exprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterCall_function_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitCall_function_expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitCall_function_expr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParenExprContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ParenExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterParenExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitParenExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitParenExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GetSize_ArregloContext extends ExprContext {
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public GetSize_ArregloContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterGetSize_Arreglo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitGetSize_Arreglo(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitGetSize_Arreglo(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotExprContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public NotExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterNotExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitNotExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitNotExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnaryMinusExprContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public UnaryMinusExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterUnaryMinusExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitUnaryMinusExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitUnaryMinusExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolExprContext extends ExprContext {
		public Token b;
		public BoolExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterBoolExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitBoolExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitBoolExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdExprContext extends ExprContext {
		public Token id;
		public TerminalNode ID() { return getToken(GramaticaParser.ID, 0); }
		public IdExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).enterIdExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GramaticaListener ) ((GramaticaListener)listener).exitIdExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof GramaticaVisitor ) return ((GramaticaVisitor<? extends T>)visitor).visitIdExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 64;
		enterRecursionRule(_localctx, 64, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(476);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,42,_ctx) ) {
			case 1:
				{
				_localctx = new ParenExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(442);
				match(T__8);
				setState(443);
				expr(0);
				setState(444);
				match(T__9);
				}
				break;
			case 2:
				{
				_localctx = new EnteroExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(446);
				((EnteroExprContext)_localctx).entero = match(INTEGER);
				}
				break;
			case 3:
				{
				_localctx = new StrExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(447);
				((StrExprContext)_localctx).str = match(STRING);
				}
				break;
			case 4:
				{
				_localctx = new RealExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(448);
				((RealExprContext)_localctx).real = match(REAL);
				}
				break;
			case 5:
				{
				_localctx = new IdExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(449);
				((IdExprContext)_localctx).id = match(ID);
				}
				break;
			case 6:
				{
				_localctx = new CharExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(450);
				match(CHAR);
				}
				break;
			case 7:
				{
				_localctx = new UnaryMinusExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(451);
				match(T__42);
				setState(452);
				expr(14);
				}
				break;
			case 8:
				{
				_localctx = new NotExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(453);
				match(T__43);
				setState(454);
				match(T__44);
				setState(455);
				match(T__43);
				setState(456);
				expr(13);
				}
				break;
			case 9:
				{
				_localctx = new BoolExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(457);
				match(T__43);
				setState(458);
				((BoolExprContext)_localctx).b = match(T__45);
				setState(459);
				match(T__43);
				}
				break;
			case 10:
				{
				_localctx = new BoolExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(460);
				match(T__43);
				setState(461);
				((BoolExprContext)_localctx).b = match(T__46);
				setState(462);
				match(T__43);
				}
				break;
			case 11:
				{
				_localctx = new Call_function_exprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(463);
				match(ID);
				setState(464);
				match(T__8);
				setState(468);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 7)) & ~0x3f) == 0 && ((1L << (_la - 7)) & ((1L << (T__6 - 7)) | (1L << (T__8 - 7)) | (1L << (T__42 - 7)) | (1L << (T__43 - 7)) | (1L << (T__61 - 7)) | (1L << (INTEGER - 7)) | (1L << (REAL - 7)) | (1L << (ID - 7)) | (1L << (STRING - 7)) | (1L << (CHAR - 7)))) != 0)) {
					{
					{
					setState(465);
					l_expr();
					}
					}
					setState(470);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(471);
				match(T__9);
				}
				break;
			case 12:
				{
				_localctx = new GetSize_ArregloContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(472);
				match(T__61);
				setState(473);
				match(T__8);
				setState(474);
				match(ID);
				setState(475);
				match(T__9);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(513);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,44,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(511);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,43,_ctx) ) {
					case 1:
						{
						_localctx = new OpExprContext(new ExprContext(_parentctx, _parentState));
						((OpExprContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(478);
						if (!(precpred(_ctx, 23))) throw new FailedPredicateException(this, "precpred(_ctx, 23)");
						setState(479);
						((OpExprContext)_localctx).op = match(T__40);
						setState(480);
						((OpExprContext)_localctx).right = expr(24);
						}
						break;
					case 2:
						{
						_localctx = new OpExprContext(new ExprContext(_parentctx, _parentState));
						((OpExprContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(481);
						if (!(precpred(_ctx, 22))) throw new FailedPredicateException(this, "precpred(_ctx, 22)");
						setState(482);
						((OpExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__16 || _la==T__34) ) {
							((OpExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(483);
						((OpExprContext)_localctx).right = expr(23);
						}
						break;
					case 3:
						{
						_localctx = new OpExprContext(new ExprContext(_parentctx, _parentState));
						((OpExprContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(484);
						if (!(precpred(_ctx, 21))) throw new FailedPredicateException(this, "precpred(_ctx, 21)");
						setState(485);
						((OpExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__41 || _la==T__42) ) {
							((OpExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(486);
						((OpExprContext)_localctx).right = expr(22);
						}
						break;
					case 4:
						{
						_localctx = new Relacionales_ExprContext(new ExprContext(_parentctx, _parentState));
						((Relacionales_ExprContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(487);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(488);
						((Relacionales_ExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__47 || _la==T__48) ) {
							((Relacionales_ExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(489);
						((Relacionales_ExprContext)_localctx).right = expr(11);
						}
						break;
					case 5:
						{
						_localctx = new Relacionales_ExprContext(new ExprContext(_parentctx, _parentState));
						((Relacionales_ExprContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(490);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(491);
						((Relacionales_ExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__49 || _la==T__50) ) {
							((Relacionales_ExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(492);
						((Relacionales_ExprContext)_localctx).right = expr(10);
						}
						break;
					case 6:
						{
						_localctx = new Relacionales_ExprContext(new ExprContext(_parentctx, _parentState));
						((Relacionales_ExprContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(493);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(494);
						((Relacionales_ExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__51 || _la==T__52) ) {
							((Relacionales_ExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(495);
						((Relacionales_ExprContext)_localctx).right = expr(9);
						}
						break;
					case 7:
						{
						_localctx = new Relacionales_ExprContext(new ExprContext(_parentctx, _parentState));
						((Relacionales_ExprContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(496);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(497);
						((Relacionales_ExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__53 || _la==T__54) ) {
							((Relacionales_ExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(498);
						((Relacionales_ExprContext)_localctx).right = expr(8);
						}
						break;
					case 8:
						{
						_localctx = new Relacionales_ExprContext(new ExprContext(_parentctx, _parentState));
						((Relacionales_ExprContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(499);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(500);
						((Relacionales_ExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__55 || _la==T__56) ) {
							((Relacionales_ExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(501);
						((Relacionales_ExprContext)_localctx).right = expr(7);
						}
						break;
					case 9:
						{
						_localctx = new Relacionales_ExprContext(new ExprContext(_parentctx, _parentState));
						((Relacionales_ExprContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(502);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(503);
						((Relacionales_ExprContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__57 || _la==T__58) ) {
							((Relacionales_ExprContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(504);
						((Relacionales_ExprContext)_localctx).right = expr(6);
						}
						break;
					case 10:
						{
						_localctx = new Relacionales_ExprContext(new ExprContext(_parentctx, _parentState));
						((Relacionales_ExprContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(505);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(506);
						((Relacionales_ExprContext)_localctx).op = match(T__59);
						setState(507);
						((Relacionales_ExprContext)_localctx).right = expr(5);
						}
						break;
					case 11:
						{
						_localctx = new Relacionales_ExprContext(new ExprContext(_parentctx, _parentState));
						((Relacionales_ExprContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(508);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(509);
						((Relacionales_ExprContext)_localctx).op = match(T__60);
						setState(510);
						((Relacionales_ExprContext)_localctx).right = expr(4);
						}
						break;
					}
					} 
				}
				setState(515);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,44,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 32:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 23);
		case 1:
			return precpred(_ctx, 22);
		case 2:
			return precpred(_ctx, 21);
		case 3:
			return precpred(_ctx, 10);
		case 4:
			return precpred(_ctx, 9);
		case 5:
			return precpred(_ctx, 8);
		case 6:
			return precpred(_ctx, 7);
		case 7:
			return precpred(_ctx, 6);
		case 8:
			return precpred(_ctx, 5);
		case 9:
			return precpred(_ctx, 4);
		case 10:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\u0004\u0001E\u0205\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0002"+
		"\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004\u0002"+
		"\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007\u0002"+
		"\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b\u0002"+
		"\f\u0007\f\u0002\r\u0007\r\u0002\u000e\u0007\u000e\u0002\u000f\u0007\u000f"+
		"\u0002\u0010\u0007\u0010\u0002\u0011\u0007\u0011\u0002\u0012\u0007\u0012"+
		"\u0002\u0013\u0007\u0013\u0002\u0014\u0007\u0014\u0002\u0015\u0007\u0015"+
		"\u0002\u0016\u0007\u0016\u0002\u0017\u0007\u0017\u0002\u0018\u0007\u0018"+
		"\u0002\u0019\u0007\u0019\u0002\u001a\u0007\u001a\u0002\u001b\u0007\u001b"+
		"\u0002\u001c\u0007\u001c\u0002\u001d\u0007\u001d\u0002\u001e\u0007\u001e"+
		"\u0002\u001f\u0007\u001f\u0002 \u0007 \u0001\u0000\u0001\u0000\u0005\u0000"+
		"E\b\u0000\n\u0000\f\u0000H\t\u0000\u0001\u0000\u0001\u0000\u0001\u0000"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0005\u0001Q\b\u0001"+
		"\n\u0001\f\u0001T\t\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0003\u0004\u0003^\b\u0003"+
		"\u000b\u0003\f\u0003_\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004"+
		"\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0003\u0004"+
		"q\b\u0004\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0001\u0006"+
		"\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0004\u0006|\b\u0006"+
		"\u000b\u0006\f\u0006}\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006"+
		"\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0004\u0006"+
		"\u0089\b\u0006\u000b\u0006\f\u0006\u008a\u0001\u0006\u0001\u0006\u0001"+
		"\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001"+
		"\u0006\u0003\u0006\u0096\b\u0006\u0001\u0007\u0001\u0007\u0001\u0007\u0001"+
		"\u0007\u0003\u0007\u009c\b\u0007\u0001\u0007\u0001\u0007\u0001\b\u0001"+
		"\b\u0001\b\u0003\b\u00a3\b\b\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001"+
		"\t\u0001\t\u0003\t\u00ac\b\t\u0001\t\u0001\t\u0001\t\u0001\n\u0001\n\u0001"+
		"\n\u0001\n\u0001\n\u0001\u000b\u0001\u000b\u0001\u000b\u0001\u000b\u0001"+
		"\u000b\u0005\u000b\u00bb\b\u000b\n\u000b\f\u000b\u00be\t\u000b\u0001\f"+
		"\u0001\f\u0001\f\u0001\f\u0003\f\u00c4\b\f\u0001\r\u0001\r\u0001\r\u0001"+
		"\r\u0001\r\u0001\r\u0001\r\u0001\r\u0005\r\u00ce\b\r\n\r\f\r\u00d1\t\r"+
		"\u0001\r\u0001\r\u0001\r\u0001\r\u0001\r\u0001\r\u0001\r\u0001\r\u0001"+
		"\r\u0001\r\u0001\r\u0001\r\u0001\r\u0001\r\u0001\r\u0001\r\u0001\r\u0001"+
		"\r\u0003\r\u00e5\b\r\u0001\u000e\u0001\u000e\u0001\u000f\u0001\u000f\u0001"+
		"\u000f\u0001\u000f\u0001\u000f\u0001\u000f\u0005\u000f\u00ef\b\u000f\n"+
		"\u000f\f\u000f\u00f2\t\u000f\u0001\u000f\u0005\u000f\u00f5\b\u000f\n\u000f"+
		"\f\u000f\u00f8\t\u000f\u0001\u000f\u0003\u000f\u00fb\b\u000f\u0001\u000f"+
		"\u0001\u000f\u0001\u000f\u0001\u0010\u0001\u0010\u0001\u0010\u0001\u0010"+
		"\u0001\u0010\u0001\u0010\u0001\u0010\u0005\u0010\u0107\b\u0010\n\u0010"+
		"\f\u0010\u010a\t\u0010\u0001\u0011\u0001\u0011\u0005\u0011\u010e\b\u0011"+
		"\n\u0011\f\u0011\u0111\t\u0011\u0001\u0012\u0001\u0012\u0003\u0012\u0115"+
		"\b\u0012\u0001\u0012\u0001\u0012\u0001\u0012\u0001\u0012\u0001\u0012\u0001"+
		"\u0012\u0001\u0012\u0005\u0012\u011e\b\u0012\n\u0012\f\u0012\u0121\t\u0012"+
		"\u0001\u0012\u0001\u0012\u0001\u0012\u0003\u0012\u0126\b\u0012\u0001\u0013"+
		"\u0001\u0013\u0003\u0013\u012a\b\u0013\u0001\u0013\u0001\u0013\u0001\u0013"+
		"\u0001\u0013\u0001\u0013\u0001\u0013\u0005\u0013\u0132\b\u0013\n\u0013"+
		"\f\u0013\u0135\t\u0013\u0001\u0013\u0001\u0013\u0001\u0013\u0003\u0013"+
		"\u013a\b\u0013\u0001\u0014\u0001\u0014\u0003\u0014\u013e\b\u0014\u0001"+
		"\u0015\u0001\u0015\u0003\u0015\u0142\b\u0015\u0001\u0016\u0001\u0016\u0001"+
		"\u0016\u0003\u0016\u0147\b\u0016\u0001\u0017\u0001\u0017\u0001\u0017\u0001"+
		"\u0017\u0001\u0017\u0001\u0017\u0001\u0017\u0001\u0018\u0001\u0018\u0001"+
		"\u0018\u0001\u0018\u0001\u0018\u0003\u0018\u0155\b\u0018\u0001\u0018\u0003"+
		"\u0018\u0158\b\u0018\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u0019\u0005"+
		"\u0019\u015e\b\u0019\n\u0019\f\u0019\u0161\t\u0019\u0001\u0019\u0001\u0019"+
		"\u0001\u0019\u0005\u0019\u0166\b\u0019\n\u0019\f\u0019\u0169\t\u0019\u0001"+
		"\u0019\u0005\u0019\u016c\b\u0019\n\u0019\f\u0019\u016f\t\u0019\u0001\u0019"+
		"\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u001a\u0001\u001a\u0001\u001a"+
		"\u0001\u001a\u0005\u001a\u0179\b\u001a\n\u001a\f\u001a\u017c\t\u001a\u0001"+
		"\u001a\u0001\u001a\u0001\u001b\u0001\u001b\u0001\u001b\u0001\u001b\u0005"+
		"\u001b\u0184\b\u001b\n\u001b\f\u001b\u0187\t\u001b\u0001\u001b\u0001\u001b"+
		"\u0001\u001b\u0001\u001b\u0003\u001b\u018d\b\u001b\u0001\u001b\u0001\u001b"+
		"\u0001\u001b\u0005\u001b\u0192\b\u001b\n\u001b\f\u001b\u0195\t\u001b\u0001"+
		"\u001b\u0005\u001b\u0198\b\u001b\n\u001b\f\u001b\u019b\t\u001b\u0001\u001b"+
		"\u0001\u001b\u0001\u001b\u0001\u001b\u0001\u001c\u0001\u001c\u0001\u001c"+
		"\u0005\u001c\u01a4\b\u001c\n\u001c\f\u001c\u01a7\t\u001c\u0001\u001c\u0001"+
		"\u001c\u0001\u001d\u0001\u001d\u0001\u001d\u0001\u001d\u0001\u001d\u0001"+
		"\u001d\u0005\u001d\u01b1\b\u001d\n\u001d\f\u001d\u01b4\t\u001d\u0001\u001e"+
		"\u0001\u001e\u0001\u001f\u0001\u001f\u0001 \u0001 \u0001 \u0001 \u0001"+
		" \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001"+
		" \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001"+
		" \u0005 \u01d3\b \n \f \u01d6\t \u0001 \u0001 \u0001 \u0001 \u0001 \u0003"+
		" \u01dd\b \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001"+
		" \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001"+
		" \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001 \u0001"+
		" \u0001 \u0001 \u0001 \u0001 \u0005 \u0200\b \n \f \u0203\t \u0001 \u0000"+
		"\u0001@!\u0000\u0002\u0004\u0006\b\n\f\u000e\u0010\u0012\u0014\u0016\u0018"+
		"\u001a\u001c\u001e \"$&(*,.02468:<>@\u0000\t\u0001\u0000$(\u0002\u0000"+
		"\u0011\u0011##\u0001\u0000*+\u0001\u000001\u0001\u000023\u0001\u00004"+
		"5\u0001\u000067\u0001\u000089\u0001\u0000:;\u0233\u0000F\u0001\u0000\u0000"+
		"\u0000\u0002L\u0001\u0000\u0000\u0000\u0004Y\u0001\u0000\u0000\u0000\u0006"+
		"]\u0001\u0000\u0000\u0000\bp\u0001\u0000\u0000\u0000\nr\u0001\u0000\u0000"+
		"\u0000\f\u0095\u0001\u0000\u0000\u0000\u000e\u0097\u0001\u0000\u0000\u0000"+
		"\u0010\u00a2\u0001\u0000\u0000\u0000\u0012\u00a4\u0001\u0000\u0000\u0000"+
		"\u0014\u00b0\u0001\u0000\u0000\u0000\u0016\u00b5\u0001\u0000\u0000\u0000"+
		"\u0018\u00c3\u0001\u0000\u0000\u0000\u001a\u00e4\u0001\u0000\u0000\u0000"+
		"\u001c\u00e6\u0001\u0000\u0000\u0000\u001e\u00e8\u0001\u0000\u0000\u0000"+
		" \u00ff\u0001\u0000\u0000\u0000\"\u010b\u0001\u0000\u0000\u0000$\u0114"+
		"\u0001\u0000\u0000\u0000&\u0129\u0001\u0000\u0000\u0000(\u013b\u0001\u0000"+
		"\u0000\u0000*\u013f\u0001\u0000\u0000\u0000,\u0146\u0001\u0000\u0000\u0000"+
		".\u0148\u0001\u0000\u0000\u00000\u014f\u0001\u0000\u0000\u00002\u0159"+
		"\u0001\u0000\u0000\u00004\u0174\u0001\u0000\u0000\u00006\u017f\u0001\u0000"+
		"\u0000\u00008\u01a0\u0001\u0000\u0000\u0000:\u01aa\u0001\u0000\u0000\u0000"+
		"<\u01b5\u0001\u0000\u0000\u0000>\u01b7\u0001\u0000\u0000\u0000@\u01dc"+
		"\u0001\u0000\u0000\u0000BE\u00036\u001b\u0000CE\u00032\u0019\u0000DB\u0001"+
		"\u0000\u0000\u0000DC\u0001\u0000\u0000\u0000EH\u0001\u0000\u0000\u0000"+
		"FD\u0001\u0000\u0000\u0000FG\u0001\u0000\u0000\u0000GI\u0001\u0000\u0000"+
		"\u0000HF\u0001\u0000\u0000\u0000IJ\u0003\u0002\u0001\u0000JK\u0005\u0000"+
		"\u0000\u0001K\u0001\u0001\u0000\u0000\u0000LM\u0005\u0001\u0000\u0000"+
		"MN\u0005B\u0000\u0000NR\u0003\u0004\u0002\u0000OQ\u0003\b\u0004\u0000"+
		"PO\u0001\u0000\u0000\u0000QT\u0001\u0000\u0000\u0000RP\u0001\u0000\u0000"+
		"\u0000RS\u0001\u0000\u0000\u0000SU\u0001\u0000\u0000\u0000TR\u0001\u0000"+
		"\u0000\u0000UV\u0005\u0002\u0000\u0000VW\u0005\u0001\u0000\u0000WX\u0005"+
		"B\u0000\u0000X\u0003\u0001\u0000\u0000\u0000YZ\u0005\u0003\u0000\u0000"+
		"Z[\u0005\u0004\u0000\u0000[\u0005\u0001\u0000\u0000\u0000\\^\u0003\b\u0004"+
		"\u0000]\\\u0001\u0000\u0000\u0000^_\u0001\u0000\u0000\u0000_]\u0001\u0000"+
		"\u0000\u0000_`\u0001\u0000\u0000\u0000`\u0007\u0001\u0000\u0000\u0000"+
		"aq\u0003\n\u0005\u0000bq\u0003\u0016\u000b\u0000cq\u0003:\u001d\u0000"+
		"dq\u0003\u001a\r\u0000eq\u0003\f\u0006\u0000fq\u0003\u0012\t\u0000gq\u0003"+
		"\u0014\n\u0000hq\u0003\u001c\u000e\u0000iq\u0003\u001e\u000f\u0000jq\u0003"+
		"$\u0012\u0000kq\u0003&\u0013\u0000lq\u0003(\u0014\u0000mq\u0003*\u0015"+
		"\u0000nq\u00034\u001a\u0000oq\u00038\u001c\u0000pa\u0001\u0000\u0000\u0000"+
		"pb\u0001\u0000\u0000\u0000pc\u0001\u0000\u0000\u0000pd\u0001\u0000\u0000"+
		"\u0000pe\u0001\u0000\u0000\u0000pf\u0001\u0000\u0000\u0000pg\u0001\u0000"+
		"\u0000\u0000ph\u0001\u0000\u0000\u0000pi\u0001\u0000\u0000\u0000pj\u0001"+
		"\u0000\u0000\u0000pk\u0001\u0000\u0000\u0000pl\u0001\u0000\u0000\u0000"+
		"pm\u0001\u0000\u0000\u0000pn\u0001\u0000\u0000\u0000po\u0001\u0000\u0000"+
		"\u0000q\t\u0001\u0000\u0000\u0000rs\u0005\u0005\u0000\u0000st\u0003\u0006"+
		"\u0003\u0000tu\u0005\u0006\u0000\u0000u\u000b\u0001\u0000\u0000\u0000"+
		"vw\u0003>\u001f\u0000wx\u0005\u0007\u0000\u0000xy\u0005\b\u0000\u0000"+
		"y{\u0005\t\u0000\u0000z|\u0003\u0010\b\u0000{z\u0001\u0000\u0000\u0000"+
		"|}\u0001\u0000\u0000\u0000}{\u0001\u0000\u0000\u0000}~\u0001\u0000\u0000"+
		"\u0000~\u007f\u0001\u0000\u0000\u0000\u007f\u0080\u0005\n\u0000\u0000"+
		"\u0080\u0081\u0005\u000b\u0000\u0000\u0081\u0082\u0005B\u0000\u0000\u0082"+
		"\u0096\u0001\u0000\u0000\u0000\u0083\u0084\u0003>\u001f\u0000\u0084\u0085"+
		"\u0005\u000b\u0000\u0000\u0085\u0086\u0005B\u0000\u0000\u0086\u0088\u0005"+
		"\t\u0000\u0000\u0087\u0089\u0003\u0010\b\u0000\u0088\u0087\u0001\u0000"+
		"\u0000\u0000\u0089\u008a\u0001\u0000\u0000\u0000\u008a\u0088\u0001\u0000"+
		"\u0000\u0000\u008a\u008b\u0001\u0000\u0000\u0000\u008b\u008c\u0001\u0000"+
		"\u0000\u0000\u008c\u008d\u0005\n\u0000\u0000\u008d\u0096\u0001\u0000\u0000"+
		"\u0000\u008e\u008f\u0003>\u001f\u0000\u008f\u0090\u0005\u0007\u0000\u0000"+
		"\u0090\u0091\u0005\f\u0000\u0000\u0091\u0092\u0005\u000b\u0000\u0000\u0092"+
		"\u0093\u0005B\u0000\u0000\u0093\u0094\u0003\u000e\u0007\u0000\u0094\u0096"+
		"\u0001\u0000\u0000\u0000\u0095v\u0001\u0000\u0000\u0000\u0095\u0083\u0001"+
		"\u0000\u0000\u0000\u0095\u008e\u0001\u0000\u0000\u0000\u0096\r\u0001\u0000"+
		"\u0000\u0000\u0097\u0098\u0005\t\u0000\u0000\u0098\u009b\u0005\r\u0000"+
		"\u0000\u0099\u009a\u0005\u0007\u0000\u0000\u009a\u009c\u0005\r\u0000\u0000"+
		"\u009b\u0099\u0001\u0000\u0000\u0000\u009b\u009c\u0001\u0000\u0000\u0000"+
		"\u009c\u009d\u0001\u0000\u0000\u0000\u009d\u009e\u0005\n\u0000\u0000\u009e"+
		"\u000f\u0001\u0000\u0000\u0000\u009f\u00a0\u0005\u0007\u0000\u0000\u00a0"+
		"\u00a3\u0003@ \u0000\u00a1\u00a3\u0003@ \u0000\u00a2\u009f\u0001\u0000"+
		"\u0000\u0000\u00a2\u00a1\u0001\u0000\u0000\u0000\u00a3\u0011\u0001\u0000"+
		"\u0000\u0000\u00a4\u00a5\u0005\u000e\u0000\u0000\u00a5\u00a6\u0005\t\u0000"+
		"\u0000\u00a6\u00a7\u0005B\u0000\u0000\u00a7\u00a8\u0005\t\u0000\u0000"+
		"\u00a8\u00ab\u0003@ \u0000\u00a9\u00aa\u0005\u0007\u0000\u0000\u00aa\u00ac"+
		"\u0003@ \u0000\u00ab\u00a9\u0001\u0000\u0000\u0000\u00ab\u00ac\u0001\u0000"+
		"\u0000\u0000\u00ac\u00ad\u0001\u0000\u0000\u0000\u00ad\u00ae\u0005\n\u0000"+
		"\u0000\u00ae\u00af\u0005\n\u0000\u0000\u00af\u0013\u0001\u0000\u0000\u0000"+
		"\u00b0\u00b1\u0005\u000f\u0000\u0000\u00b1\u00b2\u0005\t\u0000\u0000\u00b2"+
		"\u00b3\u0005B\u0000\u0000\u00b3\u00b4\u0005\n\u0000\u0000\u00b4\u0015"+
		"\u0001\u0000\u0000\u0000\u00b5\u00b6\u0003>\u001f\u0000\u00b6\u00b7\u0005"+
		"\u000b\u0000\u0000\u00b7\u00bc\u0003\u0018\f\u0000\u00b8\u00b9\u0005\u0007"+
		"\u0000\u0000\u00b9\u00bb\u0003\u0018\f\u0000\u00ba\u00b8\u0001\u0000\u0000"+
		"\u0000\u00bb\u00be\u0001\u0000\u0000\u0000\u00bc\u00ba\u0001\u0000\u0000"+
		"\u0000\u00bc\u00bd\u0001\u0000\u0000\u0000\u00bd\u0017\u0001\u0000\u0000"+
		"\u0000\u00be\u00bc\u0001\u0000\u0000\u0000\u00bf\u00c0\u0005B\u0000\u0000"+
		"\u00c0\u00c1\u0005\u0010\u0000\u0000\u00c1\u00c4\u0003@ \u0000\u00c2\u00c4"+
		"\u0005B\u0000\u0000\u00c3\u00bf\u0001\u0000\u0000\u0000\u00c3\u00c2\u0001"+
		"\u0000\u0000\u0000\u00c4\u0019\u0001\u0000\u0000\u0000\u00c5\u00c6\u0005"+
		"B\u0000\u0000\u00c6\u00c7\u0005\u0010\u0000\u0000\u00c7\u00e5\u0003@ "+
		"\u0000\u00c8\u00c9\u0005B\u0000\u0000\u00c9\u00ca\u0005\u0010\u0000\u0000"+
		"\u00ca\u00cb\u0005\t\u0000\u0000\u00cb\u00cf\u0005\u0011\u0000\u0000\u00cc"+
		"\u00ce\u0003\u0010\b\u0000\u00cd\u00cc\u0001\u0000\u0000\u0000\u00ce\u00d1"+
		"\u0001\u0000\u0000\u0000\u00cf\u00cd\u0001\u0000\u0000\u0000\u00cf\u00d0"+
		"\u0001\u0000\u0000\u0000\u00d0\u00d2\u0001\u0000\u0000\u0000\u00d1\u00cf"+
		"\u0001\u0000\u0000\u0000\u00d2\u00d3\u0005\u0011\u0000\u0000\u00d3\u00e5"+
		"\u0005\n\u0000\u0000\u00d4\u00d5\u0005B\u0000\u0000\u00d5\u00d6\u0005"+
		"\u0012\u0000\u0000\u00d6\u00d7\u0003@ \u0000\u00d7\u00d8\u0005\u0013\u0000"+
		"\u0000\u00d8\u00d9\u0005\u0010\u0000\u0000\u00d9\u00da\u0003@ \u0000\u00da"+
		"\u00e5\u0001\u0000\u0000\u0000\u00db\u00dc\u0005B\u0000\u0000\u00dc\u00dd"+
		"\u0005\u0012\u0000\u0000\u00dd\u00de\u0003@ \u0000\u00de\u00df\u0005\u0007"+
		"\u0000\u0000\u00df\u00e0\u0003@ \u0000\u00e0\u00e1\u0005\u0013\u0000\u0000"+
		"\u00e1\u00e2\u0005\u0010\u0000\u0000\u00e2\u00e3\u0003@ \u0000\u00e3\u00e5"+
		"\u0001\u0000\u0000\u0000\u00e4\u00c5\u0001\u0000\u0000\u0000\u00e4\u00c8"+
		"\u0001\u0000\u0000\u0000\u00e4\u00d4\u0001\u0000\u0000\u0000\u00e4\u00db"+
		"\u0001\u0000\u0000\u0000\u00e5\u001b\u0001\u0000\u0000\u0000\u00e6\u00e7"+
		"\u0005\u0014\u0000\u0000\u00e7\u001d\u0001\u0000\u0000\u0000\u00e8\u00e9"+
		"\u0005\u0015\u0000\u0000\u00e9\u00ea\u0005\t\u0000\u0000\u00ea\u00eb\u0003"+
		"@ \u0000\u00eb\u00ec\u0005\n\u0000\u0000\u00ec\u00f0\u0005\u0016\u0000"+
		"\u0000\u00ed\u00ef\u0003\b\u0004\u0000\u00ee\u00ed\u0001\u0000\u0000\u0000"+
		"\u00ef\u00f2\u0001\u0000\u0000\u0000\u00f0\u00ee\u0001\u0000\u0000\u0000"+
		"\u00f0\u00f1\u0001\u0000\u0000\u0000\u00f1\u00f6\u0001\u0000\u0000\u0000"+
		"\u00f2\u00f0\u0001\u0000\u0000\u0000\u00f3\u00f5\u0003 \u0010\u0000\u00f4"+
		"\u00f3\u0001\u0000\u0000\u0000\u00f5\u00f8\u0001\u0000\u0000\u0000\u00f6"+
		"\u00f4\u0001\u0000\u0000\u0000\u00f6\u00f7\u0001\u0000\u0000\u0000\u00f7"+
		"\u00fa\u0001\u0000\u0000\u0000\u00f8\u00f6\u0001\u0000\u0000\u0000\u00f9"+
		"\u00fb\u0003\"\u0011\u0000\u00fa\u00f9\u0001\u0000\u0000\u0000\u00fa\u00fb"+
		"\u0001\u0000\u0000\u0000\u00fb\u00fc\u0001\u0000\u0000\u0000\u00fc\u00fd"+
		"\u0005\u0002\u0000\u0000\u00fd\u00fe\u0005\u0015\u0000\u0000\u00fe\u001f"+
		"\u0001\u0000\u0000\u0000\u00ff\u0100\u0005\u0017\u0000\u0000\u0100\u0101"+
		"\u0005\u0015\u0000\u0000\u0101\u0102\u0005\t\u0000\u0000\u0102\u0103\u0003"+
		"@ \u0000\u0103\u0104\u0005\n\u0000\u0000\u0104\u0108\u0005\u0016\u0000"+
		"\u0000\u0105\u0107\u0003\b\u0004\u0000\u0106\u0105\u0001\u0000\u0000\u0000"+
		"\u0107\u010a\u0001\u0000\u0000\u0000\u0108\u0106\u0001\u0000\u0000\u0000"+
		"\u0108\u0109\u0001\u0000\u0000\u0000\u0109!\u0001\u0000\u0000\u0000\u010a"+
		"\u0108\u0001\u0000\u0000\u0000\u010b\u010f\u0005\u0017\u0000\u0000\u010c"+
		"\u010e\u0003\b\u0004\u0000\u010d\u010c\u0001\u0000\u0000\u0000\u010e\u0111"+
		"\u0001\u0000\u0000\u0000\u010f\u010d\u0001\u0000\u0000\u0000\u010f\u0110"+
		"\u0001\u0000\u0000\u0000\u0110#\u0001\u0000\u0000\u0000\u0111\u010f\u0001"+
		"\u0000\u0000\u0000\u0112\u0113\u0005B\u0000\u0000\u0113\u0115\u0005\r"+
		"\u0000\u0000\u0114\u0112\u0001\u0000\u0000\u0000\u0114\u0115\u0001\u0000"+
		"\u0000\u0000\u0115\u0116\u0001\u0000\u0000\u0000\u0116\u0117\u0005\u0018"+
		"\u0000\u0000\u0117\u0118\u0003\u001a\r\u0000\u0118\u0119\u0005\u0007\u0000"+
		"\u0000\u0119\u011a\u0003@ \u0000\u011a\u011b\u0005\u0007\u0000\u0000\u011b"+
		"\u011f\u0003@ \u0000\u011c\u011e\u0003\b\u0004\u0000\u011d\u011c\u0001"+
		"\u0000\u0000\u0000\u011e\u0121\u0001\u0000\u0000\u0000\u011f\u011d\u0001"+
		"\u0000\u0000\u0000\u011f\u0120\u0001\u0000\u0000\u0000\u0120\u0122\u0001"+
		"\u0000\u0000\u0000\u0121\u011f\u0001\u0000\u0000\u0000\u0122\u0123\u0005"+
		"\u0002\u0000\u0000\u0123\u0125\u0005\u0018\u0000\u0000\u0124\u0126\u0005"+
		"B\u0000\u0000\u0125\u0124\u0001\u0000\u0000\u0000\u0125\u0126\u0001\u0000"+
		"\u0000\u0000\u0126%\u0001\u0000\u0000\u0000\u0127\u0128\u0005B\u0000\u0000"+
		"\u0128\u012a\u0005\r\u0000\u0000\u0129\u0127\u0001\u0000\u0000\u0000\u0129"+
		"\u012a\u0001\u0000\u0000\u0000\u012a\u012b\u0001\u0000\u0000\u0000\u012b"+
		"\u012c\u0005\u0018\u0000\u0000\u012c\u012d\u0005\u0019\u0000\u0000\u012d"+
		"\u012e\u0005\t\u0000\u0000\u012e\u012f\u0003@ \u0000\u012f\u0133\u0005"+
		"\n\u0000\u0000\u0130\u0132\u0003\b\u0004\u0000\u0131\u0130\u0001\u0000"+
		"\u0000\u0000\u0132\u0135\u0001\u0000\u0000\u0000\u0133\u0131\u0001\u0000"+
		"\u0000\u0000\u0133\u0134\u0001\u0000\u0000\u0000\u0134\u0136\u0001\u0000"+
		"\u0000\u0000\u0135\u0133\u0001\u0000\u0000\u0000\u0136\u0137\u0005\u0002"+
		"\u0000\u0000\u0137\u0139\u0005\u0018\u0000\u0000\u0138\u013a\u0005B\u0000"+
		"\u0000\u0139\u0138\u0001\u0000\u0000\u0000\u0139\u013a\u0001\u0000\u0000"+
		"\u0000\u013a\'\u0001\u0000\u0000\u0000\u013b\u013d\u0005\u001a\u0000\u0000"+
		"\u013c\u013e\u0005B\u0000\u0000\u013d\u013c\u0001\u0000\u0000\u0000\u013d"+
		"\u013e\u0001\u0000\u0000\u0000\u013e)\u0001\u0000\u0000\u0000\u013f\u0141"+
		"\u0005\u001b\u0000\u0000\u0140\u0142\u0005B\u0000\u0000\u0141\u0140\u0001"+
		"\u0000\u0000\u0000\u0141\u0142\u0001\u0000\u0000\u0000\u0142+\u0001\u0000"+
		"\u0000\u0000\u0143\u0144\u0005\u0007\u0000\u0000\u0144\u0147\u0005B\u0000"+
		"\u0000\u0145\u0147\u0005B\u0000\u0000\u0146\u0143\u0001\u0000\u0000\u0000"+
		"\u0146\u0145\u0001\u0000\u0000\u0000\u0147-\u0001\u0000\u0000\u0000\u0148"+
		"\u0149\u0005\u0007\u0000\u0000\u0149\u014a\u0005\u001c\u0000\u0000\u014a"+
		"\u014b\u0005\t\u0000\u0000\u014b\u014c\u0005\u001d\u0000\u0000\u014c\u014d"+
		"\u0005\n\u0000\u0000\u014d\u014e\u0005\u000b\u0000\u0000\u014e/\u0001"+
		"\u0000\u0000\u0000\u014f\u0150\u0003>\u001f\u0000\u0150\u0151\u0003.\u0017"+
		"\u0000\u0151\u0157\u0005B\u0000\u0000\u0152\u0154\u0005\t\u0000\u0000"+
		"\u0153\u0155\u0003,\u0016\u0000\u0154\u0153\u0001\u0000\u0000\u0000\u0154"+
		"\u0155\u0001\u0000\u0000\u0000\u0155\u0156\u0001\u0000\u0000\u0000\u0156"+
		"\u0158\u0005\n\u0000\u0000\u0157\u0152\u0001\u0000\u0000\u0000\u0157\u0158"+
		"\u0001\u0000\u0000\u0000\u01581\u0001\u0000\u0000\u0000\u0159\u015a\u0005"+
		"\u001e\u0000\u0000\u015a\u015b\u0005B\u0000\u0000\u015b\u015f\u0005\t"+
		"\u0000\u0000\u015c\u015e\u0003,\u0016\u0000\u015d\u015c\u0001\u0000\u0000"+
		"\u0000\u015e\u0161\u0001\u0000\u0000\u0000\u015f\u015d\u0001\u0000\u0000"+
		"\u0000\u015f\u0160\u0001\u0000\u0000\u0000\u0160\u0162\u0001\u0000\u0000"+
		"\u0000\u0161\u015f\u0001\u0000\u0000\u0000\u0162\u0163\u0005\n\u0000\u0000"+
		"\u0163\u0167\u0003\u0004\u0002\u0000\u0164\u0166\u00030\u0018\u0000\u0165"+
		"\u0164\u0001\u0000\u0000\u0000\u0166\u0169\u0001\u0000\u0000\u0000\u0167"+
		"\u0165\u0001\u0000\u0000\u0000\u0167\u0168\u0001\u0000\u0000\u0000\u0168"+
		"\u016d\u0001\u0000\u0000\u0000\u0169\u0167\u0001\u0000\u0000\u0000\u016a"+
		"\u016c\u0003\b\u0004\u0000\u016b\u016a\u0001\u0000\u0000\u0000\u016c\u016f"+
		"\u0001\u0000\u0000\u0000\u016d\u016b\u0001\u0000\u0000\u0000\u016d\u016e"+
		"\u0001\u0000\u0000\u0000\u016e\u0170\u0001\u0000\u0000\u0000\u016f\u016d"+
		"\u0001\u0000\u0000\u0000\u0170\u0171\u0005\u0002\u0000\u0000\u0171\u0172"+
		"\u0005\u001e\u0000\u0000\u0172\u0173\u0005B\u0000\u0000\u01733\u0001\u0000"+
		"\u0000\u0000\u0174\u0175\u0005\u001f\u0000\u0000\u0175\u0176\u0005B\u0000"+
		"\u0000\u0176\u017a\u0005\t\u0000\u0000\u0177\u0179\u0003\u0010\b\u0000"+
		"\u0178\u0177\u0001\u0000\u0000\u0000\u0179\u017c\u0001\u0000\u0000\u0000"+
		"\u017a\u0178\u0001\u0000\u0000\u0000\u017a\u017b\u0001\u0000\u0000\u0000"+
		"\u017b\u017d\u0001\u0000\u0000\u0000\u017c\u017a\u0001\u0000\u0000\u0000"+
		"\u017d\u017e\u0005\n\u0000\u0000\u017e5\u0001\u0000\u0000\u0000\u017f"+
		"\u0180\u0005 \u0000\u0000\u0180\u0181\u0005B\u0000\u0000\u0181\u0185\u0005"+
		"\t\u0000\u0000\u0182\u0184\u0003,\u0016\u0000\u0183\u0182\u0001\u0000"+
		"\u0000\u0000\u0184\u0187\u0001\u0000\u0000\u0000\u0185\u0183\u0001\u0000"+
		"\u0000\u0000\u0185\u0186\u0001\u0000\u0000\u0000\u0186\u0188\u0001\u0000"+
		"\u0000\u0000\u0187\u0185\u0001\u0000\u0000\u0000\u0188\u0189\u0005\n\u0000"+
		"\u0000\u0189\u018a\u0005!\u0000\u0000\u018a\u018c\u0005\t\u0000\u0000"+
		"\u018b\u018d\u0005B\u0000\u0000\u018c\u018b\u0001\u0000\u0000\u0000\u018c"+
		"\u018d\u0001\u0000\u0000\u0000\u018d\u018e\u0001\u0000\u0000\u0000\u018e"+
		"\u018f\u0005\n\u0000\u0000\u018f\u0193\u0003\u0004\u0002\u0000\u0190\u0192"+
		"\u00030\u0018\u0000\u0191\u0190\u0001\u0000\u0000\u0000\u0192\u0195\u0001"+
		"\u0000\u0000\u0000\u0193\u0191\u0001\u0000\u0000\u0000\u0193\u0194\u0001"+
		"\u0000\u0000\u0000\u0194\u0199\u0001\u0000\u0000\u0000\u0195\u0193\u0001"+
		"\u0000\u0000\u0000\u0196\u0198\u0003\b\u0004\u0000\u0197\u0196\u0001\u0000"+
		"\u0000\u0000\u0198\u019b\u0001\u0000\u0000\u0000\u0199\u0197\u0001\u0000"+
		"\u0000\u0000\u0199\u019a\u0001\u0000\u0000\u0000\u019a\u019c\u0001\u0000"+
		"\u0000\u0000\u019b\u0199\u0001\u0000\u0000\u0000\u019c\u019d\u0005\u0002"+
		"\u0000\u0000\u019d\u019e\u0005 \u0000\u0000\u019e\u019f\u0005B\u0000\u0000"+
		"\u019f7\u0001\u0000\u0000\u0000\u01a0\u01a1\u0005B\u0000\u0000\u01a1\u01a5"+
		"\u0005\t\u0000\u0000\u01a2\u01a4\u0003\u0010\b\u0000\u01a3\u01a2\u0001"+
		"\u0000\u0000\u0000\u01a4\u01a7\u0001\u0000\u0000\u0000\u01a5\u01a3\u0001"+
		"\u0000\u0000\u0000\u01a5\u01a6\u0001\u0000\u0000\u0000\u01a6\u01a8\u0001"+
		"\u0000\u0000\u0000\u01a7\u01a5\u0001\u0000\u0000\u0000\u01a8\u01a9\u0005"+
		"\n\u0000\u0000\u01a99\u0001\u0000\u0000\u0000\u01aa\u01ab\u0005\"\u0000"+
		"\u0000\u01ab\u01ac\u0005#\u0000\u0000\u01ac\u01ad\u0005\u0007\u0000\u0000"+
		"\u01ad\u01b2\u0003<\u001e\u0000\u01ae\u01af\u0005\u0007\u0000\u0000\u01af"+
		"\u01b1\u0003<\u001e\u0000\u01b0\u01ae\u0001\u0000\u0000\u0000\u01b1\u01b4"+
		"\u0001\u0000\u0000\u0000\u01b2\u01b0\u0001\u0000\u0000\u0000\u01b2\u01b3"+
		"\u0001\u0000\u0000\u0000\u01b3;\u0001\u0000\u0000\u0000\u01b4\u01b2\u0001"+
		"\u0000\u0000\u0000\u01b5\u01b6\u0003@ \u0000\u01b6=\u0001\u0000\u0000"+
		"\u0000\u01b7\u01b8\u0007\u0000\u0000\u0000\u01b8?\u0001\u0000\u0000\u0000"+
		"\u01b9\u01ba\u0006 \uffff\uffff\u0000\u01ba\u01bb\u0005\t\u0000\u0000"+
		"\u01bb\u01bc\u0003@ \u0000\u01bc\u01bd\u0005\n\u0000\u0000\u01bd\u01dd"+
		"\u0001\u0000\u0000\u0000\u01be\u01dd\u0005@\u0000\u0000\u01bf\u01dd\u0005"+
		"C\u0000\u0000\u01c0\u01dd\u0005A\u0000\u0000\u01c1\u01dd\u0005B\u0000"+
		"\u0000\u01c2\u01dd\u0005D\u0000\u0000\u01c3\u01c4\u0005+\u0000\u0000\u01c4"+
		"\u01dd\u0003@ \u000e\u01c5\u01c6\u0005,\u0000\u0000\u01c6\u01c7\u0005"+
		"-\u0000\u0000\u01c7\u01c8\u0005,\u0000\u0000\u01c8\u01dd\u0003@ \r\u01c9"+
		"\u01ca\u0005,\u0000\u0000\u01ca\u01cb\u0005.\u0000\u0000\u01cb\u01dd\u0005"+
		",\u0000\u0000\u01cc\u01cd\u0005,\u0000\u0000\u01cd\u01ce\u0005/\u0000"+
		"\u0000\u01ce\u01dd\u0005,\u0000\u0000\u01cf\u01d0\u0005B\u0000\u0000\u01d0"+
		"\u01d4\u0005\t\u0000\u0000\u01d1\u01d3\u0003\u0010\b\u0000\u01d2\u01d1"+
		"\u0001\u0000\u0000\u0000\u01d3\u01d6\u0001\u0000\u0000\u0000\u01d4\u01d2"+
		"\u0001\u0000\u0000\u0000\u01d4\u01d5\u0001\u0000\u0000\u0000\u01d5\u01d7"+
		"\u0001\u0000\u0000\u0000\u01d6\u01d4\u0001\u0000\u0000\u0000\u01d7\u01dd"+
		"\u0005\n\u0000\u0000\u01d8\u01d9\u0005>\u0000\u0000\u01d9\u01da\u0005"+
		"\t\u0000\u0000\u01da\u01db\u0005B\u0000\u0000\u01db\u01dd\u0005\n\u0000"+
		"\u0000\u01dc\u01b9\u0001\u0000\u0000\u0000\u01dc\u01be\u0001\u0000\u0000"+
		"\u0000\u01dc\u01bf\u0001\u0000\u0000\u0000\u01dc\u01c0\u0001\u0000\u0000"+
		"\u0000\u01dc\u01c1\u0001\u0000\u0000\u0000\u01dc\u01c2\u0001\u0000\u0000"+
		"\u0000\u01dc\u01c3\u0001\u0000\u0000\u0000\u01dc\u01c5\u0001\u0000\u0000"+
		"\u0000\u01dc\u01c9\u0001\u0000\u0000\u0000\u01dc\u01cc\u0001\u0000\u0000"+
		"\u0000\u01dc\u01cf\u0001\u0000\u0000\u0000\u01dc\u01d8\u0001\u0000\u0000"+
		"\u0000\u01dd\u0201\u0001\u0000\u0000\u0000\u01de\u01df\n\u0017\u0000\u0000"+
		"\u01df\u01e0\u0005)\u0000\u0000\u01e0\u0200\u0003@ \u0018\u01e1\u01e2"+
		"\n\u0016\u0000\u0000\u01e2\u01e3\u0007\u0001\u0000\u0000\u01e3\u0200\u0003"+
		"@ \u0017\u01e4\u01e5\n\u0015\u0000\u0000\u01e5\u01e6\u0007\u0002\u0000"+
		"\u0000\u01e6\u0200\u0003@ \u0016\u01e7\u01e8\n\n\u0000\u0000\u01e8\u01e9"+
		"\u0007\u0003\u0000\u0000\u01e9\u0200\u0003@ \u000b\u01ea\u01eb\n\t\u0000"+
		"\u0000\u01eb\u01ec\u0007\u0004\u0000\u0000\u01ec\u0200\u0003@ \n\u01ed"+
		"\u01ee\n\b\u0000\u0000\u01ee\u01ef\u0007\u0005\u0000\u0000\u01ef\u0200"+
		"\u0003@ \t\u01f0\u01f1\n\u0007\u0000\u0000\u01f1\u01f2\u0007\u0006\u0000"+
		"\u0000\u01f2\u0200\u0003@ \b\u01f3\u01f4\n\u0006\u0000\u0000\u01f4\u01f5"+
		"\u0007\u0007\u0000\u0000\u01f5\u0200\u0003@ \u0007\u01f6\u01f7\n\u0005"+
		"\u0000\u0000\u01f7\u01f8\u0007\b\u0000\u0000\u01f8\u0200\u0003@ \u0006"+
		"\u01f9\u01fa\n\u0004\u0000\u0000\u01fa\u01fb\u0005<\u0000\u0000\u01fb"+
		"\u0200\u0003@ \u0005\u01fc\u01fd\n\u0003\u0000\u0000\u01fd\u01fe\u0005"+
		"=\u0000\u0000\u01fe\u0200\u0003@ \u0004\u01ff\u01de\u0001\u0000\u0000"+
		"\u0000\u01ff\u01e1\u0001\u0000\u0000\u0000\u01ff\u01e4\u0001\u0000\u0000"+
		"\u0000\u01ff\u01e7\u0001\u0000\u0000\u0000\u01ff\u01ea\u0001\u0000\u0000"+
		"\u0000\u01ff\u01ed\u0001\u0000\u0000\u0000\u01ff\u01f0\u0001\u0000\u0000"+
		"\u0000\u01ff\u01f3\u0001\u0000\u0000\u0000\u01ff\u01f6\u0001\u0000\u0000"+
		"\u0000\u01ff\u01f9\u0001\u0000\u0000\u0000\u01ff\u01fc\u0001\u0000\u0000"+
		"\u0000\u0200\u0203\u0001\u0000\u0000\u0000\u0201\u01ff\u0001\u0000\u0000"+
		"\u0000\u0201\u0202\u0001\u0000\u0000\u0000\u0202A\u0001\u0000\u0000\u0000"+
		"\u0203\u0201\u0001\u0000\u0000\u0000-DFR_p}\u008a\u0095\u009b\u00a2\u00ab"+
		"\u00bc\u00c3\u00cf\u00e4\u00f0\u00f6\u00fa\u0108\u010f\u0114\u011f\u0125"+
		"\u0129\u0133\u0139\u013d\u0141\u0146\u0154\u0157\u015f\u0167\u016d\u017a"+
		"\u0185\u018c\u0193\u0199\u01a5\u01b2\u01d4\u01dc\u01ff\u0201";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}