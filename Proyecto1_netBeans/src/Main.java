/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author pilloubool
 */
import Entorno.Entorno;
import Gramatica.GramaticaLexer;
import Gramatica.GramaticaParser;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import static org.antlr.v4.runtime.CharStreams.fromFileName;
import static org.antlr.v4.runtime.CharStreams.fromString;

public class Main {

    public static String LinkedList_to_String(LinkedList<String> list) {
        String retorno = "";
        for (int i = 0; i < list.size(); i++) {
            retorno += list.get(i) + "\n";
        }
        return retorno;
    }

    public static void Principal() throws IOException {
        //--------------------------------

        String input = "int var1 = 54+17*78;";
        String path = "./src/Micelaneos/input.txt";

        CharStream cs = fromString(input);
        CharStream cs_file = fromFileName(path);

        GramaticaLexer lexico = new GramaticaLexer(cs_file);
        CommonTokenStream tokens = new CommonTokenStream(lexico);
        GramaticaParser sintactico = new GramaticaParser(tokens);
        //GramaticaParser.StartContext startCtx = sintactico.start(); //produccion inicial
        ParseTree tree = sintactico.start();    //arbol desde la raiz

        MyVisitor visitor = new MyVisitor(new Entorno(null),false);
        // visitor.visit(startCtx);
        Object retorno = visitor.visit(tree);

//crear grafico del arbol
        List<String> rulesName = Arrays.asList(sintactico.getRuleNames());
        TreeViewer viewr = new TreeViewer(rulesName, tree);
        //viewr.open();

        String s = LinkedList_to_String((LinkedList<String>) retorno);
        System.out.println(s);

    }

    public static void main(String[] args) throws IOException {
        //-----------------------compilar antlr v4---------
        try {
            // antlr4 -Dlanguage=Java -o Gramatica -visitor -no-listener Gramatica.g4

            //String[] cmd = {"antlr4","-o","ParSer","-package","ParSer","-Dlanguage=Java","-listener","-visitor","-lib","./src/Gramatica.g4"}; 
            //  String[] cmd = {"antlr4", "-Dlanguage=Java", "-o", "./src/PARSER", "-visitor", "-no-listener", "./src/Gramatica.g4"}; 
//            Runtime.getRuntime().exec(cmd);
            System.out.println("<Main>");
            Principal();
        } catch (IOException ioe) {
            System.out.println(ioe);
        }

    }
}
