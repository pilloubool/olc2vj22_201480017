/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Instrucciones;

/**
 *
 * @author pilloubool
 */
public class Declaracion {

    String id;
    Object valor;
    String tipo;

    public Declaracion(String id, Object valor) {
        this.id = id;
        this.valor = valor;
        this.tipo = "";
    }

    public Declaracion(String id, Object valor, String tipo) {
        this.id = id;
        this.valor = valor;
        this.tipo = tipo;
    }

//---------------------------------------------
    public void agregar_valor_predeterminado() {
        switch (this.tipo) {
            case "integer":
                this.valor = "0";
                break;

            case "real":
                this.valor = "0.00000000";
                break;

            case "complex":
                this.valor = " (9.192517926E-43,0.00000000)";
                break;

            case "character":
                this.valor = "";
                break;

            case "logical":
                this.valor = "false";
                break;
        }
    }//metodo

    //---------------------------------------------
    public Boolean comprobacion_de_tipos() {
        switch (this.tipo) {
            case "integer":
                if (this.valor instanceof Integer) {
                    return true;
                }
                break;
            case "real":
                if (this.valor instanceof Double) {
                    return true;
                }
                break;
            case "character":
                if (this.valor instanceof Character) {
                    return true;
                }
                break;
            case "logical":
                if (this.valor instanceof Boolean) {
                    return true;
                }
                break;
            case "function":
                return true;
        }
        return false;
    }//metodo
//---------------------------------------------

    public String getTipo_valor() {
        if (valor instanceof Integer) {
            return "Integer";
        }
        if (valor instanceof Double) {
            return "Real";
        }
        if (valor instanceof Boolean) {
            return "Logical";
        }
        if (valor instanceof Character) {
            return "Character";
        }
        return "";
    }
    
        public void setTipo_valor() {
        if (valor instanceof Integer) {
            tipo= "Integer";
        }
        if (valor instanceof Double) {
            tipo=  "Real";
        }
        if (valor instanceof Boolean) {
            tipo=  "Logical";
        }
        if (valor instanceof Character) {
            tipo=  "Character";
        }
    }
//---------------------------------------------

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

}
