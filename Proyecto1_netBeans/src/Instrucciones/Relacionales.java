/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Instrucciones;

import java.util.LinkedList;

/**
 *
 * @author pilloubool
 */
public class Relacionales {

    Object izq;
    Object der;
    String relacion;

    public Relacionales(Object izq, Object der, String relacion) {
        this.izq = izq;
        this.der = der;
        this.relacion = relacion.replace("'", "");
    }

    public Object validar(LinkedList<Object> msj, int fila, int columna) {
        if (((relacion.equals("==")) || (relacion.equals(".eq."))) || ((relacion.equals("/=")) || (relacion.equals(".ne.")))) {
            Boolean tipo_correcto = false;
            if ((izq instanceof Integer) && (der instanceof Integer)) {
                tipo_correcto = true;
            } else if ((izq instanceof Double) && (der instanceof Double)) {
                tipo_correcto = true;
            } else if ((izq instanceof Boolean) && (der instanceof Boolean)) {
                tipo_correcto = true;
            }
            if (tipo_correcto) {
                if ((relacion.equals("==")) || (relacion.equals(".eq."))) {
                    return (izq.equals(der));
                }
                if ((relacion.equals("/=")) || (relacion.equals(".ne."))) {
                    return (izq != der);
                }
            }
        }//if

        //int y double
        if (((relacion.equals(">")) || (relacion.equals(".gt."))) || ((relacion.equals("<")) || (relacion.equals(".lt."))) || ((relacion.equals(">=")) || (relacion.equals(".ge."))) || ((relacion.equals("<=")) || (relacion.equals(".le.")))) {
            if ((izq instanceof Integer) && (der instanceof Integer)) {
                int auxIzq = (Integer) izq;
                int auxDer = (Integer) der;

                if ((relacion.equals(">")) || (relacion.equals(".gt."))) {
                    return (auxIzq > auxDer);
                } else if (((relacion.equals("<")) || (relacion.equals(".lt.")))) {
                    return (auxIzq < auxDer);
                } else if (((relacion.equals(">=")) || (relacion.equals(".ge.")))) {
                    return (auxIzq >= auxDer);
                } else if (((relacion.equals("<=")) || (relacion.equals(".le.")))) {
                    return (auxIzq <= auxDer);
                }
            }
            if ((izq instanceof Double) && (der instanceof Double)) {
                double auxIzq = (Double) izq;
                double auxDer = (Double) der;

                if ((relacion.equals(">")) || (relacion.equals(".gt."))) {
                    return (auxIzq > auxDer);
                } else if (((relacion.equals("<")) || (relacion.equals(".lt.")))) {
                    return (auxIzq < auxDer);
                } else if (((relacion.equals(">=")) || (relacion.equals(".ge.")))) {
                    return (auxIzq >= auxDer);
                } else if (((relacion.equals("<=")) || (relacion.equals(".le.")))) {
                    return (auxIzq <= auxDer);
                }
            }
        }

        if ((relacion.equals(".and.")) || (relacion.equals(".or."))) {
            if ((izq instanceof Boolean) && (der instanceof Boolean)) {
                if (relacion.equals(".and.")) {
                    return ((Boolean) izq && (Boolean) der);
                }
                return ((Boolean) izq || (Boolean) der);
            }
            msj.add(" <Error ," + "Relacional: \"" + izq.toString() + " " + this.relacion + " " + der.toString() + "\" > [fila: " + fila + ",col: " + columna + "] {Error de tipos, debe ser (bool y bool) }");
            return new Error();
        }

        msj.add(" <Error ," + "Relacional: \"" + izq.toString() + " " + this.relacion + " " + der.toString() + "\" > [fila: " + fila + ",col: " + columna + "] { Se intenta validar tipos incorrectos }");
        return new Error();
    }//validar

}//fin
