/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Instrucciones;

import Entorno.Entorno;
import Gramatica.GramaticaParser;
import java.util.List;

/**
 *
 * @author pilloubool
 */
public class Funciones {

    Entorno entSub;
    List<GramaticaParser.List_paramContext> list_parm;
    List<GramaticaParser.Declaracion_parametro_funcContext> declaracion_param;
    List<GramaticaParser.InstruccionesContext> instrucciones;
    List<String> list_nom_param;
    String retorno;

    public Funciones(Entorno entSub, List<GramaticaParser.List_paramContext> list_parm, List<GramaticaParser.Declaracion_parametro_funcContext> declaracion_param, List<GramaticaParser.InstruccionesContext> instrucciones, List<String> list_nom_param,String retorno) {
        this.entSub = entSub;
        this.list_parm = list_parm;
        this.declaracion_param = declaracion_param;
        this.instrucciones = instrucciones;
        this.list_nom_param = list_nom_param;
        this.retorno = retorno;
    }
//------------------------------------------------------------------------------

    public Funciones() {
    }

    public Entorno getEntSub() {
        return entSub;
    }

    public void setEntSub(Entorno entSub) {
        this.entSub = entSub;
    }

    public List<GramaticaParser.List_paramContext> getList_parm() {
        return list_parm;
    }

    public void setList_parm(List<GramaticaParser.List_paramContext> list_parm) {
        this.list_parm = list_parm;
    }

    public List<GramaticaParser.Declaracion_parametro_funcContext> getDeclaracion_param() {
        return declaracion_param;
    }

    public void setDeclaracion_param(List<GramaticaParser.Declaracion_parametro_funcContext> declaracion_param) {
        this.declaracion_param = declaracion_param;
    }

    public List<GramaticaParser.InstruccionesContext> getInstrucciones() {
        return instrucciones;
    }

    public void setInstrucciones(List<GramaticaParser.InstruccionesContext> instrucciones) {
        this.instrucciones = instrucciones;
    }

    public List<String> getList_nom_param() {
        return list_nom_param;
    }

    public void setList_nom_param(List<String> list_nom_param) {
        this.list_nom_param = list_nom_param;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

}
