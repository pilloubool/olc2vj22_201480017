/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Instrucciones;

import java.util.ArrayList;

/**
 *
 * @author pilloubool
 */
public class Arreglo {

    String tipo;
    int cant_dimenciones;

    int size_unaDim;
    int size_dosDim;
    boolean isDinamic;
    ArrayList<Object> L_parametros;

    public ArrayList<Object> L_una_dimension;
    ArrayList<ArrayList<Object>> L_dos_dimensiones;

    public Arreglo(String tipo) {
        this.tipo = tipo;
        this.cant_dimenciones = -1;
        this.L_dos_dimensiones = new ArrayList<>();
        this.L_una_dimension = new ArrayList<>();
        this.L_parametros = new ArrayList<>();
        this.size_dosDim = -1;
        this.size_unaDim = -1;
        this.isDinamic = false;
    }

    public Arreglo(String tipo, boolean isDinamic_, int cant_dim) {
        this.tipo = tipo;
        this.isDinamic = isDinamic_;
        this.cant_dimenciones = cant_dim;
        this.L_dos_dimensiones = new ArrayList<>();
        this.L_una_dimension = new ArrayList<>();
        this.L_parametros = new ArrayList<>();
        this.size_dosDim = -1;
        this.size_unaDim = -1;
    }

    public int getSize() {
        return (cant_dimenciones == 1) ? size_unaDim : (size_dosDim * size_unaDim);
    }

    public Boolean comprobacion_de_tipos(Object valor) {
        switch (this.tipo) {
            case "integer":
                if (valor instanceof Integer) {
                    return true;
                }
                break;
            case "real":
                if (valor instanceof Double) {
                    return true;
                }
                break;
            case "character":
                if (valor instanceof Character) {
                    return true;
                }
                break;
            case "logical":
                if (valor instanceof Boolean) {
                    return true;
                }
                break;
        }
        return false;
    }//metodo

//-********************************************************************Arreglos NO dinamicos-********************************************************************
    public void addParametro(Object obj) {
        this.L_parametros.add(obj);
        this.cant_dimenciones = this.L_parametros.size();
    }
    //---------------------------------------------

    public Boolean add_dato_unaDim(Object obj) {
        for (int i = 0; i < this.L_una_dimension.size(); i++) {
            if (this.L_una_dimension.get(i) == null) {
                this.L_una_dimension.set(i, obj);
                return true;
            }
        }
        return false;
    }
//---------------------------------------------

    public void add_dato_dos_dim(int posIzq, int posDer, Object objDato) {
        posIzq--;
        posDer--;

        ArrayList<Object> arr = this.L_dos_dimensiones.get(posIzq);
        arr.set(posDer, objDato);
        this.L_dos_dimensiones.set(posIzq, arr);
    }

    public Object get_dato_dos_dim(int i, int j) {
        ArrayList<Object> arr = this.L_dos_dimensiones.get(i);
        return arr.get(j);
    }

    public Object get_dato_una_dim(int i) {
        return this.L_una_dimension.get(i);
    }

//---------------------- Print -----------------------
    public String print() {
        if (this.cant_dimenciones == 1) {
            return print_una_Dim();
        } else if (this.cant_dimenciones >= 2) {
            return print_dos_Dim();
        }
        return "";
    }

    private String print_una_Dim() {
        String str = "";
//        for (Object dato : this.L_una_dimension) {
//            str += dato + " , ";
//        }
        int tam = this.L_una_dimension.size();
        for (int i = 0; i < tam; i++) {
            Object obj = this.L_una_dimension.get(i);
            str += obj + ((i != (tam - 1)) ? " , " : "");
        }
        return str;
    }

    private String print_dos_Dim() {
        String retorno = "\n";
        for (int i = 0; i < size_unaDim; i++) {
            ArrayList<Object> arr = L_dos_dimensiones.get(i);
            for (int j = 0; j < size_dosDim; j++) {
                retorno += arr.get(j) + ((i != (size_dosDim - 1)) ? " , " : "");
            }
            retorno += "\n";
        }

        return retorno;
    }
//---------------------------------------------

    public void Establecer_limites_de_Arreglo() {
        if (this.cant_dimenciones == 1) {
            this.size_unaDim = (Integer) this.L_parametros.get(0);
            for (int i = 0; i < this.size_unaDim; i++) {
                Object o = null;
                this.L_una_dimension.add(o);
            }
        } else if (this.cant_dimenciones == 2) {
            this.size_unaDim = (Integer) this.L_parametros.get(0);
            this.size_dosDim = (Integer) this.L_parametros.get(1);

            for (int i = 0; i < this.size_unaDim; i++) {
                ArrayList<Object> nueva = new ArrayList<>();
                for (int j = 0; j < this.size_dosDim; j++) {
                    Object o = null;
                    nueva.add(o);
                }
                this.L_dos_dimensiones.add(nueva);
            }
        }
    }
//---------------------------------------------

    public void Establecer_limites_de_Arreglo_dinamico(Object objUno, Object objDos) {
        int x = (Integer) objUno;
        int y = -1;
        if (objDos instanceof Integer) {
            y = (Integer) objDos;
        }

        if (this.cant_dimenciones == 1) {
            this.size_unaDim = x;

            /*
            for (int i = 0; i < this.size_unaDim; i++) {
                Object o = null;
                this.L_una_dimension.add(o);
            }
             */
        } else if (this.cant_dimenciones == 2) {
            this.size_unaDim = x;
            this.size_dosDim = y;

            for (int i = 0; i < this.size_unaDim; i++) {
                ArrayList<Object> una = new ArrayList<>();
                for (int j = 0; j < this.size_dosDim; j++) {
                    Object o = null;
                    una.add(o);
                }
                this.L_dos_dimensiones.add(una);
            }
        }

    }
//---------------------------------------------
//-********************************************************************Arreglos dinamicos-********************************************************************

    public boolean deallocate() {
        if (this.size_unaDim == -1 && this.size_dosDim == -1) {
            return false;
        }

        this.L_una_dimension.clear();
        this.L_dos_dimensiones.clear();
        this.size_unaDim = -1;
        this.size_dosDim = -1;
        return true;
    }
//---------------------------------------------
//---------------------------------------------
//---------------------------------------------
//---------------------------------------------

    public boolean tieneDimension() {
        if ((this.size_unaDim != -1) || (this.size_dosDim != -1)) {
            return true;
        }
        return false;
    }
//---------------------------------------------

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCant_dimenciones() {
        return cant_dimenciones;
    }

    public void setCant_dimenciones(int cant_dimenciones) {
        this.cant_dimenciones = cant_dimenciones;
    }

    public ArrayList<Object> getL_parametros() {
        return L_parametros;
    }

    public void setL_parametros(ArrayList<Object> L_parametros) {
        this.L_parametros = L_parametros;
    }

    public ArrayList<Object> getL_una_dimension() {
        return L_una_dimension;
    }

    public void setL_una_dimension(ArrayList<Object> L_una_dimension) {
        this.L_una_dimension = L_una_dimension;
    }

    public ArrayList<ArrayList<Object>> getL_dos_dimensiones() {
        return L_dos_dimensiones;
    }

    public void setL_dos_dimensiones(ArrayList<ArrayList<Object>> L_dos_dimensiones) {
        this.L_dos_dimensiones = L_dos_dimensiones;
    }

    public int getSize_unaDim() {
        return size_unaDim;
    }

    public int getSize_dosDim() {
        return size_dosDim;
    }

}
