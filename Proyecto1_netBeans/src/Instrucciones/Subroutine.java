/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Instrucciones;

import Entorno.Entorno;
import Gramatica.GramaticaParser.Declaracion_parametro_funcContext;
import Gramatica.GramaticaParser.InstruccionesContext;
import Gramatica.GramaticaParser.List_paramContext;
import java.util.List;

/**
 *
 * @author pilloubool
 */
public class Subroutine {

    Entorno entSub;
    List<List_paramContext> list_parm;
    List<Declaracion_parametro_funcContext> declaracion_param;
    List<InstruccionesContext> instrucciones;
    List<String> list_nom_param;

    public Subroutine(Entorno entSub, List<List_paramContext> list_parm, List<Declaracion_parametro_funcContext> declaracion_param, List<InstruccionesContext> instrucciones, List<String> lista_nom_param) {
        this.entSub = entSub;
        this.list_parm = list_parm;
        this.declaracion_param = declaracion_param;
        this.instrucciones = instrucciones;
        this.list_nom_param = lista_nom_param;
    }
//------------------------------------------------------------------------------------------------------

    public List<List_paramContext> getList_parm() {
        return list_parm;
    }

    public void setList_parm(List<List_paramContext> list_parm) {
        this.list_parm = list_parm;
    }

    public List<Declaracion_parametro_funcContext> getDeclaracion_param() {
        return declaracion_param;
    }

    public void setDeclaracion_param(List<Declaracion_parametro_funcContext> declaracion_param) {
        this.declaracion_param = declaracion_param;
    }

    public List<InstruccionesContext> getInstrucciones() {
        return instrucciones;
    }

    public void setInstrucciones(List<InstruccionesContext> instrucciones) {
        this.instrucciones = instrucciones;
    }

    public Entorno getEntSub() {
        return entSub;
    }

    public void setEntSub(Entorno entSub) {
        this.entSub = entSub;
    }

    public List<String> getList_nom_param() {
        return list_nom_param;
    }

    public void setList_nom_param(List<String> list_nom_param) {
        this.list_nom_param = list_nom_param;
    }

}
