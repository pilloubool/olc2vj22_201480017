/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Reportes;

import java.util.ArrayList;

/**
 *
 * @author pilloubool
 */
public class Nodo_Ast {

    Object valor;
    Object tipo;//indica si un nodo es terminal o no terminal 
    //si es terminal tipo = tipoValido
    //si es noTerminal tipo = null
    ArrayList hijos;
    Nodo_Ast izq;
    Nodo_Ast der;
    int id;
    public static int correlativo = 1;

    public Nodo_Ast(Object valor, Object tipo) {
        this.valor = valor;
        this.tipo = tipo;
        this.hijos = new ArrayList();
        this.izq = null;
        this.der = null;
        this.id = correlativo++;
    }

    public Nodo_Ast(Object valor) {
        this.valor = valor;
        this.tipo = "";
        this.hijos = new ArrayList();
        this.izq = null;
        this.der = null;
        this.id = correlativo++;
    }

    public String printRecursivo() {
        String str;
        if ((izq == null) && (der == null)) {
            System.out.println("der: " + valor);
            str = valor.toString();
        }

        if (izq != null) {

        }

//        
//        if (raiz.getIzq() != null) {
//            return printRecursivo(raiz.getIzq());
//        }
//
//        if (raiz.getDer() != null) {
//            return printRecursivo(raiz.getDer());
//        }
//        System.out.println("der: " + raiz.getValor());
        return "";
    }

    public String getAstGraphviz() {
        String retorno = ""
                + "digraph grafica{\n"
                + "rankdir=TB;\n"
                + "node [shape = record, style=filled, fillcolor=seashell2];\n"
                + getAstString()
                + "\n}";
        return retorno;
    }

    private String getAstString() {
        String etiqueta;
        if (izq == null && der == null) {
            etiqueta = "nodo" + id + " [ label =\"" + valor + "\"];\n";
        } else {
            etiqueta = "nodo" + id + " [ label =\"<C0>|" + valor + "|<C1>\"];\n";
        }
        if (izq != null) {
            etiqueta = etiqueta + izq.getAstString()
                    + "nodo" + id + ":C0->nodo" + izq.id + "\n";
        }
        if (der != null) {
            etiqueta = etiqueta + der.getAstString()
                    + "nodo" + id + ":C1->nodo" + der.id + "\n";
        }
        return etiqueta;
    }

    //----------------------------------------------
    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

    public Object getTipo() {
        return tipo;
    }

    public void setTipo(Object tipo) {
        this.tipo = tipo;
    }

    public ArrayList getHijos() {
        return hijos;
    }

    public void setHijos(ArrayList hijos) {
        this.hijos = hijos;
    }

    public void addHijo(Object hijo) {
        this.hijos.add(hijo);
    }

    public Nodo_Ast getIzq() {
        return izq;
    }

    public void setIzq(Nodo_Ast izq) {
        this.izq = izq;
    }

    public Nodo_Ast getDer() {
        return der;
    }

    public void setDer(Nodo_Ast der) {
        this.der = der;
    }

}
