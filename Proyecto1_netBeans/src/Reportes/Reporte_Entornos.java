/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Reportes;

import Entorno.Entorno;
import Entorno.Simbolo;
import Instrucciones.Arreglo;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;

/**
 *
 * @author pilloubool
 */
public class Reporte_Entornos {

    String data;
    String path;
    String extension;

    Stack<Entorno> pila_Ent = new Stack<>();

    public Reporte_Entornos(Stack<Entorno> pila_Ent_) {
        this.pila_Ent = pila_Ent_;
        data = "";
        path = "./src/Reportes/Entornos";
        extension = "pdf";
        crearReporte_Ent();
        Compilar_dot();
    }

    public void crearReporte_Ent() {
        int cont = 0;
        String bgColor = "bgcolor=\"#E9967A\"";
        data += "digraph G {\n"
                + "node [fontname=\"Helvetica,Arial,sans-serif\"]\n"
                + "node [shape=square fillcolor=\"#8FBC8F\" style=\"radial\" gradientangle=180]\n";
        for (Entorno ent = this.pila_Ent.peek(); ent != null; ent = ent.getPadre()) {
            HashMap tablaSim = ent.getTablaSimbolo();
            data += "ent" + cont + "[label=<\n";
            data += "<TABLE>\n ";
            data += "<tr>    "//filas
                    + " <td " + bgColor + ">" + "Id" + "</td>"//col
                    + " <td " + bgColor + ">Tipo" + "</td>"
                    + " <td " + bgColor + "> Valor" + "</td>"
                    + " </tr>\n";
            // Imprimimos el Map con un Iterador
            Iterator it = tablaSim.keySet().iterator();
            int cont2 = 0;
            while (it.hasNext()) {
                String key = (String) it.next();
                Simbolo sim = (Simbolo) tablaSim.get(key);
//                System.out.println("Clave: " + key + " -> Valor: " + tablaSim.get(key));
                String colorInterno = "bgcolor=\"#B0C4DE\"";
                if ((cont2 % 2) == 0) {
                    colorInterno = "bgcolor=\"#FFFFFF\"";
                }

                data += "<tr>    "//filas
                        + " <td " + colorInterno + ">" + key + "</td>"//col
                        + " <td " + colorInterno + ">" + sim.getTipo() + "</td>"
                        + " <td " + colorInterno + ">" + comprobarSiEsARReglo(sim.getValor()) + "</td>"
                        + " </tr>"
                        + "\n";
                cont2++;
            }//while

            data += "</TABLE>>];\n";
            if (ent.getPadre() == null) {
                data += "ent" + cont + "->null; \n}";
                break;
            } else {
                data += "ent" + cont + "->ent" + (cont + 1) + ";\n";
            }
            data += "\n\n\n";
            cont++;

        }//for
        crearArch_dot(data);
    }//metodo

    public String comprobarSiEsARReglo(Object obj) {
        if (obj instanceof Arreglo) {
            return "Arreglo";
        }
        return obj.toString();
    }

    public void crearArch_dot(String data) {
        try {
            File file = new File(path + ".dot");
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(data);
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Compilar_dot() {
        try {
            //dot -Tpng nombreArchivo.dot -o nomImagen.png
            String[] cmd = {"dot", "-T" + extension, path + ".dot", "-o", path + "." + extension};
            Runtime.getRuntime().exec(cmd);

            String[] cmd2 = {"okular", path + "." + extension};
            Runtime.getRuntime().exec(cmd2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}//clase
