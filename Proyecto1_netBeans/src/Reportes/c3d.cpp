// temporal = (cont var de todos los Entornos anteriores) + (posicion de la variable en el entorno actual)
#include<stdio.h>
double STACK[30101999];
double HEAP[30101999];
double tprintInt;
double tprint_i;
double tprint_j;
double P; //puntero del STACK
double H; //puntero del HEAP
double t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t22,t23,t24,t25,t26,t27,t28,t29,t30,t31,t32,t33,t34,t35;

void imprimir_var_int()
{
 double tprint;
 tprint = STACK[(int)P];
 printf("%f\n",tprint );
 return;
}

void imprimir_int()
{
 printf("%f\n",tprintInt);
 return;
}

void imprimir_var_char()
{
 double tprint;
 tprint = STACK[(int)P];
 printf("%c\n",(char)tprint );
 return;
}

void imprimir_char()
{
 printf("%c\n",(char)tprintInt);
 return;
}

void imprimir_string()
{
 L0:
  t31 = HEAP[(int)H] ;
     if (t31 != -1) goto L1 ;
     goto L2 ;
 L1:
     printf("%c",(char)t31);
     H = H + 1 ;
     goto L0 ;
  L2:
     printf("%c\n",(char)32);
 return;
}

void imprimir_arr_char_una_dim()
{
     printf("%c",(char) 91);
 L3:
  t32 = HEAP[(int)H] ;
     if (t32 != -1) goto L4 ;
     goto L5 ;
 L4:
 if ( t32 == 0 ) goto L6 ; //if para imprimir un espacio libre en el arreglo
 goto L7 ;
 L6:
     t32 = 95 ;//  95 = guion bajo
 L7:
     printf("%c",(char) t32 ) ;
     printf("%c",(char) 32);//espacios para que se observe mejor el numero
     printf("%c",(char) 32);
     H = H + 1 ;
     goto L3 ;
  L5:
     printf("%c\n",(char) 93);
 return;
}

void imprimir_arr_float_una_dim()
{
     printf("%c",(char) 91);
 L8:
  t33 = HEAP[(int)H] ;
     if (t33 != -1) goto L9 ;
     goto L10 ;
 L9:
     printf("%f",(float) t33);
     printf("%c",(char) 32);
     printf("%c",(char) 32);
     H = H + 1 ;
     goto L8 ;
  L10:
     printf("%c\n",(char) 93);
//Salto de linea para generar una mejor visivilidad return;
}

void imprimir_arr_float_dos_dim()
{
     printf("%c",(char) 91);
double cont = 0 ; L11:
  t34 = HEAP[(int)H] ;
     if ( t34 != -1) goto L12 ;
     goto L13 ;
 L12:
if ( cont == tprint_j ) goto L14 ;
goto L15 ;
L14: 
     printf("%c\n",(char) 32);
L15: 
     printf("%f",(float) t34);
     printf("%c",(char) 32);
     printf("%c",(char) 32);
     H = H + 1 ;
     cont = cont + 1 ;
     goto L11 ;
  L13:
     printf("%c\n\n",(char) 93);
//Salto de linea para generar una mejor visivilidad return;
}

void imprimir_arr_char_dos_dim()
{
     printf("%c",(char) 91);
double cont = 0 ; L16:
  t35 = HEAP[(int)H] ;
     if ( t35 != -1) goto L17 ;
     goto L18 ;
 L17:
if ( cont == tprint_j ) goto L19 ;
goto L20 ;
L19: 
     printf("%c\n",(char) 32);
L20: 
     printf("%c",(char) t35);
     printf("%c",(char) 32);
     printf("%c",(char) 32);
     H = H + 1 ;
     cont = cont + 1 ;
     goto L16 ;
  L18:
     printf("%c\n\n",(char) 93);
//Salto de linea para generar una mejor visivilidad return;
}


//----------------------------------- MAIN ------------------------------------------------
int main(){
  //Declaracion int
t0 = 0 + 0 ;
P = t0;
STACK[(int)P] = 100;

  //Declaracion int
t1 = 0 + 1 ;
P = t1;
STACK[(int)P] = 4;

  //Asignacion Arreglo: arr_dim.dim()=2
t2 = 0 + -1 ;
P = t2;
H = STACK[(int)P] ;
H = H + 16 ;//posicion relativa, dada por calculo de rowMajor
STACK[(int)H] = 9 ;


  //Print String:("arr_dim: ")
  t3 = H ;
    HEAP[(int)H] = 97 ;
      H = H + 1 ;
    HEAP[(int)H] = 114 ;
      H = H + 1 ;
    HEAP[(int)H] = 114 ;
      H = H + 1 ;
    HEAP[(int)H] = 95 ;
      H = H + 1 ;
    HEAP[(int)H] = 100 ;
      H = H + 1 ;
    HEAP[(int)H] = 105 ;
      H = H + 1 ;
    HEAP[(int)H] = 109 ;
      H = H + 1 ;
    HEAP[(int)H] = 58 ;
      H = H + 1 ;
    HEAP[(int)H] = 32 ;
      H = H + 1 ;
    HEAP[(int)H] = -1 ;
    H = t3 ;
  imprimir_string();

//Print Arreglo 2 dim Float: arr_dim;
  t4 = 0 + -1;
  P = t4;
  H = STACK[(int)P] ;
  tprint_i = -1 ;
  tprint_j = -1 ;
   imprimir_arr_float_dos_dim() ;

  //Declaracion int
t5 = 0 + 2 ;
P = t5;
STACK[(int)P] = -22;

  //Declaracion int
t6 = 0 + 3 ;
P = t6;
STACK[(int)P] = 3;

  //Declaracion Arreglo: array2.dim()=1
t7 = 0 + 4 ;
P = t7;
STACK[(int)P] = H ;
  HEAP[(int)H] = 1 ;
  H = H + 1 ;
  HEAP[(int)H] = 2 ;
  H = H + 1 ;
  HEAP[(int)H] = 3 ;
  H = H + 1 ;
  HEAP[(int)H] = 4 ;
  H = H + 1 ;
  HEAP[(int)H] = 5 ;
  H = H + 1 ;
  HEAP[(int)H] = 6 ;
  H = H + 1 ;
  HEAP[(int)H] = -1 ;   //fin del arreglo
  H = H + 1 ;

  //Asignacion Arreglo: array2.dim()=1
t8 = 0 + 4 ;
P = t8;
H = STACK[(int)P] ;
  HEAP[(int)H] = 1 ;
  H = H + 1 ;
  HEAP[(int)H] = 2 ;
  H = H + 1 ;
  HEAP[(int)H] = 3 ;
  H = H + 1 ;
  HEAP[(int)H] = 4 ;
  H = H + 1 ;
  HEAP[(int)H] = 5 ;
  H = H + 1 ;
  HEAP[(int)H] = 6 ;
  H = H + 1 ;
  HEAP[(int)H] = -1 ;   //fin del arreglo
  H = H + 1 ;


  //Print String:("array2: ")
  t9 = H ;
    HEAP[(int)H] = 97 ;
      H = H + 1 ;
    HEAP[(int)H] = 114 ;
      H = H + 1 ;
    HEAP[(int)H] = 114 ;
      H = H + 1 ;
    HEAP[(int)H] = 97 ;
      H = H + 1 ;
    HEAP[(int)H] = 121 ;
      H = H + 1 ;
    HEAP[(int)H] = 50 ;
      H = H + 1 ;
    HEAP[(int)H] = 58 ;
      H = H + 1 ;
    HEAP[(int)H] = 32 ;
      H = H + 1 ;
    HEAP[(int)H] = -1 ;
    H = t9 ;
  imprimir_string();

//Print Arreglo 1 dim Float: array2;
  t10 = 0 + 4;
  P = t10;
  H = STACK[(int)P] ;
   imprimir_arr_float_una_dim() ;

  //Declaracion Arreglo: array3.dim()=2
t11 = 0 + 5 ;
P = t11;
STACK[(int)P] = H ;
  HEAP[(int)H] = 1 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 15 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 0 ;
  H = H + 1 ;
  HEAP[(int)H] = 2 ;
  H = H + 1 ;
  HEAP[(int)H] = -1 ;   //fin del arreglo
  H = H + 1 ;

  //Asignacion Arreglo: array3.dim()=2
t12 = 0 + 5 ;
P = t12;
H = STACK[(int)P] ;
H = H + 14 ;//posicion relativa, dada por calculo de rowMajor
STACK[(int)H] = 15 ;

  //Asignacion Arreglo: array3.dim()=2
t13 = 0 + 5 ;
P = t13;
H = STACK[(int)P] ;
H = H + 5 ;//posicion relativa, dada por calculo de rowMajor
STACK[(int)H] = 1 ;

  //Asignacion Arreglo: array3.dim()=2
t14 = 0 + 5 ;
P = t14;
H = STACK[(int)P] ;
H = H + 24 ;//posicion relativa, dada por calculo de rowMajor
STACK[(int)H] = 2 ;

//Print Arreglo 2 dim Float: array3;
  t15 = 0 + 5;
  P = t15;
  H = STACK[(int)P] ;
  tprint_i = 5 ;
  tprint_j = 4 ;
   imprimir_arr_float_dos_dim() ;

  //Declaracion Arreglo: array2.dim()=1
t16 = 0 + 4 ;
P = t16;
STACK[(int)P] = H ;
  HEAP[(int)H] = 1 ;
  H = H + 1 ;
  HEAP[(int)H] = 2 ;
  H = H + 1 ;
  HEAP[(int)H] = 3 ;
  H = H + 1 ;
  HEAP[(int)H] = 4 ;
  H = H + 1 ;
  HEAP[(int)H] = 5 ;
  H = H + 1 ;
  HEAP[(int)H] = 6 ;
  H = H + 1 ;
  HEAP[(int)H] = -1 ;   //fin del arreglo
  H = H + 1 ;

  //Declaracion int
t17 = 0 + 6 ;
P = t17;
STACK[(int)P] = 0 ;

  //Declaracion int
t18 = 0 + 7 ;
P = t18;
STACK[(int)P] = 0 ;

  //Asignacion
t19 = 0 + 7 ;
P = t19 ;
STACK[(int)P] = 0 ;


  //Print String:("asignacion  -> c: ")
  t20 = H ;
    HEAP[(int)H] = 97 ;
      H = H + 1 ;
    HEAP[(int)H] = 115 ;
      H = H + 1 ;
    HEAP[(int)H] = 105 ;
      H = H + 1 ;
    HEAP[(int)H] = 103 ;
      H = H + 1 ;
    HEAP[(int)H] = 110 ;
      H = H + 1 ;
    HEAP[(int)H] = 97 ;
      H = H + 1 ;
    HEAP[(int)H] = 99 ;
      H = H + 1 ;
    HEAP[(int)H] = 105 ;
      H = H + 1 ;
    HEAP[(int)H] = 111 ;
      H = H + 1 ;
    HEAP[(int)H] = 110 ;
      H = H + 1 ;
    HEAP[(int)H] = 32 ;
      H = H + 1 ;
    HEAP[(int)H] = 32 ;
      H = H + 1 ;
    HEAP[(int)H] = 45 ;
      H = H + 1 ;
    HEAP[(int)H] = 62 ;
      H = H + 1 ;
    HEAP[(int)H] = 32 ;
      H = H + 1 ;
    HEAP[(int)H] = 99 ;
      H = H + 1 ;
    HEAP[(int)H] = 58 ;
      H = H + 1 ;
    HEAP[(int)H] = 32 ;
      H = H + 1 ;
    HEAP[(int)H] = -1 ;
    H = t20 ;
  imprimir_string();

  //Print ID int
t21 = 0 + 7;
P = t21;
imprimir_var_int();

  //Print ID int
t22 = 10 + 7;
P = t22;
imprimir_var_int();


  //Print String:("hola")
  t23 = H ;
    HEAP[(int)H] = 104 ;
      H = H + 1 ;
    HEAP[(int)H] = 111 ;
      H = H + 1 ;
    HEAP[(int)H] = 108 ;
      H = H + 1 ;
    HEAP[(int)H] = 97 ;
      H = H + 1 ;
    HEAP[(int)H] = -1 ;
    H = t23 ;
  imprimir_string();

  //Declaracion int
t24 = 10 + 0 ;
P = t24;
STACK[(int)P] = 7;

  //Declaracion int
t25 = 10 + 0 ;
P = t25;
STACK[(int)P] = 7;

  //Declaracion int
t26 = 11 + 0 ;
P = t26;
STACK[(int)P] = 72;

  //Declaracion int
t27 = 0 + 8 ;
P = t27;
  //Asignacion
t28 = 0 + 8 ;
P = t28 ;
STACK[(int)P] =  0.00000000 ;


  //Print String:("------>   la variable variable =")
  t29 = H ;
    HEAP[(int)H] = 45 ;
      H = H + 1 ;
    HEAP[(int)H] = 45 ;
      H = H + 1 ;
    HEAP[(int)H] = 45 ;
      H = H + 1 ;
    HEAP[(int)H] = 45 ;
      H = H + 1 ;
    HEAP[(int)H] = 45 ;
      H = H + 1 ;
    HEAP[(int)H] = 45 ;
      H = H + 1 ;
    HEAP[(int)H] = 62 ;
      H = H + 1 ;
    HEAP[(int)H] = 32 ;
      H = H + 1 ;
    HEAP[(int)H] = 32 ;
      H = H + 1 ;
    HEAP[(int)H] = 32 ;
      H = H + 1 ;
    HEAP[(int)H] = 108 ;
      H = H + 1 ;
    HEAP[(int)H] = 97 ;
      H = H + 1 ;
    HEAP[(int)H] = 32 ;
      H = H + 1 ;
    HEAP[(int)H] = 118 ;
      H = H + 1 ;
    HEAP[(int)H] = 97 ;
      H = H + 1 ;
    HEAP[(int)H] = 114 ;
      H = H + 1 ;
    HEAP[(int)H] = 105 ;
      H = H + 1 ;
    HEAP[(int)H] = 97 ;
      H = H + 1 ;
    HEAP[(int)H] = 98 ;
      H = H + 1 ;
    HEAP[(int)H] = 108 ;
      H = H + 1 ;
    HEAP[(int)H] = 101 ;
      H = H + 1 ;
    HEAP[(int)H] = 32 ;
      H = H + 1 ;
    HEAP[(int)H] = 118 ;
      H = H + 1 ;
    HEAP[(int)H] = 97 ;
      H = H + 1 ;
    HEAP[(int)H] = 114 ;
      H = H + 1 ;
    HEAP[(int)H] = 105 ;
      H = H + 1 ;
    HEAP[(int)H] = 97 ;
      H = H + 1 ;
    HEAP[(int)H] = 98 ;
      H = H + 1 ;
    HEAP[(int)H] = 108 ;
      H = H + 1 ;
    HEAP[(int)H] = 101 ;
      H = H + 1 ;
    HEAP[(int)H] = 32 ;
      H = H + 1 ;
    HEAP[(int)H] = 61 ;
      H = H + 1 ;
    HEAP[(int)H] = -1 ;
    H = t29 ;
  imprimir_string();

  //Declaracion int
t30 = 0 + 2 ;
P = t30;
STACK[(int)P] = -22;



return 0;
}